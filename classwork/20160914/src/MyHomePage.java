/**
 * @author Svintenok Katya
 * 11501
 */

import java.util.regex.*;
import java.io.*;
import java.util.Scanner;


public class MyHomePage {
    //creation of menu
    public static void writeMenu(FileWriter writer) throws IOException {
        Scanner reader = new Scanner(new File("menu.html"));
        while (reader.hasNext()){
            writer.write(reader.nextLine() + "\n");
        }
    }
    //creation of footer
    public static void writeFooter(FileWriter writer) throws IOException {
        Scanner reader = new Scanner(new File("footer.html"));
        while (reader.hasNext()){
            writer.write(reader.nextLine() + "\n");
        }
    }

    //creation error's page
    public static void write404(FileWriter writer) throws IOException {
        Scanner reader = new Scanner(new File("404.html"));
        while (reader.hasNext()){
            writer.write(reader.nextLine() + "\n");
        }
    }


    public static void main(String[] args) throws IOException {
        Scanner sc  = new Scanner(System.in);
        String request = sc.next();
        FileWriter writer = new FileWriter(new File("results/" + System.nanoTime()+ ".html"));

        Scanner reader = new Scanner(new File("head.html"));
        while (reader.hasNextLine()){
            writer.write(reader.nextLine() + "\n");
        }

        //home page
        if (request.matches("/index")) {
            writeMenu(writer);
            writer.write("\t\t<h3>Добро Пожаловать!</h3>\n");
            writeFooter(writer);
        }

        //list of friends
        else if (request.matches("/friends")){
            writeMenu(writer);
            writer.write("\t\t<h3>Друзья:</h3>\n");
            writer.write("\t\t<ul>\n");

            reader = new Scanner(new File("friends.txt"));

            while (reader.hasNextLine()){
                String[] s = reader.nextLine().split(" ");
                writer.write("\t\t\t<li><a href="+s[0]+"/>"+s[1]+"</a></li>\n");
            }

            writer.write("\t\t</ul>\n");
            writeFooter(writer);
        }

        //list of posts
        else if (request.matches("/posts")){
            writeMenu(writer);
            writer.write("\t\t<h3>Посты:</h3>\n");
            writer.write("\t\t<ul>\n");

            reader = new Scanner(new File("posts.txt"));

            while (reader.hasNextLine()){
                writer.write("\t\t\t<li><a href="+reader.nextLine()+"/>"+reader.nextLine()+"</a></li>\n");
                reader.nextLine();
                reader.nextLine();
            }

            writer.write("\t\t</ul>\n");
            writeFooter(writer);
        }

        //friend's personal page
        else if (request.matches("/friends/\\d+")){

            Pattern p = Pattern.compile("/friends/(?<id>\\d+)");
            Matcher m = p.matcher(request);
            boolean flag = false;

            if (m.find()){
                String id = m.group("id");

                reader = new Scanner(new File("friends.txt"));

                while (reader.hasNextLine()) {
                    String[] s = reader.nextLine().split(" ");
                    if (s[0].equals(id)) {
                        writeMenu(writer);
                        writer.write("\t\t<p><b>Имя: </b>" + s[1] + "</p>\n");
                        writer.write("\t\t<p><b>Университет: </b>" + s[2] + "</p>\n");
                        writer.write("\t\t<p><b>Увлечения: </b>" + s[3] + "</p>\n");
                        writeFooter(writer);
                        flag = true;
                        break;
                    }
                }
            }
            if (!flag)
                write404(writer);
        }

        //single post's page
        else if (request.matches("/posts/\\d+")){

            Pattern p = Pattern.compile("/posts/(?<id>\\d+)");
            Matcher m = p.matcher(request);
            boolean flag = false;
            if (m.find()) {
                String id = m.group("id");

                reader = new Scanner(new File("posts.txt"));

                while (reader.hasNextLine()) {
                    if (reader.nextLine().equals(id)) {
                        writeMenu(writer);
                        writer.write("\t\t<b>" + reader.nextLine() + "</b>\n");
                        writer.write("\t\t<p>" + reader.nextLine() + "</p>\n");
                        writer.write("\t\t<p><b>Дата: </b>" + reader.nextLine() + "</p>\n");
                        writeFooter(writer);
                        flag = true;
                        break;
                    }
                }
            }
            if (!flag)
                write404(writer);
        }

        //if URL was not found
        else {
            write404(writer);
        }

        writer.flush();
    }
}