import java.util.regex.*;

public class RegExTask {
    public static void main(String [] args) {
        //birth day
        Pattern p1 = Pattern.compile("^\\d{4}(\\/|\\.)(0\\d|1[0-2])\\1([0-2]\\d|3[0-1])$");

        System.out.println("birth day:");
        Matcher m1_1 = p1.matcher("2000.09.18");
        System.out.println(m1_1.matches());
        Matcher m1_2 = p1.matcher("1993/50/09");
        System.out.println(m1_2.matches());

        //password
        Pattern p2 = Pattern.compile("^[a-zA-Z0-9]{6,20}$");

        System.out.println("password:");
        Matcher m2_1 = p2.matcher("20fds918");
        System.out.println(m2_1.matches());
        Matcher m2_2 = p2.matcher("199ka");
        System.out.println(m2_2.matches());

        //nickname
        Pattern p3 = Pattern.compile("^[a-zA-Z0-9_-]{4,20}$");

        System.out.println("nickname:");
        Matcher m3_1 = p3.matcher("pony_1996");
        System.out.println(m3_1.matches());
        Matcher m3_2 = p3.matcher("p@ny");
        System.out.println(m3_2.matches());

        //e-mail
        Pattern p4 = Pattern.compile("^[a-zA-Z0-9._-]+@[a-zA-Z0-9]+\\.[a-z]{2,3}$");

        System.out.println("e-mail:");
        Matcher m4_1 = p4.matcher("svintenok@gmail.com");
        System.out.println(m4_1.matches());
        Matcher m4_2 = p4.matcher("svintenokgmail.com");
        System.out.println(m4_2.matches());

    }
}
