import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.regex.*;

public class Classwork1 {

    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("input1.txt"));

        while (scanner.hasNext()) {
            String str = scanner.nextLine();

            Pattern p = Pattern.compile("(?<hours>(0|1)\\d|2[0-3]):(?<minutes>[0-5]\\d)");
            Matcher m = p.matcher(str);

            while (m.find()){
                System.out.println(m.group("hours") + ":"+ m.group("minutes"));
            }
        }
    }
}
