import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task005 20160914
 */
public class Task005 {
    public static void main(String[] args) throws IOException {
        FileReader fileReader = new FileReader(new File("in.txt"));
        FileWriter fileWriter = new FileWriter(new File("out.txt"));

        int symbol = fileReader.read();
        while(symbol != -1){
            fileWriter.write(symbol);
            symbol = fileReader.read();
        }
        fileWriter.flush();
    }
}