import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task001 20160914
 */
public class Task001 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("in.txt")));
        PrintWriter printWriter = new PrintWriter(new File("out.txt"));
        printWriter.write(bufferedReader.readLine());
        printWriter.flush();
    }
}
