import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task015 20160914
 */
public class Task015 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("in.txt")));
        PrintWriter printWriter = new PrintWriter(new File("out.html"));
        String s = bufferedReader.readLine();
        if (s.equals("/index"))
            printWriter.write("YES");
        else
            printWriter.write("NO");
        printWriter.flush();
    }
}