import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task010 20160914
 */
public class Task010 {
    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File("in.txt"));
        FileOutputStream fileOutputStream = new FileOutputStream(new File("out.txt"));
        int symbol = fileInputStream.read();
        while (symbol != -1){
            fileOutputStream.write(symbol);
            symbol = fileInputStream.read();
        }
        fileOutputStream.flush();
    }
}