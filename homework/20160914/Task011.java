import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task011 20160914
 */
public class Task011 {
    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File("in.txt"));
        FileOutputStream fileOutputStream = new FileOutputStream(new File("out.txt"));
        byte[] buf = new byte[fileInputStream.available()];
        fileInputStream.read(buf);

        for (int i = 0; i < buf.length; i++)
            fileOutputStream.write(buf[i]);

        fileOutputStream.flush();
    }
}