import java.io.*;
import java.util.Scanner;

/**
 * @author Svintenok Katya
 * 11501
 * Task008 20160914
 */
public class Task008 {
    public static void main(String[] args) throws IOException {
        Scanner cs = new Scanner(System.in);
        FileWriter fileWriter = new FileWriter(new File("out.txt"));
        String s = cs.nextLine();
        for (int i = 0; i < s.length(); i++)
            fileWriter.write(s.charAt(i));
        fileWriter.flush();
    }
}
