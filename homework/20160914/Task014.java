import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task014 20160914
 */
public class Task014 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("in.txt")));
        String s = bufferedReader.readLine();
        if (s.matches("/index"))
            System.out.println("YES");
        else
            System.out.println("NO");
    }
}