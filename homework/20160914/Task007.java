import java.io.*;
import java.util.Scanner;

/**
 * @author Svintenok Katya
 * 11501
 * Task007 20160914
 */
public class Task007 {
    public static void main(String[] args) throws IOException {
        Scanner cs = new Scanner(System.in);
        PrintWriter printWriter = new PrintWriter(new File("out.txt"));
        printWriter.write(cs.nextLine());
        printWriter.flush();
    }
}
