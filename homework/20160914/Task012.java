import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task012 20160914
 */
public class Task012 {
    public static void main(String[] args) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File("in.txt"));
        FileOutputStream fileOutputStream = new FileOutputStream(new File("out.txt"));
        byte[] buf = new byte[fileInputStream.available()];
        fileInputStream.read(buf);
        fileOutputStream.write(buf);
        fileOutputStream.flush();
    }
}