import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task003 20160914
 */
public class Task003 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("in.txt")));
        String s = bufferedReader.readLine();
        for (int i = 0; i < s.length(); i++)
            System.out.print(s.charAt(i));
    }
}