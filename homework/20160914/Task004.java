import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task004 20160914
 */
public class Task004 {
    public static void main(String[] args) throws IOException {
        FileReader fileReader = new FileReader(new File("in.txt"));
        PrintWriter printWriter = new PrintWriter(new File("out.txt"));
        int symbol = fileReader.read();
        String s = "";
        while(symbol != -1){
            s += (char)symbol;
            symbol = fileReader.read();
        }
        printWriter.write(s);
        printWriter.flush();
    }
}
