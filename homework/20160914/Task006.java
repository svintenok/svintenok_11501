import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task006 20160914
 */
public class Task006 {
    public static void main(String[] args) throws IOException {
        FileReader fileReader = new FileReader(new File("in.txt"));

        int symbol = fileReader.read();
        while(symbol != -1) {
            System.out.print((char) symbol);
            symbol = fileReader.read();
        }
    }
}
