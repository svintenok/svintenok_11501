import java.util.Scanner;

/**
 * @author Svintenok Katya
 * 11501
 * Task009 20160914
 */
public class Task009 {
    public static void main(String[] args) {
        Scanner cs = new Scanner(System.in);
        String s = cs.nextLine();
        for (int i = 0; i < s.length(); i++)
            System.out.print(s.charAt(i));
    }
}