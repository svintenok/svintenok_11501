import java.io.*;

/**
 * @author Svintenok Katya
 * 11501
 * Task002  20160914
 */
public class Task002 {
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("in.txt")));
        FileWriter fileWriter = new FileWriter(new File("out.txt"));
        fileWriter.write(bufferedReader.readLine());
        fileWriter.flush();
    }
}