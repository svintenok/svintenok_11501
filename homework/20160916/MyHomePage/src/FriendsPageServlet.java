import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;


@WebServlet(name = "FriendsPageServlet")
public class FriendsPageServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();
        Statement st = Filter.st;
        String id = request.getParameter("id");

        if( id == null) {
            try {
                writer.write("\t\t<h3>Друзья:</h3>\n");
                writer.write("\t\t<ul>\n");

                ResultSet rs = st.executeQuery("select * from friends");
                while (rs.next())
                    writer.write("\t\t\t<li><a href=friends?id=" + rs.getString("id") + ">" + rs.getString("name") + "</a></li>\n");

                writer.write("\t\t</ul>\n");
                writer.write("\t\t<hr/>\n");
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        else {
            boolean flag = false;

            try {
                ResultSet rs = st.executeQuery("select * from friends where id="+ id);

                if (rs.next()) {
                    flag = true;
                    writer.write("\t\t<p><b>Имя: </b>" + rs.getString("name") + "</p>\n");
                    writer.write("\t\t<p><b>Университет: </b>" + rs.getString("univer") + "</p>\n");
                    writer.write("\t\t<p><b>Увлечения: </b>" + rs.getString("interests") + "</p>\n");
                    writer.write("\t\t<hr/>\n");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (!flag)
                response.sendError(404);

        }

    }
}
