import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;


@WebFilter(filterName = "Filter")
public class Filter implements javax.servlet.Filter {

    static Statement st;

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {

        resp.setContentType("text/html; charset=windows-1251");
        PrintWriter writer = resp.getWriter();

        Scanner reader = new Scanner(new File("D:/IdeaProjects/MyHomePage1/head.html"));
        while (reader.hasNextLine())
            writer.write(reader.nextLine() + "\n");

        reader = new Scanner(new File("D:/IdeaProjects/MyHomePage1/menu.html"));
        while (reader.hasNext())
            writer.write(reader.nextLine() + "\n");

        try {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            Connection conn = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/MyHomePage",
                    "postgres",
                    "postgres"
            );

            st = conn.createStatement();
            chain.doFilter(req, resp);

        } catch (SQLException e) {
            e.printStackTrace();
        }

        reader = new Scanner(new File("D:/IdeaProjects/MyHomePage1/footer.html"));
        while (reader.hasNext())
            writer.write(reader.nextLine() + "\n");

    }

    public void init(FilterConfig config) throws ServletException {}

    public void destroy() {}
}
