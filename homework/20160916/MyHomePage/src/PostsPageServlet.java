import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;


@WebServlet(name = "PostsPageServlet")
public class PostsPageServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter writer = response.getWriter();
        Statement st = Filter.st;
        String id = request.getParameter("id");

        if( id == null) {
            try {
                writer.write("\t\t<h3>Посты:</h3>\n");
                writer.write("\t\t<ul>\n");

                ResultSet rs = st.executeQuery("select * from posts");
                while (rs.next())
                    writer.write("\t\t\t<li><a href=/posts?id=" + rs.getString("id") + ">" + rs.getString("title") + "</a></li>\n");

                writer.write("\t\t</ul>\n");
                writer.write("\t\t<hr/>\n");

            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        else {
            boolean flag = false;

            try {
                ResultSet rs = st.executeQuery("select * from posts where id=" + id);

                if (rs.next()) {
                    flag = true;
                    writer.write("\t\t<b>" + rs.getString("title") + "</b>\n");
                    writer.write("\t\t<p>" + rs.getString("text") + "</p>\n");
                    writer.write("\t\t<p><b>Дата: </b>" + rs.getString("date") + "</p>\n");
                    writer.write("\t\t<hr/>\n");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (!flag)
                response.sendError(404);

        }
    }
}
