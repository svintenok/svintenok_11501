create table friends(
    id serial primary key,
    "name" varchar(30) NOT NULL,
    univer varchar(10) NOT NULL,
    interests varchar(30) NOT NULL
);

create table posts(
    id serial primary key,
    "title" varchar(30) NOT NULL,
    "text" text NOT NULL,
    "date" date NOT NULL
);