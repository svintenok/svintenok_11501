<#include "base.ftl">

<#macro content>
    <a href="/guest">Guest Page</a>
    <hr/>
    <form action="/login" method="POST">
        <input type="text" name="username" <#if login??> value="${login}"</#if>/>
        <input type="password" name="password"/>
        <input type="submit" value="Login"/>
    </form>
    <#if err??><p>Sorry, wrong username or password</p></#if>
</#macro>