<#macro messages_menu>
    <ul>
        <li><a href="/new_message">New message</a></li>
        <li><a href="/in_messages">Incoming messages</a></li>
        <li><a href="/out_messages">Sent messages</a></li>
    </ul>
    <hr/>
</#macro>