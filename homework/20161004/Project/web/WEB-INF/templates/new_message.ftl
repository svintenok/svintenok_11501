<#include "messages_menu.ftl">
<#include "base.ftl">

<#macro content>
    <a href="/secret">Secret Page</a>
    <a href="/guest">Guest Page</a>
    <a href="/logout">Logout</a>
    <hr/>
    <@messages_menu/>
    <div class="container">
    <form action="/new_message" method="POST">
        <div class = "form-group">
            <textarea class = "form-control" rows = "3" name="text" placeholder="Type your text here.."></textarea>
        </div>
        <div class="form-inline">
        <select class="form-control" name="recipient">
         <#list users as user>
             <option value="${user}">${user}</option>
             <li>${user}</li>
         </#list>
        </select>
        <button type = "submit" class = "btn btn-default">Submit</button>
        </div>
    </div>
    </form>
</#macro>