<#include "messages_menu.ftl">
<#include "base.ftl">

<#macro content>
    <a href="/secret">Secret Page</a>
    <a href="/guest">Guest Page</a>
    <a href="/logout">Logout</a>
    <hr/>
    <@messages_menu/>
    <h3>In messages:</h3>
    <table class="table">
        <thead>
        <tr>
            <th>recipient</th>
            <th></th>
            <th>date</th>
        </tr>
        </thead>
        <tbody>
            <#list messages as message>
            <tr>
                <td>${message.recipient}</td>
                <td>${message.text}</td>
                <td>${message.date}</td>
            </tr>
            </#list>
        </tbody>
    </table>
</#macro>