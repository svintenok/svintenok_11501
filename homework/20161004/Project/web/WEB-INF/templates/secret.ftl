<#include "base.ftl">

<#macro content>
    <a href="/in_messages">Messages</a>
    <a href="/guest">Guest Page</a>
    <a href="/logout">Logout</a>
    <hr/>
    <h3>This is secret text for ${username}</h3>
    <hr/>
    <ul>
    <#list users as user>
        <li>${user}</li>
    </#list>
    </ul>
</#macro>