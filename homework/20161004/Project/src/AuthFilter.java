import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

@WebFilter(filterName = "AuthFilter")
public class AuthFilter implements javax.servlet.Filter {

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        boolean authorization = false;
        if(request.getSession().getAttribute("current_user") != null) {
            authorization = true;
        }
        else {
            Cookie[] cookies = request.getCookies();
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("user")) {
                    HttpSession session = request.getSession();
                    session.setAttribute("current_user", cookie.getValue());
                    authorization = true;
                    break;
                }
            }
        }
        if (authorization)
            chain.doFilter(req, resp);
        else
            response.sendRedirect("/login");

    }

    public void init(FilterConfig config) throws ServletException {}

    public void destroy() {}

}
