import freemarker.template.Template;
import freemarker.template.TemplateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;


@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = request.getParameter("username").toLowerCase();
        String password = request.getParameter("password");

        try {
            Statement st = DBSingleton.getConnection().createStatement();
            ResultSet rs = st.executeQuery("select * from users where username='" + username +
                    "' and password='" + password + "'");
            if (rs.next()) {
                Cookie cookie = new Cookie("user", username);
                cookie.setMaxAge(24 * 60 * 60);
                response.addCookie(cookie);
                session.setAttribute("current_user", username);
                response.sendRedirect("/secret");
            }
            else {
                response.sendRedirect("/login?err=true&login=" + username);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Template tmpl = ConfigSingleton.getConfig(
                request.getServletContext()
        ).getTemplate("login.ftl");

        Map<String, Object> root = new HashMap<>();
        root.put("login", request.getParameter("login"));
        root.put("err", request.getParameter("err"));

        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }

    }
}
