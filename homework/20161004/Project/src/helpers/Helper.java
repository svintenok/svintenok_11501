package helpers;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import singletons.ConfigSingleton;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;


public class Helper {
    public static void render(HttpServletResponse response,
                              HttpServletRequest request,
                              String template,
                              HashMap<String, Object> root) throws IOException {
        Template tmpl = ConfigSingleton.getConfig(
                request.getServletContext()
        ).getTemplate(template);

        try {
            tmpl.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

}
