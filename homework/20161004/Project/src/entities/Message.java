package entities;

import java.sql.Timestamp;
import java.time.format.DateTimeFormatter;

public class Message {
    public String text;
    public String sender;
    public String recipient;
    public Timestamp date;
    public boolean status;

    public Message(String text, String sender, String recipient, Timestamp date, boolean status) {
        this.text = text;
        this.sender = sender;
        this.recipient = recipient;
        this.date = date;
        this.status = status;
    }

    @Override
    public String toString() {
        return "Message{" +
                "text='" + text + '\'' +
                ", sender='" + sender + '\'' +
                ", recipient='" + recipient + '\'' +
                ", date='" + date + '\'' +
                ", status=" + status +
                '}';
    }

    public String getText() {
        return text;
    }

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd HH:mm");
        return date.toLocalDateTime().format(formatter);
    }

    public boolean isStatus() {
        return status;
    }
}
