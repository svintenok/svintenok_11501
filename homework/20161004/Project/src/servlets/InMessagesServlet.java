package servlets;

import entities.Message;
import singletons.DBSingleton;
import static helpers.Helper.render;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@WebServlet(name = "servlets.InMessagesServlet")
public class InMessagesServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HashMap<String, Object> root = new HashMap<>();
        String username = (String) request.getSession().getAttribute("current_user");

        try {
            Connection con = DBSingleton.getConnection();
            PreparedStatement psmt = con.prepareStatement("select * from message where recipient=? order by \"date\" desc");
            psmt.setString(1, username);

            ResultSet rs = psmt.executeQuery();
            List<Message> messages = new ArrayList<>();

            while (rs.next()) {
                Message message = new Message(
                        rs.getString("text"),
                        rs.getString("sender"),
                        rs.getString("recipient"),
                        rs.getTimestamp("date"),
                        rs.getBoolean("status"));
                messages.add(message);
            }

            root.put("messages", messages);
            render(response, request, "in_messages.ftl", root);

            psmt = con.prepareStatement("update message set status=true where recipient=? and status = false;");
            psmt.setString(1, username);
            psmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
