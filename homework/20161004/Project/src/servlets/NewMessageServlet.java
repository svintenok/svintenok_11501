package servlets;

import singletons.DBSingleton;
import static helpers.Helper.render;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@WebServlet(name = "NewMessageServlet")
public class NewMessageServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = (String) request.getSession().getAttribute("current_user");
        String recipient = request.getParameter("recipient").toLowerCase();
        String text = request.getParameter("text");

        try {
            Connection con = DBSingleton.getConnection();
            PreparedStatement psmt = con.prepareStatement(
                    "insert into message(\"text\", sender, recipient, \"date\") values (?, ?, ?, 'now');");
            psmt.setString(1, text);
            psmt.setString(2, username);
            psmt.setString(3, recipient);
            psmt.executeUpdate();

            response.sendRedirect("/out_messages");

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HashMap<String, Object> root = new HashMap<>();

        try {
            Statement st = DBSingleton.getConnection().createStatement();
            ResultSet rs = st.executeQuery("select username from users");
            List<String> users = new ArrayList<>();

            while (rs.next()) {
                users.add(rs.getString("username"));
            }

            root.put("users", users);
            render(response, request, "new_message.ftl", root);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
