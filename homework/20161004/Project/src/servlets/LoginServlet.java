package servlets;

import singletons.DBSingleton;
import static helpers.Helper.render;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;


@WebServlet(name = "servlets.LoginServlet")
public class LoginServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String username = request.getParameter("username").toLowerCase();
        String password = request.getParameter("password");

        try {
            Connection con = DBSingleton.getConnection();
            PreparedStatement psmt = con.prepareStatement("select * from users where username=? and password=?");
            psmt.setString(1, username);
            psmt.setString(2, password);

            ResultSet rs = psmt.executeQuery();
            if (rs.next()) {
                Cookie cookie = new Cookie("user", username);
                cookie.setMaxAge(24 * 60 * 60);
                response.addCookie(cookie);
                session.setAttribute("current_user", username);
                response.sendRedirect("/secret");
            }
            else {
                response.sendRedirect("/login?err=true&login=" + username);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HashMap<String, Object> root = new HashMap<>();
        root.put("login", request.getParameter("login"));
        root.put("err", request.getParameter("err"));

        render(response, request, "login.ftl", root);
    }
}
