package servlets;

import singletons.DBSingleton;

import static helpers.Helper.render;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


@WebServlet(name = "servlets.SecretServlet")
public class SecretServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        HashMap<String, Object> root = new HashMap<>();
        root.put("username", request.getSession().getAttribute("current_user"));

        try {
            Statement st = DBSingleton.getConnection().createStatement();
            ResultSet rs = st.executeQuery("select username from users");
            List<String> users = new ArrayList<>();

            while (rs.next()) {
                users.add(rs.getString("username"));
            }

            root.put("users", users);
            render(response, request, "secret.ftl", root);

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
