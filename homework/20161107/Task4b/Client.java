package Task4b;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Author: Svintenok Kate
 * Date: 07.11.2016
 * Group: 11-501
 */
public class Client {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
        PrintWriter printWriter = new PrintWriter(s.getOutputStream(), true);

        System.out.println("Чтобы сдаться, введите 'сдаюсь'");
        boolean end = false;

        while (true) {

            String city = bufferedReader.readLine();
            if (!city.equals("сдаюсь")) {
                System.out.println("Город противника:" + city);
            } else {
                System.out.println("Вы выиграли");
                break;
            }

            boolean flag = false;

            while (!flag) {
                System.out.print("Вaш город: ");
                city = scanner.nextLine();
                if (!city.equals("сдаюсь")) {
                    printWriter.println(city);

                    if (bufferedReader.readLine().equals("верно")) {
                        flag = true;
                    } else {
                        System.out.println("Неверно, попробуйте еще раз");
                    }
                } else {
                    printWriter.println(city);
                    System.out.println("Вы проиграли");
                    end = true;
                    break;
                }
            }
            if (end)
                break;
        }
    }
}