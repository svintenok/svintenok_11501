package Task4b;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;

/**
 * Author: Svintenok Kate
 * Date: 07.11.2016
 * Group: 11-501
 * Task: semester project
 */
public class Server {
    public static void main(String[] args) throws IOException {

        String[] cities = {"абакан", "нижнекамск", "казань", "новосибирск", "калининград", "донецк"};

        Scanner scanner = new Scanner(System.in);

        final int PORT = 3456;
        ServerSocket s = new ServerSocket(PORT);
        Socket client = s.accept();

        HashSet<String> citiesSet = new HashSet<>();
        for (String city : cities)
            citiesSet.add(city);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
        PrintWriter printWriter = new PrintWriter(client.getOutputStream(), true);

        System.out.println("Чтобы сдаться, введите 'сдаюсь'");
        char letter;
        boolean flag = false;
        String city = "";
        boolean end = false;

        while (!flag) {
            System.out.print("Вaш город: ");
            city = scanner.nextLine().toLowerCase();

            if (citiesSet.contains(city)) {
                flag = true;
            } else {
                System.out.println("Неверно, попробуйте еще раз");
            }

        }

        citiesSet.remove(city);
        printWriter.println(city);
        letter = city.charAt(city.length() - 1);

        while (true) {

            flag = false;

            while (!flag) {
                city = bufferedReader.readLine().toLowerCase();
                if (!city.equals("сдаюсь")) {
                    if (citiesSet.contains(city) && letter == city.charAt(0)) {
                        citiesSet.remove(city);
                        System.out.println("Город противника:" + city);
                        letter = city.charAt(city.length() - 1);
                        printWriter.println("верно");
                        flag = true;
                    } else {
                        printWriter.println("неверно");
                    }
                } else {
                    System.out.println("Вы выиграли");
                    end = true;
                    break;
                }
            }
            if (end)
                break;

            flag = false;

            while (!flag) {
                System.out.print("Вaш город: ");
                city = scanner.nextLine().toLowerCase();
                if (!city.equals("сдаюсь")) {
                    if (citiesSet.contains(city) && letter == city.charAt(0)) {
                        citiesSet.remove(city);
                        printWriter.println(city);
                        letter = city.charAt(city.length()-1);
                        flag = true;
                    } else {
                        System.out.println("Неверно, попробуйте еще раз");
                    }
                } else {
                    printWriter.println(city);
                    System.out.println("Вы проиграли");
                    end = true;
                    break;
                }
            }
            if (end)
                break;

        }

    }
}
