package Task4a;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Author: Svintenok Kate
 * Date: 07.11.2016
 * Group: 11-501
 */
public class Client {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);

        InputStream is = s.getInputStream();
        OutputStream os = s.getOutputStream();
        while (true) {

            System.out.print("Введите цифру: ");
            byte b = (byte) scanner.nextInt();
            os.write(b);

            int x = is.read();
            if (x == 1)
                System.out.println("Вы угадали!");
            else
                System.out.println("Нет");

        }
    }
}
