package Task4a;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.Scanner;

/**
 * Author: Svintenok Kate
 * Date: 07.11.2016
 * Group: 11-501
 * Task: semester project
 */
public class Server {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        final int PORT = 3456;
        ServerSocket s = new ServerSocket(PORT);
        Socket client = s.accept();

        OutputStream os = client.getOutputStream();
        InputStream is = client.getInputStream();
        Random random = new Random();
        while (true) {
            int y = random.nextInt(10);

            int x = is.read();
            if (y == x)
                os.write(1);
            else
                os.write(0);
        }

    }
}
