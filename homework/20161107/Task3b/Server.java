package Task3b;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Author: Svintenok Kate
 * Date: 07.11.2016
 * Group: 11-501
 * Task: semester project
 */
public class Server {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        final int PORT = 3456;
        ServerSocket s = new ServerSocket(PORT);
        Socket client = s.accept();

        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
        while (true) {

            System.out.print("Ваше cообщение: ");
            String message = scanner.next();
            bufferedWriter.write(message);
            bufferedWriter.flush();

            char[] buf = new char[200];
            int i = bufferedReader.read(buf);
            String inMessage = new String(buf, 0, i);
            System.out.println("Ответ: "+ inMessage);
        }

    }
}
