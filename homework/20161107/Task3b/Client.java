package Task3b;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Author: Svintenok Kate
 * Date: 07.11.2016
 * Group: 11-501
 * Task: semester project
 */
public class Client {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
        BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
        while (true) {

            char[] buf = new char[200];
            int i = bufferedReader.read(buf);
            String inMessage = new String(buf, 0, i);
            System.out.println("Ответ: " + inMessage);

            System.out.print("Ваше cообщение: ");
            String outMessage = scanner.next();
            bufferedWriter.write(outMessage);
            bufferedWriter.flush();
        }
    }
}
