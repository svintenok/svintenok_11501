package Task3c;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Author: Svintenok Kate
 * Date: 07.11.2016
 * Group: 11-501
 */
public class Client {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        int port = 3456;
        String host = "localhost";
        Socket s = new Socket(host, port);

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
        PrintWriter printWriter = new PrintWriter(s.getOutputStream(), true);
        System.out.print("Введите ваше имя: ");
        printWriter.println(scanner.nextLine());

        String name = bufferedReader.readLine();

        while (true) {

            String inMessage = bufferedReader.readLine();
            System.out.println(name + ": " + inMessage);

            System.out.print("Вы: ");
            String message = scanner.nextLine();
            printWriter.println(message);
        }
    }
}
