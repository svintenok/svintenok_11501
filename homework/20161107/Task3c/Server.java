package Task3c;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Author: Svintenok Kate
 * Date: 07.11.2016
 * Group: 11-501
 */
public class Server {
    public static void main(String[] args) throws IOException {

        Scanner scanner = new Scanner(System.in);

        final int PORT = 3456;
        ServerSocket s = new ServerSocket(PORT);
        Socket client = s.accept();

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(client.getInputStream()));
        PrintWriter printWriter = new PrintWriter(client.getOutputStream(), true);
        System.out.print("Введите ваше имя: ");
        printWriter.println(scanner.nextLine());

        String name = bufferedReader.readLine();

        while (true) {

            System.out.print("Вы: ");
            String message = scanner.nextLine();
            printWriter.println(message);

            String inMessage = bufferedReader.readLine();
            System.out.println(name + ": " + inMessage);
        }

    }
}
