/**
 * Author: Svintenok Kate
 * Date: 24.11.2016
 * Group: 11-501
 */
public class Main {
    public static void main(String[] args) {
        ASCIIVector2D av =
                new ASCIIVector2D(new Vector2D(3, -3));
        av.printASCIIView();
    }
}
