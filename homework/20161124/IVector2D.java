/**
 * Author: Svintenok Kate
 * Date: 24.11.2016
 * Group: 11-501
 */
public interface IVector2D {
    double getX();
    double getY();
    IVector2D add(IVector2D vector2D);
}
