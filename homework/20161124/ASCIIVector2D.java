/**
 * Author: Svintenok Kate
 * Date: 24.11.2016
 * Group: 11-501
 */
public class ASCIIVector2D implements IVector2D, IASCIIFigure {
    private IVector2D vector2D;
    private final static int n = 20;

    public ASCIIVector2D(IVector2D vector2D) {
        this.vector2D = vector2D;
    }

    @Override
    public void printASCIIView() {
        double x = getX();
        double y = getY();

        double k = y/x;


            for (int i = n; i >= -n; i--){
                double y1 = i;
                for (int j = -n; j <= n; j++){
                    double x1 = j;
                    double d1 = Math.abs( y1 / (x1 - 1) - k);
                    double d2 = Math.abs( y1 / x1 - k);
                    double d3 = Math.abs( y1 / (x1 + 1) - k);
                    if (d1 > d2 && d3 > d2 && (int)Math.signum(y1) ==(int)Math.signum(y)){
                        System.out.print('*');
                    }
                    else {
                        System.out.print(' ');
                    }
                }
                System.out.println();
            }
        }


    @Override
    public double getX() {
        return vector2D.getX();
    }

    @Override
    public double getY() {
        return vector2D.getY();
    }

    @Override
    public IVector2D add(IVector2D vector2D) {
        return this.vector2D.add(vector2D);
    }
}
