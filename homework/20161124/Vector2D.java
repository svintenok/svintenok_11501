/**
 * Author: Svintenok Kate
 * Date: 24.11.2016
 * Group: 11-501
 */
public final class Vector2D implements IVector2D {
    private double x;
    private double y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D() {
        this(0.0, 0.0);
    }

    @Override
    public Vector2D add(IVector2D vector2D) {
        return new Vector2D(this.x + vector2D.getX(), this.y + vector2D.getY());
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ')';
    }
}
