/**
 * Author: Svintenok Kate
 * Date: 24.11.2016
 * Group: 11-501
 */
public interface IASCIIFigure {
    void printASCIIView();
}
