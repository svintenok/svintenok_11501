/**
* @author Svintenok Katya
* 11501
* 41b
*/
import java.util.Scanner;

public class Task41b {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		int [][][] a = new int [n][n][n];
		boolean flag1 = true;
		boolean flag2 = true;
		
		for (int i1 = 0; i1 < n; i1++)
			for (int i2 = 0; i2 < n; i2++)
				for (int i3 = 0; i3 < n; i3++)
					a[i1][i2][i3] = sc.nextInt();
		
		for (int i1 = 0; i1 < n && flag1; i1++) {
			
			for (int i2 = 0; i2 < n; i2++) {
				
				flag2 = true;
				for (int i3 = 0; i3 < n; i3++)
					if (a[i1][i2][i3] % 3 != 0)
						flag2 = false;
			}

			flag1 = flag2;
		}
		
		System.out.println(flag1);
	}
}