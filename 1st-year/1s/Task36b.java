/**
* @author Svintenok Katya
* 11501
* 36b
*/

public class Task36b {
	public static void main(String[] args) {
		
		final int CAPACITY = 10000; 
		int [] a1 = new int[CAPACITY];
		int [] a2 = new int[CAPACITY];
		int [] a = new int[CAPACITY];
		int size1 = 0;
		int size2 = 0;

		String n = args[0];
		for (int i = n.length() - 1; i >= 0; i--){
			a1[size1] = n.charAt(i) - '0';
			size1++;
		}
		
		n = args[1];
		for (int i = n.length() - 1; i >= 0; i--){
			a2[size2] = n.charAt(i) - '0';
			size2++;
		}	
		
		for (int i = 0; i < size1 + size2; i++)
			a[i] = 0;
		
		
		int i1 = 0;
		while (i1 < size2){
			int r = 0;
			int i2 = 0;
		
			while (i2 < size1) {
				int p = a1[i2] * a2[i1]  + r + a[i1 + i2];
				a[i1 + i2] = p % 10;
				r = p / 10;
				i2++;
			}
			a[i1 + i2] =a[i1 + i2] + r;
			i1++;
		}
		
		int size = a[size1 + size2 - 1] == 0? size1 + size2 - 2: size1 + size2 - 1;
		
			for (int i = size; i >= 0; i--)
		System.out.print(a[i]);	
	}
}