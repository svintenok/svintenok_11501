/**
* @author Svintenok Katya
* 11501
* 21a
*/
import java.lang.Math;

public class Task21a {
	public static void main(String[] args) {
		int r = Integer.parseInt(args[0]);
		double x;
		double x1;
		double x2;
		for (int y = -r; y <= r; y++) {
			x = (double)(Math.sqrt(r * r  - y * y));
			x = Math.round(x);
			if (y <= 0)
				x1 = (double)(Math.sqrt(-y * ( r + y)));
			else
				x1 = (double)(Math.sqrt(y * ( r - y)));
			x1 = Math.round(x1);
			for (int i = 1; i <= r - x; i++)
				System.out.print(" ");
			for (int i = 1; i <= x - x1; i++)
				System.out.print("0");
			for (int i = 1; i <= 2 * x1; i++) {
				if (y <= 0)
					System.out.print("*");
				else
					System.out.print("0");
			}
			for (int i = 1; i <= x - x1; i++)
				System.out.print("*");
			System.out.println("");
		}
	}
}