/**
* @author Svintenok Katya
* 11501
* 23b
*/

public class Task23b {
	public static void main(String[] args) {
		final double EPS = 1e-6;
		long k = 1;
		double r = 1;
		long a;
		do {
			a = 4 * k * k;
			r = r * a / (a - 1.0);
			k++;
		}
		while((Math.abs(r - Math.PI / 2)) > EPS);
		System.out.println(r);
	}
}