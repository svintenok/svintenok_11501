/**
* @author Svintenok Katya
* 11501
* 03c
*/

class Task03c {
    public static void main(String[] args)
    {
        System.out.println("11  111      11      1111111111  11111111");
        System.out.println("11 11       1111         11      11");
        System.out.println("1111       11  11        11      11111111");
        System.out.println("11 11     11111111       11      11");
        System.out.println("11  111  11      11      11      11111111");
    }
}