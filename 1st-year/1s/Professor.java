import java.util.Random;

public class Professor {
	
	private String name, subject;
	
	public Professor(String name, String subject) {
		this.name = name;
		this.subject = subject;
	}
	
	public String getName() {
		return name;
	}
	public String getSubject() { 
		return subject;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public void assessStudent(Student st) {
		Random rand = new Random();
		int mark = rand.nextInt(4) + 2;
		String markString = new String();
		switch(mark) {
			case 2:
				markString = "неудовлетворительно";
				break;
			case 3:
				markString = "удовлетворительно";
				break;
			case 4:
				markString = "хорошо";
				break;
			case 5:
				markString = "отлично"; 
				break;
		}
		System.out.println("Преподаватель " + this.name + " оценил студента с именем " + st.getName() + " по предмету " + this.subject + " на оценку " + markString);
	}
	
}