/**
* @author Svintenok Katya
* 11501
* 31b
*/

public class Task31b {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		long s = 0;
		long i = 1;
		
		while (n > 0) {
			s = s + i;
			i = i * 10 + 1;
			n--;
		}
		 
		System.out.println(s);
	}
}