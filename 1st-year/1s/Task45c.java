/**
* @author Svintenok Katya
* 11501
* 45c
*/

import java.util.Scanner;

public class Task45c {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		String [] s = sc.nextLine().split(" ");
		
		for (int i = 0; i < s.length; i++) {
			if (s[i].compareToIgnoreCase("mom") == 0) {
				if (s[i].charAt(0) == 'm')
					s[i] = "dad";
				else
					s[i] = "Dad";
			}
			System.out.println(s[i]);
		}
	}
}