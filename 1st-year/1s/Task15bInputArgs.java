/**
* @author Svintenok Katya
* 11501
* 15bInputArgs
*/

public class Task15bInputArgs {
	public static void main(String[] args) {		
		int x = Integer.parseInt(args[0]); 
		int S = Integer.parseInt(args[1]); //an
		for (int i = 2; i < args.length; i++) {
			int a = Integer.parseInt(args[i]); // от a(n-1) до a1
			S = a + x * S;
		} 
		System.out.println(S);
	}
}
			