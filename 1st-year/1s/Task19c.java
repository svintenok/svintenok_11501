/**
* @author Svintenok Katya
* 11501
* 19c
*/

public class Task19c {
	public static void main(String[] args){
		int n = Integer.parseInt(args[0]);
		double s = 0.5;
		int m1 = 1;
		int m2 = 2;
		for (int i = 2; i <= n; i++) {
			m1 = m1 * (i - 1) * (i - 1);
			m2 = m2 * 2 * i * (2 * i - 1);
			s = s +(double)(m1) / m2;	
		}	
		System.out.println(s);
	}
}