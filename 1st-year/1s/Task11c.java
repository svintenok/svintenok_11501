/**
* @author Svintenok Katya
* 11501
* 11c
*/

public class Task11c {
    public static void main(String[] args) {
		int k = 3;
		int i = 2;
		while (i < 10) {
			System.out.println(i + " x " + k + " = " + (i * k));
			i++;
		}
	}
}