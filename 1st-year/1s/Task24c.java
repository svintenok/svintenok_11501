/**
* @author Svintenok Katya
* 11501
* 24c
*/

public class Task24c {
	public static void main(String[] args) {
		final double EPS = 1e-8;
		int m = 0;
		double s = 0;
		double a;
		int k = 1;
		do {
			a = 1.0 / (2 * m + 1) / k;
			s = s + a;
			k = k * 9;
			m++;
		}
		while (Math.abs(a) > EPS);
		System.out.println(s);
	}
}