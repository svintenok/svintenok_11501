/**
* @author Svintenok Katya
* 11501
* 34b
*/
import java.util.Scanner;

public class Task34b {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int [] a1 = new int[n];
		int [] a2 = new int[n];
		
		
		for(int i = 0; i < n; i++)
			a1[i] = sc.nextInt();
		for(int i = 0; i < n; i++)
			a2[i] = sc.nextInt();
		
		for(int i = 0; i < n; i++) {
			a1[i] = a2[a1[i]];
			System.out.print(a1[i] + " ");
		}	
	}
}