/**
* @author Svintenok Katya
* 11501
* 15cInputArgs
*/

public class Task15cInputArgs {
	public static void main(String[] args) {
		int pEven = 1;
		int sOdd = 0;
		int s = 0;
		int p = 1;
		int k;
		for (int i = 0; i < args.length; i++) {
			k = Integer.parseInt(args[i]);
			if (k % 2 != 0) 
				sOdd = sOdd + k;
			else if (k != 0)
				pEven = pEven * k;
			if (k < 0)
				s = s + k;
			else if (k != 0)
				p = p * k;
		} 
		System.out.println(sOdd +  ", " + pEven + ", " + s + ", " + p);
	}
}
			