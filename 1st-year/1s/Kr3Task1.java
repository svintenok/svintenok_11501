import java.io.*;
import java.util.Scanner;

public class Kr3Task1 {
	public static void main(String [] args) throws FileNotFoundException {
		Scanner sc = new Scanner(new File("input3Task1.txt"));
		int n = sc.nextInt();
		int [] a = new int [n];
		
		for(int i = 0; i < n; i++) 
			a[i] = sc.nextInt();
	
		for (int i = 0; i < n; i++) {
			if (a[i] < n) {
				int k = i;
				boolean flag = false;
				while (!flag) {
					System.out.print(k);
					int b = a[k];
					flag = a[k] == i;
					a[k] = n;
					k = b;
				}
				System.out.println();
			}
		}
	}
}