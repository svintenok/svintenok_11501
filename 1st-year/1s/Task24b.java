/**
* @author Svintenok Katya
* 11501
* 24b
*/

public class Task24b {
	public static void main(String[] args) {
		final double EPS = 1e-8;
		int m = 0;
		double s = 0;
		double a;
		int z = 1;
		int k = 1;
		do {
			a = (double)(z) / (2 * m + 1) / k;
			s = s + a;
			k = k * 3;
			z = -z;
			m++;
		}
		while (Math.abs(a) > EPS);
		System.out.println(s);
	}
}