/**
* @author Svintenok Katya
* 11501
* 28c
*/

public class Task28c {
	public static void main(String[] args) {
		int i = 1;
		int x2;
		boolean flag = true;
		int x1 = Integer.parseInt(args[0]);
		while ((i < args.length) && (flag)){
			x2 = Integer.parseInt(args[i]);
			if (x2 <= x1)
				flag = false;
			else {
				i++;
				x1 = x2;
			}
		}
		System.out.println(flag);
	}
}