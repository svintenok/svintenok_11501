package ru.kpfu.itis.group501.svintenok.complexClasses;
import ru.kpfu.itis.group501.svintenok.basicClasses.*;

public class RationalComplexNumber {
	private RationalFraction x, y;
	
	public RationalComplexNumber(RationalFraction x, RationalFraction y) {
		
		this.x = x;
		this.y = y;
	}
	public RationalComplexNumber() {
		this (new RationalFraction(), new RationalFraction());
	}
	
	public RationalFraction getX() { 
		return x; 
	}
	public RationalFraction getY() { 
		return y;
	}
	
	public void setX(RationalFraction x) { 
		this.x = x;
	}
	public void setY(RationalFraction y) { 
		this.y = y;
	}
	
	public String toString() {
		if (y.getX() >= 0) { 
			return x.toString() + "+" + y.toString() + "*i";
		}
		else {
			return x.toString() + "" + y.toString() + "*i";
		}
	}
	
	public RationalComplexNumber add(RationalComplexNumber cn) {
		RationalComplexNumber cn2 = new RationalComplexNumber(this.x.add(cn.getX()), this.y.add(cn.getY()) );
		return cn2;
	}
	
	public RationalComplexNumber sub(RationalComplexNumber cn) {
		RationalComplexNumber cn2 = new RationalComplexNumber(this.x.sub(cn.getX()), this.y.sub(cn.getY()) );
		return cn2;
	}
	
	public RationalComplexNumber mult(RationalComplexNumber cn) {
		RationalComplexNumber cn2 = new RationalComplexNumber(this.x.mult(cn.getX() ).sub( this.y.mult(cn.getY() ) ) , this.x.mult(cn.getY() ).add( this.y.mult(cn.getX() ) ) );
		return cn2;
	}
	

	
	public static void main(String [] args) {
		
		RationalFraction rf1 = new RationalFraction(1, 2);
		RationalFraction rf2 = new RationalFraction(1, 3);
		RationalFraction rf3 = new RationalFraction(3, 4);
		
		RationalComplexNumber cn1 = new RationalComplexNumber(rf1, rf2);
		RationalComplexNumber cn2 = new RationalComplexNumber(rf1, rf3);
		
		System.out.println(cn1);
		System.out.println(cn2);
		
		RationalComplexNumber cn3 = cn1.add(cn2); 
		System.out.println(cn3);
		
		RationalComplexNumber cn4 = cn1.sub(cn2); 
		System.out.println(cn4);
		
		RationalComplexNumber cn5 = cn1.mult(cn2); 
		System.out.println(cn5);
	}
}