package ru.kpfu.itis.group501.svintenok.complexClasses;
import ru.kpfu.itis.group501.svintenok.basicClasses.*;

public class ComplexMatrix2x2 {
		
	private ComplexNumber [][] a = new ComplexNumber [2][2];
	
	public ComplexMatrix2x2(ComplexNumber a1, ComplexNumber a2, ComplexNumber a3, ComplexNumber a4) {
		a[0][0] = a1;
		a[0][1] = a2;
		a[1][0] = a3;
		a[1][1] = a4;
	}
	public ComplexMatrix2x2() {
		this (new ComplexNumber(), new ComplexNumber(), new ComplexNumber(), new ComplexNumber());
	}                                          
	public ComplexMatrix2x2(ComplexNumber [][] cn) {
		this.a = cn;
	}
	
	public ComplexNumber getA(int i, int j) { 
		return a[i][j]; 
	}
	
	public String toString() {
		String s = a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1] + "\n" + "=================";
		return s;
	}
	
	public ComplexMatrix2x2 add(ComplexMatrix2x2 mt) {
		ComplexMatrix2x2 mt2 = new ComplexMatrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.a[i][j] = a[i][j].add( mt.a[i][j]);
			
		return mt2;
	}
	
	public ComplexMatrix2x2 mult(ComplexMatrix2x2 mt) {
		ComplexMatrix2x2 mt2 = new ComplexMatrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.a[i][j] = a[i][0].mult( mt.a[0][j]).add( a[i][1].mult( mt.a[1][j] ) );
			
		return mt2;
	}
	
	public ComplexNumber det() {
		return a[1][1].mult( a[0][0]).sub( a[0][1].mult( a[1][0]) );
	}
	
	public ComplexVector2D multVector(ComplexVector2D vec){
		ComplexVector2D vec1 = new ComplexVector2D();
		
		vec1.setA(a[0][0].mult(vec.getA() ).add( a[0][1].mult( vec.getB()) ) );
		vec1.setB(a[1][0].mult(vec.getB() ).add( a[1][1].mult( vec.getB()) ) );
			
		return vec1;
	}

		
	public static void main(String [] args) {
		
		ComplexNumber cn1 = new ComplexNumber(1, 2);
		ComplexNumber cn2 = new ComplexNumber(1, 3);
		ComplexNumber cn3 = new ComplexNumber(3, 4);
		ComplexNumber cn4 = new ComplexNumber(5, 2);
		
		ComplexNumber [][] a = new ComplexNumber [][] {{cn1, cn2}, {cn3, cn4}};
		ComplexMatrix2x2 mt1 = new ComplexMatrix2x2(a);
		ComplexMatrix2x2 mt2 = new ComplexMatrix2x2(cn1, cn2, cn3, cn4);
		
		System.out.println(mt1);
		System.out.println(mt2);
		
		ComplexMatrix2x2 mt3 = mt1.add(mt2);
		System.out.println(mt3);
		
		ComplexMatrix2x2 mt4 = mt1.mult(mt2);
		System.out.println(mt4);
		
		System.out.println(mt2.det());
		
		
		ComplexVector2D vec1 = new ComplexVector2D(cn1, cn2);
		ComplexVector2D vec = mt2.multVector(vec1);
		System.out.println(vec);
	}
}