package ru.kpfu.itis.group501.svintenok;
import ru.kpfu.itis.group501.svintenok.basicClasses.*;
import ru.kpfu.itis.group501.svintenok.complexClasses.*;

public class Main {
	
	public static void main(String [] args) {
		
		
		Vector2D vec1 = new Vector2D(1.5, 0.5);
		Vector2D vec2 = new Vector2D(-1.5, 0.5);
		
		Vector2D vec3 = vec1.add(vec2); 
		System.out.println(vec3);
		
		vec1.add2(vec2);
		System.out.println(vec1);
		
		Vector2D vec4 = vec1.sub(vec2);
		System.out.println(vec4);
		
		vec1.sub2(vec2);
		System.out.println(vec1);
		
		Vector2D vec5 = vec1.mult(3.5);
		System.out.println(vec5);
		
		vec1.mult2(3.5);
		System.out.println(vec1);
		
		System.out.println(vec1.length());
		
		System.out.println(vec1.scalarProduct(vec2));
		
		System.out.println(vec1.cos(vec2));
		
		System.out.println(vec1.equals(vec2));
		
		//////////////////////////////////////////////////
		RationalFraction rf1 = new RationalFraction(5, 7);
		RationalFraction rf2 = new RationalFraction(3, 4);
		RationalFraction rf3 = new RationalFraction(3, 1);
		RationalFraction rf4 = new RationalFraction(5, 2);
		
		
		RationalFraction rf5 = rf1.add(rf2);
		System.out.println(rf5);
		
		rf1.add2(rf2);
		System.out.println(rf1);
		
		RationalFraction rf6 = rf1.sub(rf2);
		System.out.println(rf6);
		
		rf1.sub2(rf2);
		System.out.println(rf1);
		
		RationalFraction rf7 = rf1.mult(rf2);
		System.out.println(rf7);
		
		rf1.mult2(rf2);
		System.out.println(rf1);
		
		RationalFraction rf8 = rf1.div(rf2);
		System.out.println(rf8);
		
		rf1.div2(rf2);
		System.out.println(rf1);
		
		System.out.println(rf1.value());
		
		System.out.println(rf1.equals(rf2));
		
		System.out.println(rf1.numberPart());
	
		/////////////////////////////////////////////
		ComplexNumber cn1 = new ComplexNumber(1, 2);
		ComplexNumber cn2 = new ComplexNumber(3, 4);
		ComplexNumber cn3 = new ComplexNumber(3, 1);
		ComplexNumber cn4 = new ComplexNumber(5, 2);
		
		ComplexNumber cn5 = cn1.add(cn2);
		System.out.println(cn5);
		
		cn1.add2(cn2);
		System.out.println(cn1);
		
		ComplexNumber cn6 = cn1.sub(cn2);
		System.out.println(cn6);
		
		cn1.sub2(cn2);
		System.out.println(cn1);
		
		ComplexNumber cn7 = cn1.mult(cn2);
		System.out.println(cn7);
		
		cn1.mult2(cn2);
		System.out.println(cn1);
		
		ComplexNumber cn8 = cn1.multNumber(2);
		System.out.println(cn8);
		
		cn1.multNumber2(2);
		System.out.println(cn1);
		
		ComplexNumber cn9 = cn1.div(cn2);
		System.out.println(cn9);
		
		cn1.div2(cn2);
		System.out.println(cn1);
		
		System.out.println(cn1.length());
		
		System.out.println(cn1.arg());
		
		ComplexNumber cn10 = cn1.pow(3);
		System.out.println(cn10);
		
		System.out.println(cn1.equals(cn2));
		
		///////////////////////////////////////
		double [][] a = new double [][] {{1, 2}, {3, 4}};
		Matrix2x2 mt1 = new Matrix2x2(a);
		Matrix2x2 mt2 = new Matrix2x2(2, 3, 4, 5);
		
		System.out.println(mt1);
		System.out.println(mt2);
		
		Matrix2x2 mt3 = mt1.add(mt2);
		System.out.println(mt3);
		
		mt1.add2(mt2);
		System.out.println(mt1);
		
		Matrix2x2 mt4 = mt1.sub(mt2);
		System.out.println(mt4);
		
		mt1.sub2(mt2);
		System.out.println(mt1);
		
		Matrix2x2 mt5 = mt1.mult(mt2);
		System.out.println(mt5);
		
		mt1.mult2(mt2);
		System.out.println(mt1);
		
		Matrix2x2 mt6 = mt1.multNumber(0.5);
		System.out.println(mt6);
		
		mt1.multNumber2(0.5);
		System.out.println(mt1);
		
		System.out.println(mt2.det());
		
		mt2.transpon();
		System.out.println(mt2);
		
		Matrix2x2 mt7 = mt2.inverseMatrix();
		System.out.println(mt7);
		
		Matrix2x2 mt8 = mt2.equivalentDiagonal();
		System.out.println(mt8);
		
		
		Vector2D vec59 = mt2.multVector(vec1);
		System.out.println(vec59);
		
		System.out.println(mt1);
		
		///////////////////////////////////////
	
		RationalVector2D rvec1 = new RationalVector2D(rf1, rf2);
		RationalVector2D rvec2 = new RationalVector2D(rf1, rf3);
		
		System.out.println(rvec1);
		System.out.println(rvec2);
		
		RationalVector2D rvec3 = rvec1.add(rvec2); 
		System.out.println(rvec3);
		
		System.out.println(rvec1.length());
		
		RationalFraction rf60 = rvec1.scalarProduct(rvec2); 
		System.out.println(rf60);
		
		System.out.println(rvec1.equals(rvec2));
		
		////////////////////////////////////////////	
		ComplexVector2D cvec1 = new ComplexVector2D(cn1, cn2);
		ComplexVector2D cvec2 = new ComplexVector2D(cn1, cn3);
		
		System.out.println(cvec1);
		System.out.println(cvec2);
		
		ComplexVector2D cvec3 = cvec1.add(cvec2); 
		System.out.println(vec3);
		
		ComplexNumber cn61 = cvec1.scalarProduct(cvec2);
		System.out.println(cn61);
		
		System.out.println(cvec1.equals(cvec2));
		
		////////////////////////////////////////////		

		
		RationalFraction [][] a62 = new RationalFraction [][] {{rf1, rf2}, {rf3, rf4}};
		RationalMatrix2x2 rmt1 = new RationalMatrix2x2(a62);
		RationalMatrix2x2 rmt2 = new RationalMatrix2x2(rf1, rf2, rf3, rf4);
		
		System.out.println(rmt1);
		System.out.println(rmt2);
	
		RationalMatrix2x2 rmt3 = rmt1.add(rmt2);
		System.out.println(rmt3);
		
		RationalMatrix2x2 rmt4 = rmt1.mult(rmt2);
		System.out.println(rmt4);
		
		System.out.println(rmt2.det());
		
		
		RationalVector2D vec62 = rmt2.multVector(rvec1);
		System.out.println(vec62);
		
		/////////////////////////////////////////////////
		
		ComplexNumber [][] a63 = new ComplexNumber [][] {{cn1, cn2}, {cn3, cn4}};
		ComplexMatrix2x2 cmt1 = new ComplexMatrix2x2(a63);
		ComplexMatrix2x2 cmt2 = new ComplexMatrix2x2(cn1, cn2, cn3, cn4);
		
		System.out.println(cmt1);
		System.out.println(cmt2);
		
		ComplexMatrix2x2 cmt3 = cmt1.add(cmt2);
		System.out.println(cmt3);
		
		ComplexMatrix2x2 cmt4 = cmt1.mult(cmt2);
		System.out.println(cmt4);
		
		System.out.println(cmt2.det());
		
		
		ComplexVector2D vec63 = cmt2.multVector(cvec1);
		System.out.println(vec63);
		
		///////////////////////////////////////////////////
		
		RationalComplexNumber rcn1 = new RationalComplexNumber(rf1, rf2);
		RationalComplexNumber rcn2 = new RationalComplexNumber(rf1, rf3);
		RationalComplexNumber rcn3 = new RationalComplexNumber(rf1, rf4);
		RationalComplexNumber rcn4 = new RationalComplexNumber(rf4, rf2);
		
		System.out.println(rcn1);
		System.out.println(rcn2);
		
		RationalComplexNumber rcn5 = rcn1.add(rcn2); 
		System.out.println(rcn5);
		
		RationalComplexNumber rcn6 = rcn1.sub(rcn2); 
		System.out.println(rcn6);
		
		RationalComplexNumber rcn7 = rcn1.mult(rcn2); 
		System.out.println(rcn7);
		
		//////////////////////////////////////////////////////
		
		
		RationalComplexVector2D rcvec1 = new RationalComplexVector2D(rcn1, rcn2);
		RationalComplexVector2D rcvec2 = new RationalComplexVector2D(rcn1, rcn3);
		
		System.out.println(rcvec1);
		System.out.println(rcvec2);
		
		RationalComplexVector2D rcvec3 = rcvec1.add(rcvec2); 
		System.out.println(rcvec3);
		
		RationalComplexNumber rcn65 = rcvec1.scalarProduct(rcvec2);
		System.out.println(rcn65);
		
		////////////////////////////////////////////////////
		
	
		
		RationalComplexMatrix2x2 rcmt1 = new RationalComplexMatrix2x2(rcn1);
		RationalComplexMatrix2x2 rcmt2 = new RationalComplexMatrix2x2(rcn1, rcn2, rcn3, rcn4);
		
		System.out.println(rcmt1);
		System.out.println(rcmt2);
		
		RationalComplexMatrix2x2 rcmt3 = rcmt1.add(rcmt2);
		System.out.println(rcmt3);
		
		RationalComplexMatrix2x2 rcmt4 = rcmt1.mult(rcmt2);
		System.out.println(rcmt4);
		
		System.out.println(rcmt2.det());
		
	
		RationalComplexVector2D rcvec66 = rcmt2.multVector(rcvec1);
		System.out.println(rcvec66);
	}
}