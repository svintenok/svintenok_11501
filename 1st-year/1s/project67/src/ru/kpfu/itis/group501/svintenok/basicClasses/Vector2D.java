package ru.kpfu.itis.group501.svintenok.basicClasses;

public class Vector2D {
	
	private double x, y;
	
	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public Vector2D() {
		this (0, 0);
	}
	
	public double getX() { 
		return x; 
	}
	public double getY() { 
		return y;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	public void setY(double y) {
		this.y = y;
	}
		
	public String toString() {
		return "(" + x + "," + y + ")" ;
	}
	
	public Vector2D add(Vector2D vec) {
		return new Vector2D(this.x + vec.getX(), this.y + vec.getY());
	}
	public void add2(Vector2D vec) {
		this.x = this.x + vec.getX();
		this.y = this.y + vec.getY();
	}
	
	public Vector2D sub(Vector2D vec) {
		return new Vector2D(this.x - vec.getX(), this.y - vec.getY());
	}
	public void sub2(Vector2D vec) {
		this.x = this.x - vec.getX();
		this.y = this.y - vec.getY();
	}
	
	public Vector2D mult(double c) {
		return new Vector2D(this.x * c, this.y * c);
	}
	public void mult2(double c) {
		this.x = this.x * c;
		this.y = this.y * c;
	}
	
	public double length() {
		return Math.sqrt(x * x + y * y);
	}
	
	public double scalarProduct(Vector2D vec) {
		return this.x * vec.getX() + this.y * vec.getY();
	}
	
	public double cos(Vector2D vec) {
		return this.scalarProduct(vec) / (this.length() * vec.length() );
	}
	
	public boolean equals(Vector2D vec) {
		return (this.x == vec.getX()) && (this.y == vec.getY());
	}
	
	public static void main(String [] args) {
		
		
		Vector2D vec1 = new Vector2D(1.5, 0.5);
		Vector2D vec2 = new Vector2D(-1.5, 0.5);
		
		Vector2D vec3 = vec1.add(vec2); 
		System.out.println(vec3);
		
		vec1.add2(vec2);
		System.out.println(vec1);
		
		Vector2D vec4 = vec1.sub(vec2);
		System.out.println(vec4);
		
		vec1.sub2(vec2);
		System.out.println(vec1);
		
		Vector2D vec5 = vec1.mult(3.5);
		System.out.println(vec5);
		
		vec1.mult2(3.5);
		System.out.println(vec1);
		
		System.out.println(vec1.length());
		
		System.out.println(vec1.scalarProduct(vec2));
		
		System.out.println(vec1.cos(vec2));
		
		System.out.println(vec1.equals(vec2));
	}
}