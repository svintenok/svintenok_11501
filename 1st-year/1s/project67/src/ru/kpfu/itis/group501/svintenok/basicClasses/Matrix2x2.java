package ru.kpfu.itis.group501.svintenok.basicClasses;

public class Matrix2x2 {
	
	private double[][] a = new double [2][2];
	
	public Matrix2x2(double[][] a) {
				this.a = a;
	}
	public Matrix2x2(double a1, double a2, double a3, double a4) {
		a[0][0] = a1;
		a[0][1] = a2;
		a[1][0] = a3;
		a[1][1] = a4;
	}
	public Matrix2x2() {
		this (0, 0, 0, 0);
	}                                          
	public Matrix2x2(double c) {
		for (int i = 0; i < a.length; i++)
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = c;
	}
	
	public double getA(int i, int j) { 
		return a[i][j]; 
	}
	
	public void setA(double x, int i, int j) {
		a[i][j] = x;
	}
	
	public String toString() {
		String s = a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1] + "\n" + "=================";
		return s;
	}
	
	public Matrix2x2 add(Matrix2x2 mt) {
		Matrix2x2 mt2 = new Matrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.setA(a[i][j] + mt.getA(i,j), i, j);
			
		return mt2;
	}
	public void add2(Matrix2x2 mt) {
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = a[i][j] + mt.getA(i,j);

	}
	
	public Matrix2x2 sub(Matrix2x2 mt) {
		Matrix2x2 mt2 = new Matrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.setA(a[i][j] - mt.getA(i,j), i, j);
			
		return mt2;
	}
	public void sub2(Matrix2x2 mt) {
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = a[i][j] - mt.getA(i,j);

	}
	
	public Matrix2x2 mult(Matrix2x2 mt) {
		Matrix2x2 mt2 = new Matrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.setA(a[i][0] * mt.getA(0, j) + a[i][1] * mt.getA(1, j), i, j);
			
		return mt2;
	}
	public void mult2(Matrix2x2 mt) {

		for (int i = 0; i < a.length; i++) {
			double a1 = a[i][0];
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = a1 * mt.getA(0, j) + a[i][1] * mt.getA(1, j);
		}

	}
	
	public Matrix2x2 multNumber(double c) {
		Matrix2x2 mt2 = new Matrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.setA(a[i][j] * c, i, j);
			
		return mt2;
	}
	public void multNumber2(double c) {

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = a[i][j] * c;
		}
	}
	
	public double det() {
		return a[1][1] * a[0][0] - a[0][1] * a[1][0];
	}
	
	public void transpon() {
		double a1 = a[0][1];
		a[0][1] =  a[1][0];
		a[1][0] =  a1;
	}
	
	
	public Matrix2x2 inverseMatrix() {
		
		double d = this.det();
		if (d != 0) {
			Matrix2x2 mt = new Matrix2x2(a[1][1], -a[1][0], -a[0][1], a[0][0]);
		
			mt.transpon();
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < a[i].length; j++)
					mt.a[i][j] = mt.a[i][j] / d;
			}
			return mt;
		}
		else {
			System.out.println("ошибка");
			return new Matrix2x2();
		}	
	}
	
	public Matrix2x2 equivalentDiagonal() {
		
		Matrix2x2 mt = new Matrix2x2(a[0][0],0 ,0 , a[1][1] - a[1][0] * a[0][1] / a[0][0]);
		return mt;
	}
	
	public Vector2D multVector(Vector2D vec){
		Vector2D vec1 = new Vector2D();
		
		vec1.setX(a[0][0] * vec.getX() + a[0][1] * vec.getY());
		vec1.setY(a[1][0] * vec.getX() + a[1][1] * vec.getY());
			
		return vec1;
		
	}
	
	public static void main(String [] args) {
		
		double [][] a = new double [][] {{1, 2}, {3, 4}};
		Matrix2x2 mt1 = new Matrix2x2(a);
		Matrix2x2 mt2 = new Matrix2x2(2, 3, 4, 5);
		
		System.out.println(mt1);
		System.out.println(mt2);
		
		Matrix2x2 mt3 = mt1.add(mt2);
		System.out.println(mt3);
		
		mt1.add2(mt2);
		System.out.println(mt1);
		
		Matrix2x2 mt4 = mt1.sub(mt2);
		System.out.println(mt4);
		
		mt1.sub2(mt2);
		System.out.println(mt1);
		
		Matrix2x2 mt5 = mt1.mult(mt2);
		System.out.println(mt5);
		
		mt1.mult2(mt2);
		System.out.println(mt1);
		
		Matrix2x2 mt6 = mt1.multNumber(0.5);
		System.out.println(mt6);
		
		mt1.multNumber2(0.5);
		System.out.println(mt1);
		
		System.out.println(mt2.det());
		
		mt2.transpon();
		System.out.println(mt2);
		
		Matrix2x2 mt7 = mt2.inverseMatrix();
		System.out.println(mt7);
		
		Matrix2x2 mt8 = mt2.equivalentDiagonal();
		System.out.println(mt8);
		
		Vector2D vec1 = new Vector2D(2, 3);
		Vector2D vec = mt2.multVector(vec1);
		System.out.println(vec);
		
		System.out.println(mt1);
	}
}