package ru.kpfu.itis.group501.svintenok.complexClasses;
import ru.kpfu.itis.group501.svintenok.basicClasses.*;

public class RationalComplexVector2D {
	private RationalComplexNumber a, b;
	
	public RationalComplexVector2D(RationalComplexNumber a, RationalComplexNumber b) {
		this.a = a;
		this.b = b;
	}
	
	public RationalComplexVector2D() {
		this (new RationalComplexNumber(), new RationalComplexNumber());
	}
	
	public RationalComplexNumber getA() { 
		return a; 
	}
	public RationalComplexNumber getB() { 
		return b;
	}
	
	public void setA(RationalComplexNumber a) { 
		this.a = a;
	}
	public void setB(RationalComplexNumber	b) { 
		this.b = b;
	}
	
	public String toString() {
		return "(" + a.toString() + "," + b.toString() + ")" ;
	}
	
	public RationalComplexVector2D add(RationalComplexVector2D vec) {
		return new RationalComplexVector2D( this.a.add(vec.getA()), this.b.add(vec.getB()) );
	}
	
	public RationalComplexNumber scalarProduct(RationalComplexVector2D vec) {
		return this.a.mult(vec.getA()).add( this.b.mult(vec.getB()) );
	}
	
	public static void main(String [] args) {
		
		RationalFraction rf1 = new RationalFraction(1, 2);
		RationalFraction rf2 = new RationalFraction(1, 3);
		RationalFraction rf3 = new RationalFraction(3, 4);
		RationalFraction rf4 = new RationalFraction(5, 3);
		
		RationalComplexNumber cn1 = new RationalComplexNumber(rf1, rf2);
		RationalComplexNumber cn2 = new RationalComplexNumber(rf1, rf3);
		RationalComplexNumber cn3 = new RationalComplexNumber(rf1, rf4);
		
		RationalComplexVector2D vec1 = new RationalComplexVector2D(cn1, cn2);
		RationalComplexVector2D vec2 = new RationalComplexVector2D(cn1, cn3);
		
		System.out.println(vec1);
		System.out.println(vec2);
		
		RationalComplexVector2D vec3 = vec1.add(vec2); 
		System.out.println(vec3);
		
		RationalComplexNumber cn4 = vec1.scalarProduct(vec2);
		System.out.println(cn4);
	}
}