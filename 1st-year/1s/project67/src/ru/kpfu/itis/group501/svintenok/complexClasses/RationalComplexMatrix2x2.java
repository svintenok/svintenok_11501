package ru.kpfu.itis.group501.svintenok.complexClasses;
import ru.kpfu.itis.group501.svintenok.basicClasses.*;

public class RationalComplexMatrix2x2 {
	
	private RationalComplexNumber [][] a = new RationalComplexNumber [2][2];
	
	public RationalComplexMatrix2x2(RationalComplexNumber a1, RationalComplexNumber a2, RationalComplexNumber a3, RationalComplexNumber a4) {
		a[0][0] = a1;
		a[0][1] = a2;
		a[1][0] = a3;
		a[1][1] = a4;
	}
	public RationalComplexMatrix2x2() {
		this (new RationalComplexNumber(), new RationalComplexNumber(), new RationalComplexNumber(), new RationalComplexNumber());
	}       
	public RationalComplexMatrix2x2(RationalComplexNumber c) {
		for (int i = 0; i < a.length; i++)
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = c;
	}
	
	public RationalComplexNumber getA(int i, int j) { 
		return a[i][j]; 
	}
	
	public String toString() {
		String s = a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1] + "\n" + "=================";
		return s;
	}
	
	public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 mt) {
		RationalComplexMatrix2x2 mt2 = new RationalComplexMatrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.a[i][j] = a[i][j].add( mt.a[i][j]);
			
		return mt2;
	}
	
	public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 mt) {
		RationalComplexMatrix2x2 mt2 = new RationalComplexMatrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.a[i][j] = a[i][0].mult( mt.a[0][j]).add( a[i][1].mult( mt.a[1][j] ) );
			
		return mt2;
	}
	
	public RationalComplexNumber det() {
		return a[1][1].mult( a[0][0]).sub( a[0][1].mult( a[1][0]) );
	}
	
	public RationalComplexVector2D multVector(RationalComplexVector2D vec){
		RationalComplexVector2D vec1 = new RationalComplexVector2D();
		
		vec1.setA(a[0][0].mult(vec.getA() ).add( a[0][1].mult( vec.getB()) ) );
		vec1.setB(a[1][0].mult(vec.getA() ).add( a[1][1].mult( vec.getB()) ) );
		
		return vec1;
	}
		
	
	public static void main(String [] args) {
		
		RationalFraction rf1 = new RationalFraction(1, 2);
		RationalFraction rf2 = new RationalFraction(1, 3);
		RationalFraction rf3 = new RationalFraction(3, 4);
		RationalFraction rf4 = new RationalFraction(5, 3);
		
		RationalComplexNumber cn1 = new RationalComplexNumber(rf1, rf2);
		RationalComplexNumber cn2 = new RationalComplexNumber(rf1, rf3);
		RationalComplexNumber cn3 = new RationalComplexNumber(rf1, rf4);
		RationalComplexNumber cn4 = new RationalComplexNumber(rf4, rf2);
		
		RationalComplexMatrix2x2 mt1 = new RationalComplexMatrix2x2(cn1);
		RationalComplexMatrix2x2 mt2 = new RationalComplexMatrix2x2(cn1, cn2, cn3, cn4);
		
		System.out.println(mt1);
		System.out.println(mt2);
		
		RationalComplexMatrix2x2 mt3 = mt1.add(mt2);
		System.out.println(mt3);
		
		RationalComplexMatrix2x2 mt4 = mt1.mult(mt2);
		System.out.println(mt4);
		
		System.out.println(mt2.det());
		
		RationalComplexVector2D vec1 = new RationalComplexVector2D(cn1, cn2);
		RationalComplexVector2D vec = mt2.multVector(vec1);
		System.out.println(vec);
	}
	
}