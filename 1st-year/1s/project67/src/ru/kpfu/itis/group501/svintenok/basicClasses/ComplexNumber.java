package ru.kpfu.itis.group501.svintenok.basicClasses;

public class ComplexNumber {
	
	private double x, y;
	
	public ComplexNumber(double x, double y) {
		this.x = x;
		this.y = y;
	}
	public ComplexNumber() {
		this (0, 0);
	}
	
	public double getX() { 
		return x; 
	}
	public double getY() { 
		return y;
	}
	
	public void setX(double x) {
		this.x = x;
	}
	public void setY(double y) {
		this.y = y;
	}
		
	public String toString() {
		if (y >= 0) { 
			return x + "+" + y + "*i";
		}
		else {
			return x + "" + y + "*i";
		}
	}
	
	public ComplexNumber add(ComplexNumber cn) {
		ComplexNumber cn2 = new ComplexNumber(this.x + cn.getX(), this.y + cn.getY() );
		return cn2;
	}
	
	public void add2(ComplexNumber cn) {
		this.x = this.x + cn.getX();
		this.y = this.y + cn.getY();
	}
	
	public ComplexNumber sub(ComplexNumber cn) {
		ComplexNumber cn2 = new ComplexNumber(this.x - cn.getX(), this.y - cn.getY() );
		return cn2;
	}
	
	public void sub2(ComplexNumber cn) {
		this.x = this.x - cn.getX();
		this.y = this.y - cn.getY();
	}
	
	public ComplexNumber mult(ComplexNumber cn) {
		ComplexNumber cn2 = new ComplexNumber(this.x * cn.getX() - this.y * cn.getY(), this.x * cn.getY() + this.y * cn.getX() );
		return cn2;
	}
	
	public void mult2(ComplexNumber cn) {
		double z = this.x;
		this.x = this.x * cn.getX() - this.y * cn.getY();
		this.y = z * cn.getY() + this.y * cn.getX();
	}
	
	public ComplexNumber multNumber(double c) {
		ComplexNumber cn2 = new ComplexNumber(this.x * c, this.y * c);
		return cn2;
	}
	
	public void multNumber2(double c) {
		this.x = this.x * c;
		this.y = this.y * c;
	}
		
	public ComplexNumber div(ComplexNumber cn) {
		ComplexNumber cn2 = new ComplexNumber( (this.x * cn.getX() + this.y * cn.getY()) / (cn.getX() * cn.getX() + cn.getY() * cn.getY()), (this.y * cn.getX() + this.x * cn.getY()) / (cn.getX() * cn.getX() + cn.getY() * cn.getY()));
		return cn2;
	}
	
	public void div2(ComplexNumber cn) {
		double z = this.x;
		this.x = (this.x * cn.getX() + this.y * cn.getY()) / (cn.getX() * cn.getX() + cn.getY() * cn.getY());
		this.y = (this.y * cn.getX() + z * cn.getY()) / (cn.getX() * cn.getX() + cn.getY() * cn.getY());
	}

	public double length() {
		return Math.sqrt(this.x * this.x + this.y * this.y);
	}
	
	public double arg() {
		return Math.atan(this.y / this.x);
	}
	
	public ComplexNumber pow(double c) {
		ComplexNumber cn2 = new ComplexNumber();
		cn2.setX( Math.pow(this.length(), c) * Math.cos(this.arg() * c) );
		cn2.setY( Math.pow(this.length(), c) * Math.sin(this.arg() * c) );
		return cn2;
	}
	
	public boolean equals(ComplexNumber cn) {
		return (this.x == cn.getX()) && (this.y == cn.getY());
	}
	
	
	
	public static void main(String [] args) {
		
		ComplexNumber cn1 = new ComplexNumber(1, 2);
		ComplexNumber cn2 = new ComplexNumber(3, 4);
		
		ComplexNumber cn3 = cn1.add(cn2);
		System.out.println(cn3);
		
		cn1.add2(cn2);
		System.out.println(cn1);
		
		ComplexNumber cn4 = cn1.sub(cn2);
		System.out.println(cn4);
		
		cn1.sub2(cn2);
		System.out.println(cn1);
		
		ComplexNumber cn5 = cn1.mult(cn2);
		System.out.println(cn5);
		
		cn1.mult2(cn2);
		System.out.println(cn1);
		
		ComplexNumber cn6 = cn1.multNumber(2);
		System.out.println(cn6);
		
		cn1.multNumber2(2);
		System.out.println(cn1);
		
		ComplexNumber cn7 = cn1.div(cn2);
		System.out.println(cn7);
		
		cn1.div2(cn2);
		System.out.println(cn1);
		
		System.out.println(cn1.length());
		
		System.out.println(cn1.arg());
		
		ComplexNumber cn8 = cn1.pow(3);
		System.out.println(cn8);
		
		System.out.println(cn1.equals(cn2));

	}
}