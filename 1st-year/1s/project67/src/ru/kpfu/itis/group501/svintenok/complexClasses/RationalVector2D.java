package ru.kpfu.itis.group501.svintenok.complexClasses;
import ru.kpfu.itis.group501.svintenok.basicClasses.*;

public class RationalVector2D {
	private RationalFraction x, y;
	
	public RationalVector2D(RationalFraction x, RationalFraction y) {
		
		this.x = x;
		this.y = y;
	}
	public RationalVector2D() {
		this (new RationalFraction(), new RationalFraction());
	}
	
	public RationalFraction getX() { 
		return x; 
	}
	public RationalFraction getY() { 
		return y;
	}
	
	public void setX(RationalFraction x) { 
		this.x = x;
	}
	public void setY(RationalFraction y) { 
		this.y = y;
	}
		
	public String toString() {
		return "(" + x.toString() + "," + y.toString() + ")" ;
	}
	
	public RationalVector2D add(RationalVector2D vec) {
		return new RationalVector2D( this.x.add(vec.getX()), this.y.add(vec.getY()) );
	}
	
	public double length() {
		RationalFraction rf1 = x.mult(x);
		RationalFraction rf2 = y.mult(y);
	
		RationalFraction rf3 = rf1.add(rf2);

		return Math.sqrt(rf3.value());
	}
	
	public RationalFraction scalarProduct(RationalVector2D vec) {
		return this.x.mult(vec.getX()).add( this.y.mult(vec.getY()) );
	}

	public boolean equals(RationalVector2D vec) {
		return this.x.equals(vec.getX()) && this.y.equals(vec.getY());
	}

	
	public static void main(String [] args) {
		
		RationalFraction rf1 = new RationalFraction(1, 2);
		RationalFraction rf2 = new RationalFraction(1, 3);
		RationalFraction rf3 = new RationalFraction(3, 4);
		
		RationalVector2D vec1 = new RationalVector2D(rf1, rf2);
		RationalVector2D vec2 = new RationalVector2D(rf1, rf3);
		
		System.out.println(vec1);
		System.out.println(vec2);
		
		RationalVector2D vec3 = vec1.add(vec2); 
		System.out.println(vec3);
		
		System.out.println(vec1.length());
		
		RationalFraction rf4 = vec1.scalarProduct(vec2); 
		System.out.println(rf4);
		
		System.out.println(vec1.equals(vec2));
		
	}
	
}