package ru.kpfu.itis.group501.svintenok.complexClasses;
import ru.kpfu.itis.group501.svintenok.basicClasses.*;

public class ComplexVector2D {
	
	private ComplexNumber a, b;
	
	public ComplexVector2D(ComplexNumber a, ComplexNumber b) {
		this.a = a;
		this.b = b;
	}
	
	public ComplexVector2D() {
		this (new ComplexNumber(), new ComplexNumber());
	}
	
	public ComplexNumber getA() { 
		return a; 
	}
	public ComplexNumber getB() { 
		return b;
	}
	
	public void setA(ComplexNumber a) { 
		this.a = a;
	}
	public void setB(ComplexNumber b) { 
		this.b = b;
	}
	
	public String toString() {
		return "(" + a.toString() + "," + b.toString() + ")" ;
	}
	
	public ComplexVector2D add(ComplexVector2D vec) {
		return new ComplexVector2D( this.a.add(vec.getA()), this.b.add(vec.getB()) );
	}
	
	public ComplexNumber scalarProduct(ComplexVector2D vec) {
		return this.a.mult(vec.getA()).add( this.b.mult(vec.getB()) );
	}

	public boolean equals(ComplexVector2D vec) {
		return this.a.equals(vec.getA()) && this.b.equals(vec.getB());
	}

	
	
	
	public static void main(String [] args) {
		
		ComplexNumber cn1 = new ComplexNumber(1, 2);
		ComplexNumber cn2 = new ComplexNumber(3, 4);
		ComplexNumber cn3 = new ComplexNumber(3, 1);
		
		ComplexVector2D vec1 = new ComplexVector2D(cn1, cn2);
		ComplexVector2D vec2 = new ComplexVector2D(cn1, cn3);
		
		System.out.println(vec1);
		System.out.println(vec2);
		
		ComplexVector2D vec3 = vec1.add(vec2); 
		System.out.println(vec3);
		
		ComplexNumber cn4 = vec1.scalarProduct(vec2);
		System.out.println(cn4);
		
		System.out.println(vec1.equals(vec2));
	}

}