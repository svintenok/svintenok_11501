package ru.kpfu.itis.group501.svintenok.complexClasses;
import ru.kpfu.itis.group501.svintenok.basicClasses.*;

public class RationalMatrix2x2 {
		
	private RationalFraction [][] a = new RationalFraction [2][2];
	
	public RationalMatrix2x2(RationalFraction a1, RationalFraction a2, RationalFraction a3, RationalFraction a4) {
		a[0][0] = a1;
		a[0][1] = a2;
		a[1][0] = a3;
		a[1][1] = a4;
	}
	public RationalMatrix2x2() {
		this (new RationalFraction(), new RationalFraction(), new RationalFraction(), new RationalFraction());
	}                                          
	public RationalMatrix2x2(RationalFraction [][] rf) {
		this.a = rf;
	}
	
	public RationalFraction getA(int i, int j) { 
		return a[i][j]; 
	}
	
	public String toString() {
		String s = a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1] + "\n" + "=================";
		return s;
	}
	
	public RationalMatrix2x2 add(RationalMatrix2x2 mt) {
		RationalMatrix2x2 mt2 = new RationalMatrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.a[i][j] = a[i][j].add( mt.a[i][j]);
			
		return mt2;
	}
	
	public RationalMatrix2x2 mult(RationalMatrix2x2 mt) {
		RationalMatrix2x2 mt2 = new RationalMatrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.a[i][j] = a[i][0].mult( mt.a[0][j]).add( a[i][1].mult( mt.a[1][j] ) );
			
		return mt2;
	}
	
	public RationalFraction det() {
		return a[1][1].mult( a[0][0]).sub( a[0][1].mult( a[1][0]) );
	}
	
	public RationalVector2D multVector(RationalVector2D vec){
		RationalVector2D vec1 = new RationalVector2D();
		
		vec1.setX(a[0][0].mult(vec.getX() ).add( a[0][1].mult( vec.getY()) ) );
		vec1.setY(a[1][0].mult(vec.getX() ).add( a[1][1].mult( vec.getY()) ) );
			
		return vec1;
	}

		
	public static void main(String [] args) {
		
		RationalFraction rf1 = new RationalFraction(1, 2);
		RationalFraction rf2 = new RationalFraction(1, 3);
		RationalFraction rf3 = new RationalFraction(3, 4);
		RationalFraction rf4 = new RationalFraction(5, 2);
		
		RationalFraction [][] a = new RationalFraction [][] {{rf1, rf2}, {rf3, rf4}};
		RationalMatrix2x2 mt1 = new RationalMatrix2x2(a);
		RationalMatrix2x2 mt2 = new RationalMatrix2x2(rf1, rf2, rf3, rf4);
		
		System.out.println(mt1);
		System.out.println(mt2);
	
		RationalMatrix2x2 mt3 = mt1.add(mt2);
		System.out.println(mt3);
		
		RationalMatrix2x2 mt4 = mt1.mult(mt2);
		System.out.println(mt4);
		
		System.out.println(mt2.det());
		
		
		RationalVector2D vec1 = new RationalVector2D(rf1, rf2);
		RationalVector2D vec = mt2.multVector(vec1);
		System.out.println(vec);
	}
}