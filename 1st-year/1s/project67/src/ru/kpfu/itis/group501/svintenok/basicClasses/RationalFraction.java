package ru.kpfu.itis.group501.svintenok.basicClasses;

public class RationalFraction {
	
	private int x, y;
	
	public RationalFraction(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public RationalFraction() {
		this (0, 0);
	}
	
	public int getX() { 
		return x; 
	}
	public int getY() { 
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}
		
	public String toString() {
		return x + "/" + y;
	}
	
	public void reduce() {
		int i = 2;
		while(i <= this.x && i <= this.y ) {
			if ((this.x % i == 0) && (this.y % i == 0)) {
				this.x = this.x / i;
				this.y = this.y / i;
			}
			else
				i++;
		}
		if (y < 0) {
			this.x = -this.x;
			this.y = -this.y;
		}
	}
	
	public RationalFraction add(RationalFraction rf) {
		RationalFraction rf2 = new RationalFraction(this.x * rf.getY() + rf.getX() * this.y, this.y * rf.getY() );
		rf2.reduce();
		return rf2;
	}
	public void add2(RationalFraction rf) {
		this.x = this.x * rf.getY() + rf.getX() * this.y;
		this.y = this.y * rf.getY();
		this.reduce();
	}
	
	public RationalFraction sub(RationalFraction rf) {
		RationalFraction rf2 = new RationalFraction(this.x * rf.getY() - rf.getX() * this.y, this.y * rf.getY() );
		rf2.reduce();
		return rf2;
	}
	public void sub2(RationalFraction rf) {
		this.x = this.x * rf.getY() - rf.getX() * this.y;
		this.y = this.y * rf.getY();
		this.reduce();
	}
	
	public RationalFraction mult(RationalFraction rf) {
		RationalFraction rf2 = new RationalFraction(this.x * rf.getX(), this.y * rf.getY() );
		rf2.reduce();
		return rf2;
	}
	public void mult2(RationalFraction rf) {
		this.x = this.x * rf.getX();
		this.y = this.y * rf.getY();
		this.reduce();
	}
	
	public RationalFraction div(RationalFraction rf) {
		RationalFraction rf2 = new RationalFraction(this.x * rf.getY(), this.y * rf.getX() );
		rf2.reduce();
		return rf2;
	}
	public void div2(RationalFraction rf) {
		this.x = this.x * rf.getY();
		this.y = this.y * rf.getX();
		this.reduce();
	}
	
	public double value() {
		return (double)(x)/y;
	} 
	
	public boolean equals(RationalFraction rf) {
		this.reduce();
		rf.reduce();
		return (this.x == rf.getX()) && (this.y == rf.getY());
	}
	
	public int numberPart() {
		return x/y;
	}
	
	public static void main(String [] args) {
		
		
		RationalFraction rf1 = new RationalFraction(5, 7);
		RationalFraction rf2 = new RationalFraction(3, 4);
		
		RationalFraction rf3 = rf1.add(rf2);
		System.out.println(rf3);
		
		rf1.add2(rf2);
		System.out.println(rf1);
		
		RationalFraction rf4 = rf1.sub(rf2);
		System.out.println(rf4);
		
		rf1.sub2(rf2);
		System.out.println(rf1);
		
		RationalFraction rf5 = rf1.mult(rf2);
		System.out.println(rf5);
		
		rf1.mult2(rf2);
		System.out.println(rf1);
		
		RationalFraction rf6 = rf1.div(rf2);
		System.out.println(rf6);
		
		rf1.div2(rf2);
		System.out.println(rf1);
		
		System.out.println(rf1.value());
		
		System.out.println(rf1.equals(rf2));
		
		System.out.println(rf1.numberPart());

	}
}