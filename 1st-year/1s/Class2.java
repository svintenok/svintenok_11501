import java.util.Scanner;

public class Class2 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		int [] a = new int[n];
		int k = 0;
		boolean flag = true;
		int f1 = 1;
		int f2 = 1;
		
		for (int i = 0; i < n; i++) { 
			a[i] = sc.nextInt();
			 
			int f = 1;
			for (int j = 1; j <= a[i]; j++)
				f = f * i;
				
			if (f * f - 4 * (f2 - f1) >= 0)
				k++;
			
			f1 = f1 * 2;
			f2 = f2 * (i + 1);
			
		}
		
		if (k == 0)
			flag = false;
		
		int s = 0;
		while (k > 0) {
			s = s + (k % 10);
			k = k / 10;
		}
		
		while (s > 0 && flag){
			if (s % 2 == 0)
				flag = false;
			s = s / 10;
		}
		System.out.println(flag);
	}
}