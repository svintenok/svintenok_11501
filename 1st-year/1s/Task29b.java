/**
* @author Svintenok Katya
* 11501
* 29b
*/

public class Task29b{
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int s = 0;
		
		while ( Math.abs(n) > 0) {
			s = s + (n % 10);
			n = -n / 10;
		}
		 
		System.out.println(s);
	}
}