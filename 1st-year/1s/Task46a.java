/**
* @author Svintenok Katya
* 11501
* 46a
*/

public class Task46a {
	
	//конвертация строки в массив из цифр
	public static int conversion(String n, int [] a) {
		
		int size = n.length();
		for (int i = 0; i < size; i++)
			a[i] = n.charAt(size - 1 - i) - '0';
		
		a[size] = 0;
		return size;
	}
	
	//нужно ли сдвигать
	public static boolean check(int [] a1, int [] a2, int size1, int size2, int size3) {
		
		if (a1[size1 - size3] == 0) {
			for ( int i = 0; i < size2; i++) {
				if (a1[size1 - size3 - i - 1] > a2[size2 - i - 1])
					return true;

				else if (a1[size1 - size3 - i - 1] < a2[size2 - i - 1]) 
					return false;
			}
		}
		return true;
	}
	
	//вычитание
	public static void subtract(int [] a1, int [] a2, int size2, int size) {
		int r = 0;
		for (int i = 0; i < size2; i++) { 
			int p = a1[size - size2 + i] - a2[i] - r;
				
			if (p < 0){
				p += 10;
				r = 1;
			}
			else
				r = 0;
			a1[size - size2 + i] = p;
		}
		if (r != 0)
			a1[size] = a1[size] - r;
	}
	
	//деление
	public static void division(int [] a1, int [] a2, int [] del, int size1, int size2) {
		int size3 = 0;
		while ((size1 - size2 - size3) >= 0) {
			int k1 = 0;
			
			while (check(a1, a2, size1, size2, size3)) {
				
				subtract(a1, a2, size2, size1 - size3);
				k1++;
			}
			
			del[size3] = k1;
			size3++;
		}
		writing(a1, del, size1, size3);
	}
	
	//вывод
	public static void writing(int [] a1, int [] del, int size1, int size3) {
		for ( int i = del[0] == 0 ? 1 : 0; i < size3; i++) // вывод целой части
			System.out.print(del[i]);	
		
		System.out.println();
		
		while (a1[size1 - 1] == 0 && size1 > 1)
			size1--;
		
		for ( int i = size1 - 1; i >= 0; i--) // вывод остатка от деления
			System.out.print(a1[i]);		
	}
		
	
	public static void main(String[] args) {
		
		final int CAPACITY = 10000; 
		int [] a1 = new int[CAPACITY];
		int [] a2 = new int[CAPACITY];
		int [] del = new int[CAPACITY];
		
		String n = args[0];
		int size1 = conversion(n, a1);
		
		n = args[1];
		int size2 = conversion(n, a2);
		
		division(a1, a2, del, size1, size2);
	}
}