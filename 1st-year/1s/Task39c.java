/**
* @author Svintenok Katya
* 11501
* 39c
*/
import java.util.Scanner;

public class Task39c {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		int [][] a = new int [n][n];
		
		
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				a[i][j] = sc.nextInt();
			
		for (int i = 0; i < n; i++) {
			
			for (int j = 1; j < n; j++) {
				if (a[i][0] > a[i][j])
					a[i][0] = a[i][j];
			}
			
			if (a[0][0] < a[i][0])
				a[0][0] = a[i][0];
		}
		
		System.out.println(a[0][0]);
	}
}		