/**
* @author Svintenok Katya
* 11501
* 35a
*/

public class Task35a {
	public static void main(String[] args) {
		int [] a = new int[args.length - 1];
		int b;
		
		for(int i = 0; i < a.length; i++)
			a[i] = Integer.parseInt(args[i]);
		
		int k = Integer.parseInt(args[a.length]);
		
		
		for(int i = 1; i < a.length - k + 1; i++) {
			b = a[a.length - 1];
			
			for (int i1 = a.length - 1; i1 > 0; i1--)
				a[i1] = a[i1 - 1];
			
			a[0] = b;
		}
		
		for (int i = 0; i < a.length; i++) 
			System.out.print(a[i] + " ");
	}
}