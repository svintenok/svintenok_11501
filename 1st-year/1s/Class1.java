import java.util.Scanner;

public class Class1 {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		int [] a = new int[n];
		int k = 0;
		int d;
		boolean flag = false;
		
		for (int i = 0; i < n; i ++) { 
			 a[i] = sc.nextInt();
			 d = (int)(Math.sqrt(a[i]));
			 if ((a[i] / d == d) && d % 2 == 1)
				 k++;
		}
		
		while (k > 0 && !flag){
			if (k % 2 == 0)
				flag = true;
			k = k / 10;
		}
		System.out.println(flag);
	}
}