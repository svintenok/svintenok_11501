/**
* @author Svintenok Katya
* 11501
* 40b
*/
import java.util.Scanner;

public class Task40b {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		int [][] a = new int [n][n];
		boolean flag = true;
		
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				a[i][j] = sc.nextInt();
		
		for (int i = 0; i < n && flag; i++) {
			
			int s = 0;
			int j = n - 1;
			while (j - i >= 0) { 
				s = s + a[j][j - i];
				j--;
			}
			while (j > 0) { 
				s = s + a[j][j - i + n];
				j--;
			}
			
			if (s % 2 != 0)
				flag = false;
		}
		
		System.out.println(flag);
	}
}		