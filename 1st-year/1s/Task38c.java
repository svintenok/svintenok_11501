/**
* @author Svintenok Katya
* 11501
* 38c
*/


public class Task38c {
	public static void main(String[] args) {
		int n;
		int i = 0;
		boolean flag1 = false;
		boolean flag2;
		int k_old;
		int k;
		
		while ((i < args.length) && !flag1) {
			n = Integer.parseInt(args[i]);
			flag2 = true;
			k_old = 10;
			
			while ((n > 0) && flag2) {
				k = n % 10;
				if (k >= k_old)
					flag2 = false;
				else {
					n = n / 10;
					k_old = k;
				}	
			}
			
				flag1 = flag2;
				i++;
		}
		
		System.out.println(flag1);
	}
}