/**
* @author Svintenok Katya
* 11501
* 18c
*/

public class Task18c {
	public static void main(String[] args){
		double x = Double.parseDouble(args[0]);
		int n = Integer.parseInt(args[1]);
		double s = 0;
		double f = 1;
		for (int i = 1; i <= n; i++) {
			f = f * (x + i);
			s = s + f;
		}
		System.out.println(s);
	}
}