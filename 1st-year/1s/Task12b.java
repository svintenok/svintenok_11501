/**
* @author Svintenok Katya
* 11501
* 12b
*/

public class Task12b {
    public static void main(String[] args) {
		int n = 5;
		int f = 1;
		while (n > 0) {
			f = f * n;
			n = n- 2;
		}			
		System.out.println(f);
	}
}
		
		