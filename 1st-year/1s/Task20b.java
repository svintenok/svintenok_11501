/**
* @author Svintenok Katya
* 11501
* 20b
*/

public class Task20b {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		for (int i = 1; i <= n; i++) {
			for (int i1 = 1; i1 <= 2 * n - i; i1++)
				System.out.print(" ");
			for (int i1 = 1; i1 <= 2 * i - 1; i1++)
				System.out.print("*");
			for (int i1 = 1; i1 <= 2 * n - i; i1++)
				System.out.print(" ");
			System.out.println();
		}
		for (int i = 1; i <= n ; i++) {
			for (int i1 = 1; i1 <= n - i; i1++)
				System.out.print(" ");
			for (int i1 = 1; i1 <= 2 * i - 1; i1++)
				System.out.print("*");
			for (int i1 = 1; i1 <= 2 * n + 1 - 2 * i; i1++)
				System.out.print(" ");
			for (int i1 = 1; i1 <= 2 * i - 1; i1++)
				System.out.print("*");
			for (int i1 = 1; i1 <= n - i; i1++)
				System.out.print(" ");
			System.out.println();
		}
	}
}
		