/**
* @author Svintenok Katya
* 11501
* 29c
*/

public class Task29c{
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int i = 0;
		
		while (n > 0) {
			n = n / 10;
			i++;
		}
		
		System.out.println(i);
	}
}