/**
* @author Svintenok Katya
* 11501
* 37c
*/
import java.util.Scanner;

public class Task37c {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		int m = sc.nextInt();
		int p = sc.nextInt();
		
		int [][] a1 = new int [n][m];
		int [][] a2 = new int [m][p];
		int [][] a = new int [n][p];
		
		for (int i = 0; i < n; i++)
			for (int j = 0; j < m; j++)
				a1[i][j] = sc.nextInt();
		
		for (int i = 0; i < m; i++)
			for (int j = 0; j < p; j++)
				a2[i][j] = sc.nextInt();
			
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < p; j++) {
				for (int i1 = 0; i1 < m; i1++)	
					a[i][j] = a[i][j] + a1[i][i1] * a2[i1][j];
				
				System.out.print(a[i][j] + " ");
			}
			
			System.out.println();
		}
		
	}
}		