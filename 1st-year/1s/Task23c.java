/**
* @author Svintenok Katya
* 11501
* 23c
*/

public class Task23c {
	public static void main(String[] args) {
		final double EPS = 1e-8;
		int m = 1;
		double s = 0;
		double a;
		int z = 1;
		do {
			a = (double)(z) / (m * m);
			s = s + a;
			z = -z;
			m++;
		}
		while(Math.abs(a) > EPS);
		System.out.println(s);
	}
}