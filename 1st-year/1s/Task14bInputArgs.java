/**
* @author Svintenok Katya
* 11501
* 14bInputArgs
*/

public class Task14bInputArgs {
	public static void main(String[] args) {
		int x = Integer.parseInt(args[0]);
		double y = x > 1 ? (x * x - 1.0) / (3 * x +5) : x >= 0 ? 2 * x - 5 : (x - 1.0) / (x + 3);
		System.out.println(y);
	}
}