/**
 * Created by DNS on 05.12.2015.
 */
public class Guitarist extends Musician {

    private String guitar;

    public Guitarist(String name, String genre, String guitar) {
        super(name, genre);
        this.guitar = guitar;
    }

    public Guitarist() {
        super();
        this.guitar = new String("Guitar");
    }

    public void toPlay(String song) {
        System.out.println(this.name + " is playing <<" + song + ">> on " + this.guitar );
    }

    public void toSing(String song){
        System.out.println(this.name + " is singing <<" + song + ">> and playing on " + this.guitar);
    }

    public void toPullStrings(){
        System.out.println(this.name + " is pulling strings");
    }
}
