/**
 * Created by DNS on 06.12.2015.
 */
public abstract class Actor {

    public abstract void play();
    
}
