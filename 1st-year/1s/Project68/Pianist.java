/**
 * Created by DNS on 05.12.2015.
 */
public class Pianist extends Musician implements Studying{

    private String synthesizer;

    public Pianist(String name, String genre, String synthesizer) {
        super(name, genre);
        this.synthesizer = synthesizer;
    }

    public Pianist() {
        super();
        this.synthesizer = new String("Synthesizer");
    }

    public void toPlay(String song) {
        System.out.println(this.name + " is playing <<" + song + ">> on " + this.synthesizer );
    }

    public void toSing(String song){
        System.out.println(this.name + " is singing <<" + song + ">> and playing on "+ this.synthesizer);
    }

    public void toTellMusicalNotation() {
        System.out.println(this.name + " is telling musical notation");

    }

    public void toStudy() {
        System.out.println(this.name + " is studying to play");
    }
}
