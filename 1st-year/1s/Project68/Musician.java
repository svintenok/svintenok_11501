/**
 * Created by DNS on 05.12.2015.
 */
public abstract class Musician {

    protected String genre;
    protected String name;

    public Musician(String name, String genre) {
        this.genre = genre;
        this.name = name;
    }

    public Musician() {
        this( new String("Musician"), new String("Any genre"));
    }

    public abstract void toPlay(String song);

    public void toGiveAutograph() {
        System.out.println(this.name + " is giving you autograph");
    }

    public void toSing(String song){
        System.out.println(this.name + "is singing" + song);
    }

}
