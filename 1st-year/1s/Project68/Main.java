import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by DNS on 05.12.2015.
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        final int n = 10;
        Scanner sc = new Scanner(new File ("input"));
        String [][] information = new String[n][3];
        for (int i = 0; i < n; i++){
            information[i] = sc.nextLine().split(" ");
        }

        Musician [] musicians = new Musician[n];

        for (int i = 0; i < n; i += 2){
            musicians[n - i - 1] = new Guitarist(information[n - i- 1][0], information[n - i - 1][1], information[n - i - 1][2]);
            musicians[i] = new Pianist(information[i][0], information[i][1], information[i][2]);
        }

        sc = new Scanner(new File("inputSing"));
        String [] sings = new String[n];
        for (int i = 0; i < n; i++){
            sings[i] = sc.nextLine();
        }

        for (int i = 0; i < n; i ++){
            musicians[i].toPlay(sings[i]);
            musicians[i].toSing(sings[i]);
        }

    }
}
