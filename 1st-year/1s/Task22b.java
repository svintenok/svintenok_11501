/**
* @author Svintenok Katya
* 11501
* 22b
*/

public class Task22b {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		double x = Double.parseDouble(args[1]);
		double y = 0;
		double a =-1;
		for(int i = 1; i <= n; i++) {
			a = -(x * a) / i;
			y = y + a;
		}
		System.out.println(y);
	}
}