/**
 * Created by DNS on 11.12.2015.
 */
public class Triangle implements Measurable {

    private double side1, side2, side3;

    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    public Triangle() {
        this.side1 = 1;
        this.side2 = 1;
        this.side3 = 1;
    }

    public double getP() {
        return side1 + side2 + side3;
    }

    public double getS() {
        double p = getP() / 2;
        return Math.sqrt(p * (p - side1) * (p - side2) * (p - side3));
    }

    public String toString() {

        return "sides: " + side1 + "; " + side2 + "; " + side3 ;
    }
}
