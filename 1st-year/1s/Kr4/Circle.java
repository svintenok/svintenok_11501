/**
 * Created by DNS on 11.12.2015.
 */
public class Circle implements Measurable {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle() {
        this.radius = 1;
    }
    public double getP() {
        return 2 * Math.PI * radius;
    }


    public double getS() {
        return Math.PI * radius * radius;
    }

    public String toString() {

        return "radius = " + radius;
    }
}
