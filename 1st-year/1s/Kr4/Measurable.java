/**
 * Created by DNS on 11.12.2015.
 */
public interface Measurable  {
    double getP();
    double getS();
}
