

/**
 * Created by DNS on 11.12.2015.
 */
public class Main {
    public static void main(String[] args) {

        final int n = 10;
        Measurable[] figures = new Measurable[10];

        for (int i = 0; i < n; i += 2){
            figures[i] = new Circle((int)((Math.random() + 1) * 10));
            figures[i + 1] = new Triangle((int)((Math.random() + 1) * 10), (int)((Math.random() + 1) * 10), (int)((Math.random() + 1) * 10));
        }

        for (int i = 0; i < n; i ++){
            System.out.println(figures[i] + "\nSguare = " + figures[i].getS() + " , Perimeter = " + figures[i].getP());
        }

    }
}
