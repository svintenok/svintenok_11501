/**
* @author Svintenok Katya
* 11501
* 20c
*/
import java.lang.Math;

public class Task20c {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int j;
		for (int i = 1; i <= 2 * n + 1; i++) {
			j = Math.abs(n - i + 1);
			for (int i1 = 1; i1 <= j; i1++)
				System.out.print("*");
			for (int i1 = 1; i1 <= 2 * n + 1 - 2 * j; i1++)
				System.out.print("0");
			for (int i1 = 1; i1 <= j; i1++)
				System.out.print("*");
			System.out.println();
		}
	}
}