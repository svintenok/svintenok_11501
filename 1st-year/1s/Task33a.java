/**
* @author Svintenok Katya
* 11501
* 33a
*/

public class Task33a {
	public static void main(String[] args) {
		boolean flag1 = true;
		boolean flag2;
		int i = 0;
		int k = 0;
		int n;
		int i1;
		int t;
		
		while ((i < args.length) && (flag1)) {
			n = Integer.parseInt(args[i]);
			i1 = 0;
			t = n % 2;
			flag2 = true;
			
			while ((n > 0) && flag2) {
				if (n % 2 != t)
					flag2 = false;
				else {
					i1++;
					n = n / 10;
				}
			}
			
			if (((i1 == 3) || (i1 == 5)) && (flag2)) {
				k = k + 1;
				
				if (k > 2)
					flag1 = false;
			}
			
			i++;
		}
		
		if (k != 2)
			flag1 = false;
		
		System.out.println(flag1);
	}
}