/**
* @author Svintenok Katya
* 11501
* 10c
*/

public class Task10c {
    public static void main(String[] args) {
		int a = 1;
		int b = 2;
		System.out.println(a + " + " + b + " = " + (a + b));
		System.out.println(a + " - " + b + " = " + (a - b));
		System.out.println(b + " + " + a + " = " + (b - a));
		System.out.println(a + " * " + b + " = " + (a * b));
		System.out.println(a + " / " + b + " = " + (a / b));
		System.out.println(a + " % " + b + " = " + (a % b));
		System.out.println(b + " / " + a + " = " + (b / a));
		System.out.println(b + " % " + a + " = " + (b % a));
	}
}