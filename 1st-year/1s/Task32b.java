/**
* @author Svintenok Katya
* 11501
* 32b
*/

public class Task32b {
	public static void main(String[] args) {
		int k = Integer.parseInt(args[0]);
		int n = Integer.parseInt(args[1]);
		int n1 = 0;
		int k1 = 1;
		
		while (n > 0) {
			n1 = n1 + (n % 10) * k1;
			k1 = k1 * k;
			n = n / 10;
		}
		 
		System.out.println(n1);
	}
}