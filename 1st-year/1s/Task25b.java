/**
* @author Svintenok Katya
* 11501
* 25b
*/

public class Task25b {
	public static void main(String[] args) {
		final double EPS = 1e-8;
		long k = 0;
		double s = 0;
		double a;
		int z = 1;
		long m = 1;
	    double x = Double.parseDouble(args[0]);
		double t = x;
		double x4 = x * x * x * x;
		do {
			a = z * t / (4 * k + 1) / m;
			s = s + a;
			k++;
			m = m * 2 * k * (2 * k - 1);
			t = t * x4;
			z = -z;
		}
		while (Math.abs(a) > EPS);
		System.out.println(s);
	}
}