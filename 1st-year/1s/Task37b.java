/**
* @author Svintenok Katya
* 11501
* 37b
*/

import java.util.Scanner;

public class Task37b {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		int [][] a = new int [2 * n + 1][2 * n + 1];
		
		for (int i = 0; i < 2 * n + 1; i++)
			for (int j = 0; j < 2 * n +1; j++)
				a[i][j] = sc.nextInt();
			
		for (int i = 0; i < n + 1; i++)
			for (int j = i + 1; j < 2 * n - i; j++)
				a[i][j] = 0;
			
		for (int i = n + 1; i < 2 * n + 1; i++)
			for (int j = 2 * n + 1 - i; j < i; j++)
				a[i][j] = 0;
		
		for (int i = 0; i < 2 * n + 1; i++) {
			for (int j = 0; j < 2 * n +1; j++)
				System.out.print(a[i][j] + " ");
			System.out.println();
		}
	}
}