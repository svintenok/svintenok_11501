import java.util.Scanner;

public class Task54b {

	public static boolean isLetter(char c) {
		return ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z'));
	}

	public static boolean isDigit(char c) {
		return (c >= '0' && c <= '9');
	}

	
	public static void main(String [] args) {
		
		Scanner sc = new Scanner(System.in);
		String input = sc.next();

		byte state = 0;
		int i = 0;
		while (i < input.length() && state >= 0) {
			char c = input.charAt(i);
			switch(state) {
				case 0:
					if (c == '_' || isLetter(c)) {
						state = 1;
					}
					else {
						state = -1;
					}
					break;
				case 1:
					if (c == '_' || isLetter(c) || isDigit(c)) {
						state = 1;
					}
					else if (c == ' ') {
						state = 2;
					}
					else if (c == '=') {
						state = 3;
					}
					else {
						state = -1;
					}
				case 2:
				
					if (c == ' ') {
						state = 2;
					}
					else if (c == '=') {
						state = 3;
					}
					else {
						state = -1;
					}
				case 3:
				
					if (c == ' ') {
						state = 3;
					}
					else if (c == '_' || isLetter(c) || isDigit(c)) {
						state = 4;
					}
					else {
						state = -1;
					}
				case 4:

					if (c  == '_' || isLetter(c) || isDigit(c)) {
						state = 4;
					}
					else {
						state = -1;
					}
			}
			i += 1;
		}
		if (state == 4)
			System.out.println("Correct");
		else
			System.out.println("Not correct");
	}
}