/**
* @author Svintenok Katya
* 11501
* 30a
*/

public class Task30a {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int n1 = 0;
		int i = 1;
		
		while ( n > 0) {
			if ((n % 2) != 0) {
				n1 = n1 + (n % 10 * i);
				i = i * 10;
			}
			n = n / 10;
		}
		 
		System.out.println(n1);
	}
}