/**
* @author Svintenok Katya
* 11501
* 31c
*/

public class Task31c {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		boolean flag = false;
		int i = 0;
		
		while ((n > 0) && !flag) {
			if ((n % 2 == 0) || ((n % 10) % 3 == 0)) {
				i++;
				if (i > 1)
					flag = true;
			}
			n = n / 10;
		}
		 
		System.out.println(flag);
	}
}