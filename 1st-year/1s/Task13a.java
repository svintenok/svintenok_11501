/**
* @author Svintenok Katya
* 11501
* 13a
*/

public class Task13a {
	public static void main(String[] args) {
		int n = 5000;
		int s;
		for (int i = 1; i <= n; i++) {
			s = 0;
			for (int j = 1; j < i; j++) {
				if (i % j == 0) {
					s = s + j;
				}
			}
			
			if (s == i)
				System.out.println(i);
		}
	}
}