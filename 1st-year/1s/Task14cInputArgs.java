/**
* @author Svintenok Katya
* 11501
* 14cInputArgs
*/

 public class Task14cInputArgs {
	public static void main(String[] args) {
		double x = Double.parseDouble(args[0]);
		double y = x > 6 ? (x * x - 1) / (3 * x + 5) : (x + 1.0) * (x + 1) * (x + 1) / (x - 7) + 2;
		System.out.println(y);
	}
 }
		