/**
* @author Svintenok Katya
* 11501
* 36c
*/

public class Task36c {
	public static void main(String[] args) {
		
		final int CAPACITY = 10000; 
		int [] a = new int[CAPACITY];
		int [] ap = new int[CAPACITY];
		int size = 0;


		String n = args[0];
		for (int i = n.length() - 1; i >= 0; i--){
			a[size] = n.charAt(i) - '0';
			size++;
		}
		
		int k = Integer.parseInt(args[1]);
		int p;
		int r = 0;
		int i = 0;
		
		while (i < size) {
			p = a[i] * k + r;
			ap[i] = p % 10;
			r = p / 10;
			i++;
		}
		
		if (r != 0){
			ap[size] = r;
			size++;
		}
		
			for (i = size - 1; i >= 0; i--)
		System.out.print(ap[i]);	
	}
}