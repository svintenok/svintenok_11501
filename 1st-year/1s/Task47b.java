/**
* @author Svintenok Katya
* 11501
* 47b
*/

import java.util.Scanner;

public class Task47b {
	
	//метод к задаче 19с
	public static double sum(int i, int n, double element) {
		
		if (i <= n) {
			element = element * (i - 1) * (i - 1)/ ((2 * i) * (2 * i - 1));
			return sum(i + 1, n, element) + element;
		}
		else
			return 0.5;

	}
	
	// метод к задаче 28b
	public static void input(int [] a, int i, Scanner sc) {
		if (i < a.length) {
			a[i] = sc.nextInt();;
			input(a, i + 1, sc);
		}
	}
	
	//метод к задаче 28b
	public static boolean isEven(int n) {
		return (n % 2 == 0);	
	}
	
	//метод к задаче 28b
	public static boolean localMax (int i, int [] a) {
		
		if (i < a.length) {
			if (a[i] > a[i - 1] && a[i] > a[i + 1])
				if (isEven(a[i]))
					return true;		
			return localMax(i + 1, a);
		}
		return false;
	}
		
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		int n = Integer.parseInt(args[0]);
		System.out.println(sum(2, n, 0.5));
		
		int [] a = new int [n];
		input(a, 0, sc);
		System.out.println(localMax(1, a));	
	}
}