/**
* @author Svintenok Katya
* 11501
* 37a
*/

import java.util.Scanner;

public class Task37a {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		double [][] a = new double [n][n];
		
		
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				a[i][j] = sc.nextDouble();
		
		int str = 0; // номер cтроки, которую мы вычитаем
		for (int i1 = 0; i1 < n; i1++) { // номер столбца, который мы обнуляем 
			boolean flag = false;
			int j; 
			
			// ищем ненулевой столбец
			for (j = str; j < n && !flag; j++) 
				flag = a[j][i1] != 0;  
			
			if (flag) {
				
				// cтавим ненулевую строку на i1
				if (j - 1 != str) { 
		
					double b;
					for (int i = 0; i < n; i++) { 
						b = a[str][i];
						a[str][i] = a[j - 1][i];
						a[j - 1][i] = b;
					}
				}
				
				// вычитаем
				for (int i = str + 1; i < n; i++) 
					for (j = n - 1; j >= 0; j--)
						a[i][j]= a[i][j] - (a[i][i1] * a[str][j] / a[str][i1]);					

				str ++;
			}
			
		}
		
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				double x = Math.round(a[i][j] * 10)/ 10;
				System.out.print(x + " ");
			}
			System.out.println();
		}
	}
}