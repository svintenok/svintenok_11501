/**
* @author Svintenok Katya
* 11501
* 47c
*/
public class Task47c {
	
	// метод к задаче 11c
	public static void table(int k, int i) {
		
		System.out.println(i + " x " + k + " = " + (i * k));
		if (i < 9)
			table(k, i + 1);
	}
	
	// метод к задаче 12b
	public static int factorial(int n) {
	
		if (n > 1)
			return factorial(n - 2) * n;
		else
			return 1;
	}	
	
	
    public static void main(String[] args) {

		int k = Integer.parseInt(args[0]);
		table(k, 2);
		System.out.println(factorial(k));
		
	}
}