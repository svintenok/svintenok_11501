/**
* @author Svintenok Katya
* 11501
* 45b
*/

import java.util.Scanner;

public class Task45b {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		String [] s = sc.nextLine().split(" ");
		boolean flag = true;
		
		for (int i = 0; i < s.length; i++) {
			if (s[i].compareToIgnoreCase("true") == 0) {
				if (flag) {
					if (s[i].charAt(0) == 't')
						s[i] = "false";
					else
						s[i] = "False";
				}
				flag = !flag; 
			}
			System.out.println(s[i]);
		}
	}
}	