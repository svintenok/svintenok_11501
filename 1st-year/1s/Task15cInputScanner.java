/**
* @author Svintenok Katya
* 11501
* 15cInputScanner
*/
import java.util.Scanner;

public class Task15cInputScanner {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int pEven = 1;
		int sOdd = 0;
		int s = 0;
		int p = 1;
		int k;
		int n = sc.nextInt();
		for (int i = 1; i <= n; i++) {
			k = sc.nextInt();
			if (k % 2 != 0) 
				sOdd = sOdd + k;
			else if (k != 0)
				pEven = pEven * k;
			if (k < 0)
				s = s + k;
			else if (k != 0)
				p = p * k;
		} 
		System.out.println(sOdd +  ", " + pEven + ", " + s + ", " + p);
	}
}