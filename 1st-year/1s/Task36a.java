/**
* @author Svintenok Katya
* 11501
* 36a
*/

public class Task36a {
	public static void main(String[] args) {
		
		final int CAPACITY = 10000; 
		int [] a1 = new int[CAPACITY];
		int [] a2 = new int[CAPACITY];
		int [] del = new int[CAPACITY];
		int size1 = 0;
		int size2 = 0;
		int size3 = 0;
		int k1; //

		String n = args[0];
		for (int i = n.length() - 1; i >= 0; i--){
			a1[size1] = n.charAt(i) - '0';
			size1++;
		}
		a1[size1] = 0;
		n = args[1];
		for (int i = n.length() - 1; i >= 0; i--){
			a2[size2] = n.charAt(i) - '0';
			size2++;
		}
		
		while ((size1 - size2 - size3) >= 0) {
			k1 = 0;  
			boolean flag = true;
	
			while (flag) {

				if (a1[size1 - size3] == 0) {
					boolean flag2 = false;
					for ( int i = 0; i < size2 && !flag2; i++) {
					if (a1[size1 - size3 - i - 1] > a2[size2 - i - 1]) {
						flag2 = true;
					}
					else if (a1[size1 - size3 - i - 1] < a2[size2 - i - 1]) {
						flag2 = true;
						flag = false;
					}
				}
			}
			
			if (flag) {
				int r = 0;
				for ( int i = 0; i < size2; i++) { 
					int p = a1[size1 - size2 + i - size3] - a2[i] - r;
				
					if (p < 0){
						p += 10;
						r = 1;
					}
					else
						r = 0;
				
					a1[size1 - size2 + i - size3] = p;
				}
				if (r != 0)
					a1[size1 - size3] = a1[size1 - size3] - r;
				
				k1++;
			}	
			
		}
		del[size3] = k1; 
		size3++;			
	}
				
		for ( int i = del[0] == 0 ? 1 : 0; i < size3; i++) // вывод целой части
			System.out.print(del[i]);	
		
		System.out.println();
		
		while (a1[size1 - 1] == 0 && size1 > 1)
			size1--;
		
		for ( int i = size1 - 1; i >= 0; i--) // вывод остатка от деления
			System.out.print(a1[i]);		
	}
}