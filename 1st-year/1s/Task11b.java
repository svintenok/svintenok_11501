/**
* @author Svintenok Katya
* 11501
* 11b
*/

public class Task11b {
    public static void main(String[] args) {
		int n = 3;
		double s = 0;
		int i = 1;
		int z = 1;
		while (i < 2 * n) {
			s = s + (double)(z)/(i * i);
			i = i + 2;
			z = -z;
		}
		System.out.println(s);
	}
}