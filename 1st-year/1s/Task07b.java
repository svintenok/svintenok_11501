/**
* @author Svintenok Katya
* 11501
* 07b
*/

public class Task07b {
    public static void main(String[] args) {
		double x = 1.1;
		double p = x * x;
		double p1 = p * p; //4 степень
		p = p1 * p1; //8 
		p = p * p; //16
		double p2 = p * p; //32
		p = p2 * p2; //64
		double p3 = p * p2 * p1; //64+32+4=100
		System.out.println(p3);
	}
}