/**
* @author Svintenok Katya
* 11501
* 30b
*/

public class Task30b {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int i = 10;
		int n1 = n;
		
		while (n1 > 0) {
			n1 = n1 / 10;
			i = i * 10;
		}
		
		n = i + (n * 10) + 1;
		
		System.out.println(n);
	}
}