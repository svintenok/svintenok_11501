/**
* @author Svintenok Katya
* 11501
* 46c
*/
public class Task46c {
	// метод к задаче 12b
	public static int factorial(int n) {
	
		int f = 1;
		while (n > 0) {
			f = f * n;
			n = n - 2;
		}		
		return f;	
	}
	
	//метод к задаче 28b
	public static boolean isEven(int n) {
		return (n % 2 == 0);	
	}
	
	//метод к задаче 28b
	public static boolean localMax(int [] a) {
	
		for (int i = 1; i < a.length; i++)
			if (a[i] > a[i - 1] && a[i] > a[i + 1])
				if (isEven(a[i]))
					return true;		
		return false;	
	}
	
	//метод к задаче 38c
	public static boolean isIncrease(int n) {
	
		int k_old = 10;
			
			while (n > 0) {
				int k = n % 10;
				if (k >= k_old) 
					return false;
				n = n / 10;
				k_old = k;	
			}
		
		return true;
	}

	//метод к задаче 38c
	public static boolean hasIncrease(int [] a) {
	
		for (int i = 1; i < a.length; i++)
				if (isIncrease(a[i]))
					return true;		
		return false;	
	}
	
	//метод к задаче 39c
	public static int minimum(int [] a) {
		
		int min = a[0];
		for (int i = 1; i < a.length; i++) 
			if (min > a[i])
				min = a[i];
		return min;
	}
	
	//метод к задаче 39c
	public static int maximum(int [] [] a) {
		
		int max = minimum(a[0]);
		for (int i = 1; i < a.length; i++) {
			int min = minimum(a[i]);
			if (min > max)
				max = min;
		}
		return max;
	}
	
    public static void main(String[] args) {
		int n = 5;	
		System.out.println(factorial(n));
		
		int [] a = new int [] {4, 6, 3, 9, 2};
		System.out.println(localMax(a));
		
		int [] b = new int [] {3484, 436, 423, 4329, 432};
		System.out.println(hasIncrease(b));
		
		int [][] c = new int [][] {{3, 6, 1}, {6, 4, 9}, {9, 2, 3}};
		System.out.println(maximum(c));
	}
}
