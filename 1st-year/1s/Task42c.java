/**
* @author Svintenok Katya
* 11501
* 42c
*/

public class Task42c {
	public static void main(String[] args) {
		
		String s1 = args[0];
		String s2 = args[1];
		int s = 1;
		
		int i = 0;
		do {
			if (s1.charAt(i) < s2.charAt(i))
				s = 2;
			i++;
		}
		while (i < s1.length() && i < s1.length() && s1.charAt(i) == s2.charAt(i));
		
			System.out.println(s);

	}
}