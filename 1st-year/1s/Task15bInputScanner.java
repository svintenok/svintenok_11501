/**
* @author Svintenok Katya
* 11501
* 15bInputScanner
*/
import java.util.Scanner;

public class Task15bInputScanner {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int x = sc.nextInt();
		int S = sc.nextInt();
		for (int i = 3; i <= n + 2; i++) {
			int a = sc.nextInt();
			S = a + x * S;
		} 
		System.out.println(S);
	}
}
			