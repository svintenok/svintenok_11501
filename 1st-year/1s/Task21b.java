/**
* @author Svintenok Katya
* 11501
* 21b
*/
import java.lang.Math;

public class Task21b {
	public static void main(String[] args) {
		int r = Integer.parseInt(args[0]);
		double x;
		for (int y = -r; y <= r; y++) {
			x = (double)(Math.sqrt(r * r - y * y));
			x = Math.round(x);
			for (int i = 1; i <= r - x; i++)
				System.out.print("*");
			for (int i = 1; i <= 2 * x; i++)
				System.out.print("0");
			for (int i = 1; i <= r - x; i++)
				System.out.print("*");
			System.out.println("");
		}
	}
}