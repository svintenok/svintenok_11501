/**
* @author Svintenok Katya
* 11501
* 35c
*/

public class Task35c {
	public static void main(String[] args) {
		int [] a = new int[args.length];
		int b;
		
		for(int i = 0; i < a.length; i++)
			a[i] = Integer.parseInt(args[i]);
		
		for (int i = 0; i < a.length / 2; i++) {
			b = a[i];
			a[i] = a[a.length - i - 1];
			a[a.length - i - 1] = b;	
		}
		
		for (int i = 0; i < a.length; i++) 
			System.out.print(a[i] + " ");
	}
}