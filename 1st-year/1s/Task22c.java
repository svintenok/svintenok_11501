/**
* @author Svintenok Katya
* 11501
* 22c
*/

public class Task22c {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int k = 2;
		int kd;
		while (n > 0){
			kd = k;
			for(int i = 2; i <= 5; i++) {
				if (kd % i == 0) {
					kd = kd / i;
					i = i - 1;
				}
			}
			if (kd == 1) {
				System.out.println(k);
				n = n - 1;
			}
			k++;
		}
	}
}
		
		