import java.io.*;
import java.util.Scanner;

public class Kr3Task2 {
	public static void main(String [] args) throws FileNotFoundException {
		Scanner sc = new Scanner(new File("input3Task2.txt"));
		int n = sc.nextInt();
		int [][] a = new int [n][n];
		
		for(int i = 0; i < n; i += 2) 
			for(int j = 0; j < n; j++) 
				a[i][j] = n * (n - i) - 1 - j;
				
		for(int i = 1; i < n; i += 2) 
			for(int j = 0; j < n; j++) 
				a[i][j] = n * (n - i - 1) + j;
	
		
		for(int i = 0; i < n; i++) {
			for(int j = 0; j < n; j++)
				System.out.print(a[i][j] + " ");
			System.out.println();
		}
	}
}