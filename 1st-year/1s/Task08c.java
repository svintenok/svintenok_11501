/**
* @author Svintenok Katya
* 11501
* 08c
*/

public class Task08c {
    public static void main(String[] args) {
		double x = -1;
		double y;
		if (x > 2) 
			y = (x * x - 1.0) / (x + 2);
		else if (x <= 0)
			y = x * x * (1 + 2 * x);
		else 
			y = (x * x - 1) * (x + 2);
		System.out.println(y);	
	}
}