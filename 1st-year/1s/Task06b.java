/**
* @author Svintenok Katya
* 11501
* 06b
*/

public class Task06b {
    public static void main(String[] args) {
		double x = 2;
		double y = 3;
		double z = 1;
		double u = x + y;	
		u = u + 2.1;
		x = x - z;
		x = x - 5.6;
		u = u / x;
		u = u * z;
		u = u + y;
		u = u * y;
		x = z + 1.1;
		y = z - 2;
		x = x / y;
		u = u + x;
		System.out.println(u);
	}
}