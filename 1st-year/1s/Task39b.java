/**
* @author Svintenok Katya
* 11501
* 39b
*/
import java.util.Scanner;

public class Task39b {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		int n = sc.nextInt();
		int [][] a = new int [n][n];
		
		
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
				a[i][j] = sc.nextInt();
		
		for (int i = 0; i < n; i++) {
	
			int j = 1;
			while (j + i < n) { 
				a[0][i] = a[0][i] + a[j][j + i];
				j++;
			}
			while (j < n) { 
				a[0][i] = a[0][i] + a[j][j + i - n];
				j++;
			}
			
			if (a[0][0] < a[0][i])
				a[0][0] = a[0][i];
		}
	
		
		System.out.println(a[0][0]);
	}
}		