/**
* @author Svintenok Katya
* 11501
* 14cInputScanner
*/
import java.util.Scanner;

 public class Task14cInputScanner {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		double x = sc.nextDouble();
		double y = x > 6 ? (x * x - 1) / (3 * x + 5) : (x + 1.0) * (x + 1) * (x + 1) / (x - 7) + 2;
		System.out.println(y);
	}
 }
		