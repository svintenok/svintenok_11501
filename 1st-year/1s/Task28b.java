/**
* @author Svintenok Katya
* 11501
* 28b
*/

public class Task28b{
	public static void main(String[] args) {
		boolean flag = false;
		int i = 2;
		int xOld;
		int x = Integer.parseInt(args[0]);
		int xNext = Integer.parseInt(args[1]);
		do {
			xOld = x;
			x = xNext;
			xNext = Integer.parseInt(args[i]);
			if ((x % 2 == 0) && (x > xOld) && (x > xNext))
				flag = true;
			else
				i++;
		}
		while ((i < args.length) && !flag);
		System.out.println(flag);
	}
}		