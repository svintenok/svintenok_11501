/**
* @author Svintenok Katya
* 11501
* 30с
*/

public class Task30c {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int max = 0;
		
		while ( n > 0) {
			if ((n % 10) > max)
				max = n % 10;
			n = n / 10;
		}
		 
		System.out.println(max);
	}
}