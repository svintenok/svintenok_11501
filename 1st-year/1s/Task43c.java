/**
* @author Svintenok Katya
* 11501
* 43c
*/

import java.util.Scanner;

public class Task43c {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		String s = sc.nextLine();
		
		int n = 0;
		if (s.charAt(0) <= 'Z' && s.charAt(0) >= 'A')
			n++;
		
		for (int i = 1; i < s.length(); i++) 
			if (s.charAt(i - 1) == ' ' && s.charAt(i) <= 'Z' && s.charAt(i) >= 'A')
				n++;

		System.out.println(n);

	}
}