/**
* @author Svintenok Katya
* 11501
* 44b
*/

import java.util.Scanner;

public class Task44b {
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		String s = sc.nextLine();
		int [] a = new int [26];

				
		for (int i = 0; i < a.length; i++)
				a[i] = 0;
			
		
		for (int i = 0; i < s.length(); i++) {
			if (s.charAt(i) <= 'Z' && s.charAt(i) >= 'A')
				a[s.charAt(i) - 'A']++;
			else if (s.charAt(i) <= 'z' && s.charAt(i) >= 'a')
				a[s.charAt(i) - 'a']++;
		}
		
		for (int i = 0; i < a.length; i++)
			System.out.print(a[i] + " ");
	}
}