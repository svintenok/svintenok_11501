/**
* @author Svintenok Katya
* 11501
* 34c
*/

public class Task34c {
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int [] a = new int[n];
		int z = 1;
		int k = 1;
		
		for (int i = 0; i < n; i ++) { 
			a[i] = z * k;
			z = -z;
			k = k + 2;
			System.out.println(a[i]);
		}
	}
}