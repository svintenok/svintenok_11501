/**
* @author Svintenok Katya
* 11501
* 13b
*/

public class Task13b {
	public static void main(String[] args) {
		int n = 60;
		int i = 2;
		while (n > 1) {
			if (n % i == 0) {
				System.out.println(i);
				n = n / i;
			}
			else
			i++;
		}
	}
}