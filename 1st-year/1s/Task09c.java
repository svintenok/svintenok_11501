/**
* @author Svintenok Katya
* 11501
* 09c
*/

public class Task09c {
    public static void main(String[] args) {
		int n = 8;
		int i = 2;
		int p = 1;
		while (i <= n) {
			p = p * i;
			i++;
		}
		System.out.println(p);
	}
}