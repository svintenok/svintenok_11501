/**
* @author Svintenok Katya
* 11501
* 47a
*/

public class Task47a {
	
	//конвертация строки в массив из цифр
	public static int conversion(String n, int [] a, int size) {
		if (size < n.length()) {
			a[size] = n.charAt(n.length() - size - 1) - '0';
			return conversion(n, a, size + 1);
		}
		else {
			a[size] = 0;
			return size;
		}
	}	
	
	//нужно ли сдвигать
	public static boolean check(int [] a1, int [] a2, int size1, int size2, int size3, int i) {
		
		if (a1[size1 - size3] == 0) {
			if (i < size2) {
				if (a1[size1 - size3 - i - 1] > a2[size2 - i - 1])
					return true;

				else if (a1[size1 - size3 - i - 1] < a2[size2 - i - 1]) 
					return false;
				else
					return check(a1, a2, size1, size2, size3, i + 1);
			}
		}
		return true;
	}
	
	//вычитание
	public static void subtract(int [] a1, int [] a2, int size2, int size, int i, int r) {
		
		if (i < size2) { 
			int p = a1[size - size2 + i] - a2[i] - r;
				
			if (p < 0){
				p += 10;
				r = 1;
			}
			else
				r = 0;
			a1[size - size2 + i] = p;
			subtract(a1, a2, size2, size, i + 1, r);
		}
		else
			a1[size] = a1[size] - r;
	}
	
	//деление
	public static void division(int [] a1, int [] a2, int [] del, int size1, int size2, int size3) {

		if ((size1 - size2 - size3) >= 0) {	
			del[size3] = divisionPart(a1, a2, size1, size2, size3, 0);
			division(a1, a2, del, size1, size2, size3 + 1);
		}
		else
			writing(a1, del, size1, size3);
	}
	
	//деление части
	public static int divisionPart(int [] a1, int [] a2, int size1, int size2, int size3, int k1) {
	
		if (check(a1, a2, size1, size2, size3, 0)) {	
			subtract(a1, a2, size2, size1 - size3, 0, 0);
			return divisionPart(a1, a2, size1, size2, size3, k1 + 1);
		}
		else
			return k1;
	}
	
	//вывод
	public static void writing(int [] a1, int [] del, int size1, int size3) {
		integerPart(del, size3, 0);
		size1 = lengthDel(a1, size1);	
		residue(a1, size1 - 1);
	}
	
	// вывод целой части
	public static void integerPart(int [] del, int size, int i) {
		if (i < size) {
			System.out.print(del[i]);
			integerPart(del, size, i + 1);
		}
		else
		 System.out.println();
	}
	
	//длина целой части
	public static int lengthDel(int [] a, int size) {
	
		if (a[size - 1] == 0 && size > 1)
			return lengthDel(a, size - 1);
		else
			return size;
	}
	
	// вывод остатка от деления
	public static void residue(int [] a, int size) {
		if (size >= 0) {
			System.out.print(a[size]);
			residue(a, size - 1);
		}	
	}
	
	public static void main(String[] args) {
		
		final int CAPACITY = 10000; 
		int [] a1 = new int[CAPACITY];
		int [] a2 = new int[CAPACITY];
		int [] del = new int[CAPACITY];
		
		String n = args[0];
		int size1 = conversion(n, a1, 0);
		
		n = args[1];
		int size2 = conversion(n, a2, 0);
		
		division(a1, a2, del, size1, size2, 0);
	}
}