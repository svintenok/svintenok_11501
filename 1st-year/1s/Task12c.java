/**
* @author Svintenok Katya
* 11501
* 12c
*/

public class Task12c {
    public static void main(String[] args) {
		int n = 5;
		int f1 = 0, f2 = 1, f;
		int i = 2;
		while (i <= n){			
		f = f1 + f2;
		f1 = f2;
		f2 = f;
		i++;
		}
	System.out.println(f2);
	}
}