/**
* @author Svintenok Katya
* 11501
* 29a
*/

public class Task29a{
	public static void main(String[] args) {
		int n = Integer.parseInt(args[0]);
		int n1 = 0;
		
		while ( n > 0) {
			n1 = 10 * n1 + (n % 10);
			n = n / 10;
		}
		 
		System.out.println(n1);
	}
}