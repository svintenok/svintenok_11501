/**
* @author Svintenok Katya
* 11501
* 38b
*/


public class Task38b {
	public static void main(String[] args) {
		int n;
		int i = 0;
		boolean flag1 = true;
		boolean flag2;
		int k_old;
		int k;
		int t = 0;
		
		while ((i < args.length) && flag1) {
			n = Integer.parseInt(args[i]);
			flag2 = true;
			k_old = 0;
			
			while ((n > 0) && flag2) {
				k = n % 10;
				if (k <= k_old)
					flag2 = false;
				else {
					n = n / 10;
					k_old = k;
				}
			}
			
			if (flag2 == true) {
				t++;
				if (t == 4)
					flag1 = false;
			}
			
			i++;
		}
		
		if (t < 3)
			flag1 = false;
		System.out.println(flag1);
	}
}