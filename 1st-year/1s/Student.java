public class Student {
	
	private String name;
	
	public Student(String name) {
		this.name = name; 
	}
	
	public String getName() {
		return name;
	}
	
	public void sayHi(Student s) {
		System.out.println(this.name + " says 'hi!' to " + s.getName());
	}
	
}