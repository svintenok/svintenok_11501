/* @author Svintenok Katya
* 11501
* 28a
*/

public class Task28a {
	public static void main(String[] args) {
		boolean flag = true;
		int f = 0;
		int xOld;
		int x = Integer.parseInt(args[0]);
		int xNext = Integer.parseInt(args[1]);
		int i = 2;
		do {
			xOld = x;
			x = xNext;
			xNext = Integer.parseInt(args[i]);
			if  ((x % 2 == 0) && (x > xOld) && (x > xNext)) { 
				f++;
				if (f == 3)
					flag = false;
			}
			i++;
		}
		while (( i < args.length) && flag);
		if (f < 2)
			flag = false;
		System.out.println(flag);
	}
}
		