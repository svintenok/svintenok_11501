/**
* @author Svintenok Katya
* 11501
* 26b
*/

public class Task26b {
	public static void main(String[] args) {
		final double EPS = 1e-8;
		long k = 1;
		double s = 0;
		double a;
	    double x = Double.parseDouble(args[0]);
		double t = 1;
		long f1 = 1;
		long f2 = 1;
		do {
			t = t * x * x;
			f1 = f1 * (2 * k - 1);
			f2 = f2 * 2 * k;
			a = t * f1 / f2;
			s = s + a;
			k++;
		}
		while (Math.abs(a) > EPS);
		System.out.println(s);
	}
}