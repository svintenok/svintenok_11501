/**
 * Created by DNS on 12.12.2015.
 */
public class RationalFractionException extends Exception {
    public RationalFractionException(String s){
        super(s);
    }
}
