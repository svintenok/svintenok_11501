
public class RationalFraction {
	
	private int x, y;
	
	public RationalFraction(int x, int y) throws RationalFractionException {
		if (y == 0) {
          throw new RationalFractionException("Denominator can't be zero!");
        }
        else {
		    this.x = x;
		    this.y = y;
        }
	}
	public RationalFraction() throws RationalFractionException {
		this (0, 1);
	}
	
	public int getX() { 
		return x; 
	}
	public int getY() { 
		return y;
	}
	
	public void setX(int x) {
		this.x = x;
	}
	public void setY(int y) {
		this.y = y;
	}
		
	public String toString() {
		return x + "/" + y;
	}
	
	public void reduce() {
		int i = 2;
		while(i <= this.x && i <= this.y ) {
			if ((this.x % i == 0) && (this.y % i == 0)) {
				this.x = this.x / i;
				this.y = this.y / i;
			}
			else
				i++;
		}
		if (y < 0) {
			this.x = -this.x;
			this.y = -this.y;
		}
	}
	
	public RationalFraction add(RationalFraction rf) throws RationalFractionException {
		RationalFraction rf2 = new RationalFraction(this.x * rf.getY() + rf.getX() * this.y, this.y * rf.getY() );
		rf2.reduce();
		return rf2;
	}
	public void add2(RationalFraction rf) {
		this.x = this.x * rf.getY() + rf.getX() * this.y;
		this.y = this.y * rf.getY();
		this.reduce();
	}
	
	public RationalFraction sub(RationalFraction rf) throws RationalFractionException {
		RationalFraction rf2 = new RationalFraction(this.x * rf.getY() - rf.getX() * this.y, this.y * rf.getY() );
		rf2.reduce();
		return rf2;
	}
	public void sub2(RationalFraction rf) {
		this.x = this.x * rf.getY() - rf.getX() * this.y;
		this.y = this.y * rf.getY();
		this.reduce();
	}
	
	public RationalFraction mult(RationalFraction rf) throws RationalFractionException {
		RationalFraction rf2 = new RationalFraction(this.x * rf.getX(), this.y * rf.getY() );
		rf2.reduce();
		return rf2;
	}
	public void mult2(RationalFraction rf) {
		this.x = this.x * rf.getX();
		this.y = this.y * rf.getY();
		this.reduce();
	}
	
	public RationalFraction div(RationalFraction rf) throws RationalFractionException {
		RationalFraction rf2 = new RationalFraction(this.x * rf.getY(), this.y * rf.getX() );
		rf2.reduce();
		return rf2;
	}
	public void div2(RationalFraction rf) {
		this.x = this.x * rf.getY();
		this.y = this.y * rf.getX();
		this.reduce();
	}
	
	public double value() {
		return (double)(x)/y;
	} 
	
	public boolean equals(RationalFraction rf) {
		this.reduce();
		rf.reduce();
		return (this.x == rf.getX()) && (this.y == rf.getY());
	}
	
	public int numberPart() {
		return x/y;
	}

}