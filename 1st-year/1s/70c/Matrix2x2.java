
public class Matrix2x2 {
	
	private double[][] a = new double [2][2];
	
	public Matrix2x2(double[][] a) {
				this.a = a;
	}
	public Matrix2x2(double a1, double a2, double a3, double a4) {
		a[0][0] = a1;
		a[0][1] = a2;
		a[1][0] = a3;
		a[1][1] = a4;
	}
	public Matrix2x2() {
		this (0, 0, 0, 0);
	}                                          
	public Matrix2x2(double c) {
		for (int i = 0; i < a.length; i++)
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = c;
	}
	
	public double getA(int i, int j) { 
		return a[i][j]; 
	}
	
	public void setA(double x, int i, int j) {
		a[i][j] = x;
	}
	
	public String toString() {
		String s = a[0][0] + " " + a[0][1] + "\n" + a[1][0] + " " + a[1][1] + "\n" + "=================";
		return s;
	}
	
	public Matrix2x2 add(Matrix2x2 mt) {
		Matrix2x2 mt2 = new Matrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.setA(a[i][j] + mt.getA(i,j), i, j);
			
		return mt2;
	}
	public void add2(Matrix2x2 mt) {
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = a[i][j] + mt.getA(i,j);

	}
	
	public Matrix2x2 sub(Matrix2x2 mt) {
		Matrix2x2 mt2 = new Matrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.setA(a[i][j] - mt.getA(i,j), i, j);
			
		return mt2;
	}
	public void sub2(Matrix2x2 mt) {
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = a[i][j] - mt.getA(i,j);

	}
	
	public Matrix2x2 mult(Matrix2x2 mt) {
		Matrix2x2 mt2 = new Matrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.setA(a[i][0] * mt.getA(0, j) + a[i][1] * mt.getA(1, j), i, j);
			
		return mt2;
	}
	public void mult2(Matrix2x2 mt) {

		for (int i = 0; i < a.length; i++) {
			double a1 = a[i][0];
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = a1 * mt.getA(0, j) + a[i][1] * mt.getA(1, j);
		}

	}
	
	public Matrix2x2 multNumber(double c) {
		Matrix2x2 mt2 = new Matrix2x2();
		
		for (int i = 0; i < a.length; i++) 
			for (int j = 0; j < a[i].length; j++)
				mt2.setA(a[i][j] * c, i, j);
			
		return mt2;
	}
	public void multNumber2(double c) {

		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[i].length; j++)
				a[i][j] = a[i][j] * c;
		}
	}
	
	public double det() {
		return a[1][1] * a[0][0] - a[0][1] * a[1][0];
	}
	
	public void transpon() {
		double a1 = a[0][1];
		a[0][1] =  a[1][0];
		a[1][0] =  a1;
	}
	
	
	public Matrix2x2 inverseMatrix() throws NoInverseMatrixException{
		
		double d = this.det();
		if (d == 0) {
			throw new NoInverseMatrixException("There isn't inverse matrix when the determinant is zero!");
		}
		else {
			Matrix2x2 mt = new Matrix2x2(a[1][1], -a[1][0], -a[0][1], a[0][0]);
		
			mt.transpon();
			for (int i = 0; i < a.length; i++) {
				for (int j = 0; j < a[i].length; j++)
					mt.a[i][j] = mt.a[i][j] / d;
			}
			return mt;
		}
	}
	
	public Matrix2x2 equivalentDiagonal() throws ArithmeticException{

		if (a[0][0] == 0) {
			throw new ArithmeticException("I can't reduce the matrix to diagonal form because a[0][0] = 0 :(");
		}
		else {
			Matrix2x2 mt = new Matrix2x2(a[0][0], 0, 0, a[1][1] - a[1][0] * a[0][1] / a[0][0]);
			return mt;
		}
	}
	
	public Vector2D multVector(Vector2D vec){
		Vector2D vec1 = new Vector2D();
		
		vec1.setX(a[0][0] * vec.getX() + a[0][1] * vec.getY());
		vec1.setY(a[1][0] * vec.getX() + a[1][1] * vec.getY());
			
		return vec1;
		
	}

}