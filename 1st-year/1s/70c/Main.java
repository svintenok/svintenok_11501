
public class Main {
	
	public static void main(String [] args){


		try {
			RationalFraction rf1 = new RationalFraction(5, 0);
			RationalFraction rf2 = new RationalFraction(3, 4);

			RationalFraction rf5 = rf1.add(rf2);
			System.out.println(rf5);
		} catch (RationalFractionException e) {
			System.out.println("Sorry, denominator can't be zero.");
		}
		
		///////////////////////////////////////

		Matrix2x2 mt1 = new Matrix2x2(new double [][] {{4, 2}, {2, 1}});
		Matrix2x2 mt2 = new Matrix2x2(new double [][] {{0, 2}, {3, 4}});

		try {
			Matrix2x2 mt3 = mt1.inverseMatrix();
			System.out.println(mt3);
		} catch (NoInverseMatrixException e) {
			System.out.println("There isn't inverse matrix when the determinant is zero.");
		}

		Matrix2x2 mt4 = new Matrix2x2();
		try {
			mt4 = mt2.equivalentDiagonal();
		} catch (ArithmeticException e) {
			if (mt2.getA(1, 0) != 0) {
				Matrix2x2 mt5 = new Matrix2x2(new double[][]{{mt2.getA(1, 0), 0}, {mt2.getA(1, 1), mt2.getA(0, 1)}});
				mt4 = mt5.equivalentDiagonal();
			} else {
				mt4 = new Matrix2x2(new double[][]{{0, 0}, {0, mt2.getA(1, 1)}});
			}
		}
		finally {
			System.out.println(mt4);
		}

	}
}