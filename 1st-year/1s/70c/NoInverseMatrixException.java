/**
 * Created by DNS on 12.12.2015.
 */
public class NoInverseMatrixException extends Exception{
    public NoInverseMatrixException(String s){
        super(s);
    }
}
