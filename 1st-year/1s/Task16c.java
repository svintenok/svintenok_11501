/**
* @author Svintenok Katya
* 11501
* 16c
*/
import java.util.Scanner;

public class Task16c {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		double r = 1;
		for (int i = 2; i <= 2 * n; i += 2) 
			r = r * i / (i - 1.0)  * i / (i + 1.0);
		System.out.println(r);
	}
}