/**
* @author Svintenok Katya
* 11501
* 17c
*/
import java.lang.Math;

public class Task17c {
	public static void main(String[] args){
		double x = Double.parseDouble(args[0]);
		int n = Integer.parseInt(args[1]);
		double s = 0;
		for (int i = 1; i <= n; i++)
			s = Math.cos(x + s);
		System.out.println(s);
	}
}