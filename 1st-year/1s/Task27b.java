/**
* @author Svintenok Katya
* 11501
* 27b
*/

public class Task27b {
	public static void main(String[] args) {
		final double EPS = 1e-8;
		long k = 1;
		double s = 0;
		double a;
	    double x = Double.parseDouble(args[0]);
		double t = 1;
		long f1 = 1;
		long f2 = 1;
		do {
			t = t * x;
			if (k % 2 == 0) {
				f1 = f1 * k;
				a = t / f1;
			}
			else {
				f2 = f2 * k;
				a = t / f2;
			}
			s = s + a;
			k++;
		}
		while (Math.abs(a) > EPS);
		System.out.println(s);
	}
}