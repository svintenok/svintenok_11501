/**
* @author Svintenok Katya
* 11501
* 46b
*/
public class Task46b {
	
	//конвертация строки в массив из цифр
	public static int conversion(String n, int [] a) {
		
		int size = n.length();
		for (int i = 0; i < size; i++)
			a[i] = n.charAt(size - 1 - i) - '0';

		return size;
	}
	
	//произведение цифр
	public static int compozitionOfNumeral(int [] a, int a1, int a2, int r, int i1, int i2) {
		int p = a1 * a2 + r + a[i1 + i2];
		a[i1 + i2] = p % 10;	
		return p / 10;
	}

	//произведение числа на цифру
	public static void compozitionOnNumeral(int [] a, int [] a1, int a2, int size1, int i1) {
		int r = 0;
		int i2;
		for(i2 = 0; i2 < size1; i2++) {		
				r = compozitionOfNumeral(a, a1[i2], a2, r, i1, i2);
		}
			
		a[i1 + i2] =a[i1 + i2] + r;
	}
	//произведение чисел
	public static void compozitionOnNumber(int [] a, int [] a1, int [] a2, int size1, int size2) {
		
		for(int i1 = 0; i1 < size2; i1++)
			compozitionOnNumeral(a, a1, a2[i1], size1, i1);
		
		writing(a, size1, size2);
	}
	
	//вывод проиведения
	public static void writing(int [] a, int size1, int size2) {
		
		int size = a[size1 + size2 - 1] == 0? size1 + size2 - 2: size1 + size2 - 1;
		
			for (int i = size; i >= 0; i--)
		System.out.print(a[i]);	
	}
	
	public static void main(String[] args) {
		
		final int CAPACITY = 10000; 
		int [] a1 = new int[CAPACITY];
		int [] a2 = new int[CAPACITY];
		int [] a = new int[CAPACITY];

		String n = args[0];
		int size1 = conversion(n, a1);
		
		n = args[1];
		int size2 = conversion(n, a2);
		
		for (int i = 0; i < size1 + size2; i++)
			a[i] = 0;
		
		compozitionOnNumber(a, a1, a2, size1, size2);	
	}
}