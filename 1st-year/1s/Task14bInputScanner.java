/**
* @author Svintenok Katya
* 11501
* 14bInputScanner
*/
import java.util.Scanner;

public class Task14bInputScanner {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int x  = sc.nextInt();
		double y = x > 1 ? (x * x - 1.0) / (3 * x +5) : x >= 0 ? 2 * x - 5 : (x - 1.0) / (x + 3);
		System.out.println(y);
	}
}