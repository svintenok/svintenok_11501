import java.io.*;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by DNS on 20.05.2016.
 */
public class Task1 {
    public static void main(String[] args) throws IOException {
        LinkedList<Integer> linkedList = new LinkedList<>();
        Scanner scanner = new Scanner(new File("input.txt"));

        while (scanner.hasNextInt()) {
            int x = scanner.nextInt();
            linkedList.add(x);
            if (x == 0) {
                if (scanner.hasNextInt()) {
                    int y = scanner.nextInt();
                    linkedList.add(y);
                    linkedList.add(y);
                }
                else
                    linkedList.add(-1);
            }
        }

        System.out.println(linkedList);

        FileOutputStream os = new FileOutputStream("out.txt");

        for (int i = 0; i < linkedList.size(); i++)
            os.write(linkedList.get(i));

        os.close();
    }
}
