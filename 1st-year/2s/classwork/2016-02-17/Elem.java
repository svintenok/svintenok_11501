/**
 * Created by DNS on 17.02.2016.
 */
public class Elem {

    private int value;
    private  Elem next;


    public Elem(int value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public Elem() {
    }

    public void setValue(int value) {
        this.value = value;
    }

    public void setNext(Elem next) {
        this.next = next;
    }

    public int getValue() {
        return value;
    }

    public Elem getNext() {
        return next;
    }
}
