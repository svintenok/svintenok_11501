import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by DNS on 19.03.2016.
 */
public class Task025 {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("in.txt"));
        ArrayList<Integer> arrayList = new ArrayList<>();

        while (scanner.hasNextInt())
            arrayList.add(scanner.nextInt());

        System.out.println(arrayList);

        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(new Comparator<Integer>() {
            @Override
            public int compare(Integer n1, Integer n2) {
                for (int i = 0; n1 > 0 && n2 > 0; i++) {
                    n1 = n1 / 10;
                    n2 = n2 / 10;
                }
                if (n2 == 0)
                    return 1;
                return -1;
            }
        });

        for (int i = 0; !arrayList.isEmpty(); i++)
            priorityQueue.add(arrayList.remove(0));


        while (!priorityQueue.isEmpty())
            arrayList.add(priorityQueue.poll());

        FileWriter writer = new FileWriter(new File("out.txt"));

        for (int i = 0; i < arrayList.size(); i++)
            writer.write(arrayList.get(i) + " ");

        writer.flush();

        System.out.println(arrayList);
    }
}
