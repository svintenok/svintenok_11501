import java.util.Comparator;

/**
 * Task025
 */
public class PriorityQueue<T> extends MyLinkedQueue<T> {

    private Comparator<T> comparator;

    public PriorityQueue(Comparator<T> comparator) {
        this.comparator = comparator;
    }

    @Override
    public boolean add(T t) {
        if (isEmpty()) {
            head = new Elem<>(t, null);
            return true;
        }

        if(comparator.compare(t, head.getValue()) < 0){
            head.setNext(new Elem<T>(head.getValue(), head.getNext()));
            head.setValue(t);
            return true;
        }

        Elem<T> p = head;
        while (p.getNext() != null && comparator.compare(t, (T) p.getNext().getValue()) > 0)
            p = p.getNext();
        p.setNext(new Elem<T>(t, p.getNext()));
        return true;
    }

    @Override
    public boolean offer(T t) {
        if (isEmpty()) {
            head = new Elem<>(t, null);
            return true;
        }
        Elem<T> p = head;
        while (p.getNext()!= null && comparator.compare(p.getValue(), t) <= 0)
            p = p.getNext();
        p.setNext(new Elem<>(t, p.getNext()));
        return true;
    }
}
