import java.util.Collection;
import java.util.Deque;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Task023
 */
public class MyLinkedDeque<T> extends MyLinkedQueue<T> implements Deque<T> {
    @Override
    public void addFirst(T t) {
        Elem<T> elem = new Elem<>(t, head);
        head = elem;
    }

    @Override
    public void addLast(T t) {
        add(t);
    }

    @Override
    public boolean offerFirst(T t) {
        Elem<T> elem = new Elem<>(t, head);
        head = elem;
        return true;
    }

    @Override
    public boolean offerLast(T t) {
        return offer(t);
    }

    @Override
    public T removeFirst() {
        return remove();
    }

    @Override
    public T removeLast() {
        if (isEmpty())
            throw new NoSuchElementException();
        T t;
        if (head.getNext() == null) {
            t = head.getValue();
            head = null;
        }
        else {
            Elem<T> p = head;

            while (p.getNext().getNext() != null)
                p = p.getNext();
            t = (T) p.getNext().getValue();
            p.setNext(null);
        }
        return t;
    }

    @Override
    public T pollFirst() {
        return poll();
    }

    @Override
    public T pollLast() {
        if (isEmpty())
            return null;
        T t;
        if (head.getNext() == null) {
            t = head.getValue();
            head = null;
        }
        else {
            Elem<T> p = head;

            while (p.getNext().getNext() != null)
                p = p.getNext();
            t = (T) p.getNext().getValue();
            p.setNext(null);
        }
        return t;
    }

    @Override
    public T getFirst() {
        return element();
    }

    @Override
    public T getLast() {
        if (isEmpty())
            throw new NoSuchElementException();

        Elem<T> p = head;

        while (p.getNext() != null)
            p = p.getNext();

        return p.getValue();
    }

    @Override
    public T peekFirst() {
        return peek();
    }

    @Override
    public T peekLast() {
        if (isEmpty())
            return null;
        Elem<T> p = head;

        while (p.getNext() != null)
            p = p.getNext();

        return p.getValue();
    }


    @Override
    public void push(T t) {
        addFirst(t);
    }

    @Override
    public T pop() {
        return removeFirst();
    }


    ////////////////////////////////////////////////////////////////
    @Override
    public boolean removeFirstOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean removeLastOccurrence(Object o) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public Iterator<T> descendingIterator() {
        return null;
    }
}
