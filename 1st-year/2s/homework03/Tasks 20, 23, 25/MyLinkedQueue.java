import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * Task020
 */
public class MyLinkedQueue<T> implements Queue<T> {
    protected Elem<T> head = null;

    @Override
    public boolean add(T t) {
        if (isEmpty()) {
            head = new Elem<>(t, null);
            return true;
        }
        Elem<T> p = head;
        while (p.getNext()!= null)
            p = p.getNext();
        p.setNext(new Elem<>(t, null));
        return true;
    }

    @Override
    public boolean offer(T t) {
        if (isEmpty()) {
            head = new Elem<>(t, null);
            return true;
        }
        Elem<T> p = head;
        while (p.getNext()!= null)
            p = p.getNext();
        p.setNext(new Elem<>(t, null));
        return true;
    }

    @Override
    public T remove() {
        if (isEmpty())
            throw new NoSuchElementException();
        T t = head.getValue();
        head = head.getNext();
        return t;
    }

    @Override
    public T poll() {
        if (isEmpty())
            return null;
        T t = head.getValue();
        head = head.getNext();
        return t;
    }

    @Override
    public T element() {
        if (isEmpty())
            throw new NoSuchElementException();
        return head.getValue();
    }

    @Override
    public T peek() {
        if (isEmpty())
            return null;
        return head.getValue();
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    ///////////////////////////////////////////////////////
    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }


    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }


    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {
    }
}
