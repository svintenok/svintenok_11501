import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

/**
 * Created by DNS on 19.03.2016.
 */
public class Task024 {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(new File("in.txt"));
        ArrayList<Integer> arrayList = new ArrayList<>();

        while (scanner.hasNextInt())
            arrayList.add(scanner.nextInt());


        Collections.sort(arrayList, new Comparator<Integer>() {
            @Override
            public int compare(Integer n1, Integer n2) {
                for (int i = 0; n1 > 0 && n2 > 0; i++) {
                    n1 = n1 / 10;
                    n2 = n2 / 10;
                }
                if (n2 == 0)
                    return 1;
                return -1;
            }
        });

        FileWriter writer = new FileWriter(new File("out.txt"));

        for (int i = 0; i < arrayList.size(); i++)
            writer.write(arrayList.get(i) + " ");

        writer.flush();
        System.out.println(arrayList);
    }
}
