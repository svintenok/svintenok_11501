import java.util.Scanner;

/**
 * Created by DNS on 20.03.2016.
 */
public class Task019 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = new String(scanner.nextLine());
        String[] strings = string.split("\\s");
        MyLinkedStack<Character> stack = new MyLinkedStack<>();
        String postfix = new String();

        for (int i = 0; i < strings.length; i++) {
            if (strings[i].equals("*") || strings[i].equals("/") || strings[i].equals("+") || strings[i].equals("-")) {
                if (stack.isEmpty() || stack.top() == '(')
                    stack.push(strings[i].charAt(0));
                else
                if ((strings[i].equals("*") || strings[i].equals("/")) && (stack.top() == '-' || stack.top() == '+'))
                stack.push(strings[i].charAt(0));
                else {
                    while (!stack.isEmpty() && stack.top() != '(' && !((strings[i].equals("*") || strings[i].equals("/")) && (stack.top() == '-' || stack.top() == '+')))
                        postfix = postfix + stack.pop() + " ";
                    stack.push(strings[i].charAt(0));
                }
            }

            else  if (strings[i].equals("("))
                stack.push('(');

            else if (strings[i].equals(")")) {
                while (stack.top() != '(')
                    postfix = postfix + stack.pop() + " ";
                stack.pop();
            }
            else postfix = postfix + strings[i] + " ";
        }

        while (!stack.isEmpty())
            postfix = postfix + stack.pop() + " ";

        System.out.println(postfix);
    }
}

