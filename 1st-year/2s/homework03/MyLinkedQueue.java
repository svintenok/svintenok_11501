import java.util.Collection;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Queue;

/**
 * Task021
 */
public class MyLinkedQueue<T> implements Queue<T> {

    protected MyLinkedStack<T> head = new MyLinkedStack<>();
    protected MyLinkedStack<T> tail = new MyLinkedStack<>();

    public void push() {
        while (!head.isEmpty())
            tail.push(head.pop());
    }

    @Override
    public boolean offer(T el) {
        head.push(el);
        return true;
    }

    @Override
    public T poll() {
        if(!isEmpty()) {
            if (tail.isEmpty())
                push();
            return tail.pop();
        }
        return null;
    }

    @Override
    public T peek() {
        if(!isEmpty()) {
            if (tail.isEmpty())
                push();
            return tail.top();
        }
        return null;
    }

    @Override
    public T remove() {
        if (isEmpty())
            throw new NoSuchElementException();
        if (tail.isEmpty())
            push();
        return tail.pop();
    }

    @Override
    public boolean add(T el){
        head.push(el);
        return true;
    }

    @Override
    public T element() {
        if (isEmpty())
            throw new NoSuchElementException();
        if (tail.isEmpty())
            push();
        return tail.top();
    }

    @Override
    public boolean isEmpty() {
        return head.isEmpty() && tail.isEmpty();
    }


    ///////////////////////////////////////////////////////

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
    }

    @Override
    public void clear() {

    }
}
