/**
 * Task016
 */
public class MyLinkedStack<T> implements MyStack<T> {
    protected Elem<T> head = null;

    @Override
    public void push(T el) {
        Elem<T> elem = new Elem<>(el, head);
        head = elem;
    }

    @Override
    public T pop() {
        T elem = head.getValue();
        head = head.getNext();
        return elem;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public T top() {
        return head.getValue();
    }
}
