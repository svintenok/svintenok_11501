import java.util.Scanner;

/**
 * Created by DNS on 17.03.2016.
 */
public class Task017 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = new String(scanner.next());
        MyLinkedStack<Character> stack = new MyLinkedStack<>();

        boolean flag = true;

        for (int i = 0; i < string.length() && flag; i++){
            char symbol = string.charAt(i);
            if (symbol == '(' || symbol == '{' || symbol == '[')
                stack.push(string.charAt(i));
            else {
                if (symbol == ')' || symbol == '}' || symbol == ']') {
                    if (stack.isEmpty()) {
                        flag = false;
                        System.out.println("Встретилась лишняя закрывающаяся скобка на позиции " + i);
                    }
                    else {
                        char el = stack.pop();
                        if ((symbol == ')' && el != '(') || (symbol == '}' && el != '{') || (symbol == ']' && el != '[')) {
                            flag = false;
                            System.out.println("Скобки не соответствуют друг другу: Позиция " + i);
                        }
                    }

                }
            }
        }


        if (flag) {
            if (!stack.isEmpty())
                System.out.println("Не все открывающиеся скобки закрыты");
            else
                System.out.println("Расстановка скобок правильная");
        }
    }
}
