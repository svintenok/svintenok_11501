import java.util.Scanner;

/**
 * Created by DNS on 17.03.2016.
 */
public class Task018 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String string = new String(scanner.nextLine());
        MyLinkedStack<Integer> stack = new MyLinkedStack<>();

        String[] strings = string.split(" ");

        for (int i = 0; i < strings.length; i++) {
            if (strings[i].equals("+"))
                stack.push(stack.pop() + stack.pop());
            else if (strings[i].equals("-"))
                stack.push(stack.pop() - stack.pop());
            else if (strings[i].equals("*"))
                stack.push(stack.pop() * stack.pop());
            else if (strings[i].equals("/"))
                stack.push(stack.pop() / stack.pop());
            else stack.push(Integer.parseInt(strings[i]));
        }

        System.out.println(stack.pop());
    }
}
