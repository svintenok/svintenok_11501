/**
 * Task016
 */
public interface MyStack<T> {
    void push(T el);
    T pop();
    boolean isEmpty();
    T top();
}
