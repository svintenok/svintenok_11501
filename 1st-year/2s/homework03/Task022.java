/**
 * Created by DNS on 18.03.2016.
 */
public class Task022 {
    public static void main(String[] args) {
        MyLinkedQueue<Integer> queue2 = new MyLinkedQueue<>();
        MyLinkedQueue<Integer> queue3 = new MyLinkedQueue<>();
        MyLinkedQueue<Integer> queue5 = new MyLinkedQueue<>();

        queue2.add(2);
        queue3.add(3);
        queue5.add(5);

        int n = 20;
        int minEl;

        for (int i = 0; i < n; i++) {

            if (queue2.element() < queue3.element() && queue2.element() < queue5.element())
                minEl = queue2.remove();
            else if (queue3.element() < queue5.element())
                minEl = queue3.remove();
            else minEl = queue5.remove();

            queue2.add(2 * minEl);
            queue3.add(3 * minEl);
            queue5.add(5 * minEl);

            if (queue2.element() == minEl) queue2.remove();
            if (queue3.element() == minEl) queue3.remove();
            if (queue5.element() == minEl) queue5.remove();

            System.out.print(minEl + " ");
        }
    }
}
