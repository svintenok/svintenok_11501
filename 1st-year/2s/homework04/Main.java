import java.util.TreeMap;

/**
 * Created by DNS on 27.04.2016.
 */
public class Main {
    public static void main(String[] args) {
        Graph graph = new Graph();
        graph.addEdge("1", "2");
        graph.addEdge("1", "4");
        graph.addEdge("1", "5");
        graph.addEdge("2", "3");
        graph.addEdge("3", "5");
        graph.addEdge("4", "5");

        System.out.println(graph);

        TreeMap<String, Color> colors = graph.coloring();
        System.out.println(colors);
    }
}
