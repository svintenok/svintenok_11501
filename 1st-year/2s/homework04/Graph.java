import java.util.ArrayList;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Created by DNS on 27.04.2016.
 */
public class Graph {
    private TreeMap<String, Set<String>> listOfAdjacentTops = new TreeMap<>();


    public void addTop(String name) {
        if (!listOfAdjacentTops.containsKey(name))
            listOfAdjacentTops.put(name, new TreeSet<String>());
    }

    public void  addEdge(String name1, String name2) {
        if (!listOfAdjacentTops.containsKey(name1))
            addTop(name1);
        if (!listOfAdjacentTops.containsKey(name2))
            addTop(name2);
        listOfAdjacentTops.get(name1).add(name2);
        listOfAdjacentTops.get(name2).add(name1);
    }

    public boolean hasEdge(String name1, String name2){
        if (listOfAdjacentTops.get(name1).contains(name2))
            return true;
        return false;
    }

    public int size() {
        return listOfAdjacentTops.size();
    }

    public Set<String> getTops() {
        return  listOfAdjacentTops.keySet();
    }


    private TreeMap<String, Color> colorsOfTops = new TreeMap<>();
    private ArrayList<String> tops = new ArrayList<>();

    public TreeMap<String, Color> coloring() {
        for (String top : getTops()) {
            colorsOfTops.put(top, null);
            tops.add(top);
        }

        coloringOfTop(0);
        return colorsOfTops;
    }

    private boolean coloringOfTop(int numberOfTop) {

        String name = tops.get(numberOfTop);
        Set<String> AdjacentTops = listOfAdjacentTops.get(name);

        int n = 0;
        if (colorsOfTops.get(name) != null)
            n = colorsOfTops.get(name).ordinal();

        for (Color color : Color.values()){

            boolean flag = true;
            for (String top : AdjacentTops) {
                if (colorsOfTops.get(top) == color || color.ordinal() <= n) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                colorsOfTops.put(name, color);
                if (numberOfTop < size() - 1)
                    return coloringOfTop(numberOfTop + 1);
                return true;
            }
        }
        colorsOfTops.put(name, null);
        return coloringOfTop(numberOfTop - 1);
    }

    @Override
    public String toString() {
        return listOfAdjacentTops.toString();
    }
}