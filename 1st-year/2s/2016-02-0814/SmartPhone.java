/**
 * Created by DNS on 12.02.2016.
 */
public class SmartPhone extends Notebook implements Calling {

    public SIM sim;

    public SmartPhone(String firm, String model, SIM sim) {
        super(firm, model);
        this.sim = sim;
    }

    @Override
    public void call(Calling phone) {
        System.out.println("I am calling");
    }

    @Override
    public void takeCall() {
        System.out.println("I am taking call");
    }

    @Override
    public void saveNumber(String number) {
        System.out.println("I am saving contact ");
    }
}
