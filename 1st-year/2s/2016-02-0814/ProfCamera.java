/**
 * Created by DNS on 12.02.2016.
 */
public class ProfCamera extends Camera {

    private int iso = 100;
    private double shutterSpeed = 200, diaphragm = 2;

    public ProfCamera(String firm, String model) {
        super(firm, model);
    }

    void changeShutterSpeed(double n) {
        this.shutterSpeed = this.shutterSpeed + n;
        System.out.println("shutterSpeed = " + this.shutterSpeed);
    }
    void changedDiaphragm(double n) {
        this.diaphragm = this.diaphragm + n;
        System.out.println("diaphragm = " + this.diaphragm);
    }

    public void takePhoto() {

        System.out.println("I am taking cool photo");
    }
}
