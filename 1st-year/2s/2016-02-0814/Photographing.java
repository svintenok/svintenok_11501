/**
 * Created by DNS on 12.02.2016.
 */
public interface Photographing {

    void takePhoto();
    void turnOnFlash();
    void makeVideo();
}
