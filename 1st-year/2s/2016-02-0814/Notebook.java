/**
 * Created by DNS on 12.02.2016.
 */
public class Notebook extends Player implements WithInternet {

    public Notebook(String firm, String model) {
        super(firm, model);
    }

    @Override
    public void connectToWifi(String name) {
        System.out.println("I am connecting with wi-fi " + name);
    }

    @Override
    public void connectToInternet() {
        System.out.println("I am connecting with Internet with modem");
    }

    @Override
    public void giveInternet(WithInternet device) {

        System.out.println("I am giving Internet to " + device);
    }

    @Override
    public String toString() {

        return "Notebook{" +this.firm + ", " + this.model + '}';
    }
}
