/**
 * Created by DNS on 12.02.2016.
 */
public class Phone extends Player implements Calling {

    public SIM sim;

    public Phone(String model, String firm, SIM sim) {
        super(model, firm);
        this.sim = sim;
    }

    @Override
    public void call(Calling phone) {
        System.out.println("I am calling");
    }

    @Override
    public void takeCall() {

        System.out.println("I am taking call");
    }

    @Override
    public void saveNumber(String number) {
        System.out.println("I am saving contact " + number);
    }

    public SIM getSim() {
        return sim;
    }
}
