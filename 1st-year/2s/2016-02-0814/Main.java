/**
 * Created by DNS on 12.02.2016.
 */
public class Main {
    public static void main(String[] args) {

        SIM sim = new SIM("mts","micro","89543241243");
        Notebook notebook = new Notebook("Asus", "s301l");
        SmartPhone smartPhone = new  SmartPhone("Asus", "Zenfone", sim);

        GPS gps = new GPS("Samsung", "d530");
        gps.determineLocation();
        gps.determineWay("Mirny", "Zorge");
        gps.downloadMaps(notebook);

        Player player = new Player("Sony", "f432");
        player.playMusic();
        player.downloadMusic(notebook);
        player.changeVolume(-5);

        Camera camera = new Camera("Sony", "w800s");
        camera.takePhoto();
        camera.makeVideo();
        camera.turnOnFlash();

        ProfCamera profCamera = new ProfCamera("Canon", "D650");
        profCamera.takePhoto();
        profCamera.changedDiaphragm(1.5);
        profCamera.changeShutterSpeed(-20);

        notebook.connectToInternet();
        notebook.connectToWifi("Kate");
        notebook.giveInternet(smartPhone);

        Phone phone = new Phone("Nokia", "one", sim);
        phone.call(smartPhone);
        phone.takeCall();
        phone.saveNumber("89776324081");

        smartPhone.call(phone);
        smartPhone.takeCall();
        smartPhone.saveNumber("89776324081");

        Human human = new Human("Masha", smartPhone, notebook);
        human.toCall(phone);
        human.goOnline();

        Photographing [] photographings = new Photographing[2];

        photographings[0] = new Camera("Sony", "w800s");
        photographings[1] = profCamera;

        for (Photographing photographing : photographings) {
            photographing.takePhoto();
        }
    }
}
