/**
 * Created by DNS on 12.02.2016.
 */
public class Camera implements Photographing {

    private MemoryCard memoryCard;
    private String firm, model;

    public Camera(String firm, String model) {
        this.firm = firm;
        this.model = model;
    }

    @Override
    public void takePhoto() {

        System.out.println("I am taking photo");
    }

    @Override
    public void turnOnFlash() {
        System.out.println("Flash is on");
    }

    @Override
    public void makeVideo() {
        System.out.println("I am making video");
    }
}
