/**
 * Created by DNS on 12.02.2016.
 */
public class GPS implements Navigating {

    private String firm, model;
    private int versionOfMaps = 1;

    public GPS(String firm, String model) {
        this.firm = firm;
        this.model = model;
    }

    @Override
    public void determineLocation() {

        System.out.println("I am determining location");
    }

    @Override
    public void determineWay(String departurePoint, String arrivalPoint) {
        System.out.println("I am determining way");
    }

    @Override
    public void downloadMaps(WithInternet device) {
        this.versionOfMaps = this.versionOfMaps + 1;
        System.out.println("I am dowloading maps with " + device);
    }
}
