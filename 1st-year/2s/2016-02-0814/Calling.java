/**
 * Created by DNS on 12.02.2016.
 */
public interface Calling {

    void call(Calling phone);
    void takeCall();
    void saveNumber(String number);
}
