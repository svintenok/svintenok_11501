/**
 * Created by DNS on 12.02.2016.
 */
public class Player implements PlayingMusic {

    protected String firm, model;
    private int volume = 20;

    public Player(String firm, String model) {
        this.firm = firm;
        this.model = model;
    }

    @Override
    public void playMusic() {
        System.out.println("I am playing music");
    }

    @Override
    public void changeVolume(int n) {
        this.volume = this.volume + n;
        System.out.println("volume = " + this.volume);
    }

    @Override
    public void downloadMusic(WithInternet device) {

        System.out.println("I am dowloading music with " + device);
    }

}
