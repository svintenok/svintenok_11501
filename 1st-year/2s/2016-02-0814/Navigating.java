/**
 * Created by DNS on 12.02.2016.
 */
public interface Navigating {

    void determineLocation();
    void determineWay(String departurePoint, String arrivalPoint);
    void downloadMaps(WithInternet device);
}
