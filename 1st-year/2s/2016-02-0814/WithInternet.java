/**
 * Created by DNS on 12.02.2016.
 */
public interface WithInternet {

    void connectToWifi(String name);
    void connectToInternet();
    void giveInternet(WithInternet device);
}
