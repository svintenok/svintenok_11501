/**
 * Created by DNS on 12.02.2016.
 */
public interface PlayingMusic {

    void playMusic();
    void changeVolume(int n);
    void downloadMusic(WithInternet device);
}
