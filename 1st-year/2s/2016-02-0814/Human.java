/**
 * Created by DNS on 12.02.2016.
 */
public class Human {
    private String name;
    private Calling phone;
    private WithInternet device;


    public Human(String name, Calling phone, WithInternet device) {
        this.name = name;
        this.phone = phone;
        this.device = device;
    }

    public void toCall(Calling phone1){
        phone.call(phone1);
    }

    public void goOnline(){
        device.connectToInternet();
    }
}
