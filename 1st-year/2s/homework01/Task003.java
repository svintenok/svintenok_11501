import java.util.Random;

/**
 * Created by DNS on 26.02.2016.
 */
public class Task003 {
    public static void main(String[] args) {
        Random r = new Random();
        Elem head = null;
        Elem p;

        int n = 10;

        for (int i = 0; i < n; i++) {
            p = new Elem(r.nextInt(100), head);
            head = p;
        }

        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
        System.out.println();

        int numberMax = 0;
        int numberMin = 0;

        boolean flagMax = true;
        boolean flagMin = true;

        for (p = head; p.getNext() != null; p = p.getNext()) {
            if (p.getValue() < p.getNext().getValue()) {
                if (flagMin)
                    numberMin = numberMin + 1;
                flagMax = true;
                flagMin = false;
            } else {
                if (p.getValue() > p.getNext().getValue()) {
                    if (flagMax)
                        numberMax = numberMax + 1;
                    flagMin = true;
                    flagMax = false;
                } else {
                    flagMin = false;
                    flagMax = false;
                }
            }
        }
        if (flagMax)
            numberMax = numberMax + 1;
        else if (flagMin)
            numberMin = numberMin + 1;

        System.out.println("Number of max: " + numberMax + "\nNumber of min: " + numberMin);
    }
}
