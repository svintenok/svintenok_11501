import java.util.Random;
import java.util.Scanner;

/**
 * Created by DNS on 26.02.2016.
 */
public class Task007 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        Elem head = null;
        Elem p;

        int n = 10;

        for (int i = 0; i < n; i++) {
            p = new Elem(r.nextInt(100), head);
            head = p;
        }

        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
        System.out.println();
        int x = sc.nextInt();


        for (p = head; p != null; p = p.getNext()) {
            if (p.getValue() % 2 == 0) {

                if (p.getNext() != null)
                    p.setNext(new Elem(p.getValue(), p.getNext()));
                else
                    p.setNext(new Elem(p.getValue(), null));
                p.setValue(x);
                p = p.getNext();
            }
        }

        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
    }
}
