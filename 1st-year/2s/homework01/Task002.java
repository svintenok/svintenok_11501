import java.util.Random;

/**
 * Created by DNS on 26.02.2016.
 */
public class Task002 {
    public static void main(String[] args) {
        Random r = new Random();
        Elem head = null;
        Elem p;

        int n = 10;

        for (int i = 0; i < n; i++) {
            p = new Elem(r.nextInt(100), head);
            head = p;
        }

        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
        System.out.println();

        long pr = 1;

        for (p = head; p != null && p.getNext() != null; p = p.getNext().getNext()) {
            pr = pr * p.getValue();
        }
        System.out.println(pr);
    }
}