import java.util.Random;

/**
 * Created by DNS on 22.02.2016.
 */

public class Task001 {


    public static void main(String[] args) {

        Random r = new Random();
        Elem head = null;
        Elem tail = null;
        Elem p;

        p = new Elem(r.nextInt(100));
        tail = p;
        head = p;

        int n = 9;

        for (int i = 0; i < n; i++) {
            p = new Elem(r.nextInt(100));
            tail.setNext(p);
            tail = p;
        }

        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
        System.out.println();
    }
}
