import java.util.Random;
import java.util.Scanner;

/**
 * Created by DNS on 26.02.2016.
 */
public class Task010 {
    public static void toSortArray(int[] array) {
        boolean flag = false;

        for (int i = 0; i < array.length && !flag; i++) {
            boolean flag2 = false;
            for (int j = 0; j < array.length - 1 - i; j++) {
                if (array[j] > array[j + 1]) {
                    int b = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = b;
                    flag2 = true;
                }
            }
            if (!flag2)
                flag = true;
        }
        for (int i = 0; i < array.length; i++)
            System.out.print(array[i] + " ");
        System.out.println();
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();

        int n = sc.nextInt();
        int[] array = new int[n];

        for (int i = 0; i < n; i++) {
            array[i] = r.nextInt(100);
            System.out.print(array[i] + " ");
        }
        System.out.println();

        toSortArray(array);
        int x = sc.nextInt();

        int mid = array.length / 2;
        int first = 0;
        boolean flag = false;
        for (int i = 0; i < array.length/2 + 1 && !flag; i++) {
            if (array[first + mid] == x) {
                flag = true;
                System.out.println(first + mid + 1);
            } else {
                if (array[first + mid] < x)
                    first = first + mid;
            }
            mid = mid > 1? mid / 2 : 1;

        }
        if (!flag)
            System.out.println(false);
    }
}

