import java.util.Random;

/**
 * Created by DNS on 26.02.2016.
 */
public class Task009 {
    public static void main(String[] args) {
        Random r = new Random();
        Elem head = null;
        Elem p;

        int n = 10;

        for (int i = 0; i < n; i++) {
            p = new Elem(r.nextInt(2), head);
            head = p;
        }

        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
        System.out.println();

        int number2 = 0;
        for (p = head; p != null; p = p.getNext())
            number2 = number2 * 10 + p.getValue();

        int number10 = 0;
        int f = 1;
        while (number2 > 0) {
            number10 = number10 + number2 % 10 * f;
            f = f * 2;
            number2 = number2 / 10;
        }

        head = null;
        while (number10 > 0) {
            p = new Elem((number10 % 10), head);
            head = p;
            number10 = number10 / 10;
        }


        for (p = head; p.getNext() != null; p = p.getNext()) {
            System.out.print(p.getValue() + " -> ");
        }
        System.out.print(p.getValue());
    }
}
