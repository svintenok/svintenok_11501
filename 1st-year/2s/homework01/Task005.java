import java.util.Random;

/**
 * Created by DNS on 26.02.2016.
 */
public class Task005 {

    public static boolean have5(int t) {
        boolean flag = false;

        while (t > 0 ) {
            if (t % 10 == 5)
                return true;
            t = t / 10;
        }

        return false;
    }
    public static void main(String[] args) {
        Random r = new Random();
        Elem head = null;
        Elem p;

        int n = 10;

        for (int i = 0; i < n; i++) {
            p = new Elem(r.nextInt(100), head);
            head = p;
        }

        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
        System.out.println();

        while (have5(head.getValue())){
            head = head.getNext();
        }

        p = head;
        while (p.getNext().getNext() != null) {

            if (have5(p.getNext().getValue())) {
                p.setNext(p.getNext().getNext());
            }
            else
                p = p.getNext();
        }

        if (have5(p.getNext().getValue()))
            p.setNext(null);


        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
        System.out.println();
    }
}
