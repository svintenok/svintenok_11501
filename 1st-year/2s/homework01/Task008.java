import java.util.Random;

/**
 * Created by DNS on 26.02.2016.
 */
public class Task008 {
    public static void main(String[] args) {
        Random r = new Random();
        Elem head = null;
        Elem p;

        int n = 10;

        for (int i = 0; i < n; i++) {
            p = new Elem(r.nextInt(100), head);
            head = p;
        }

        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
        System.out.println();


        for (p = head; p != null; p = p.getNext()) {
            boolean flag = true;

            for (int i = 2; i <= p.getValue() / 2; i++) {
                if (p.getValue() % i == 0)
                    flag = false;
            }
            if (flag) {
                int k = p.getValue();
                while (k >= 10)
                    k = k / 10;

                if (p.getNext() != null) {
                    p.setNext(new Elem(p.getValue() % 10, p.getNext()));
                    p.setNext(new Elem(p.getValue(), p.getNext()));
                }
                else {
                    p.setNext(new Elem(p.getValue() % 10, null));
                    p.setNext(new Elem(p.getValue(), p.getNext()));
                }
                p.setValue(k);
                p = p.getNext().getNext();
            }
        }

        for (p = head; p != null; p = p.getNext()) {
            System.out.print(p.getValue() + " ");
        }
    }
}
