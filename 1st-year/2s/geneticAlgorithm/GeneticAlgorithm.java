import java.util.*;

public class GeneticAlgorithm {
    final static int POPULATIONSIZE = 50;
    static String ideal;

    public static int compareWithIdeal(String s) {
        int distance = 0;
        for (int i = 0; i < ideal.length(); i++)
            distance = distance + Math.abs(ideal.charAt(i) - s.charAt(i));
        return distance;
    }

    public static void main(String[] args) {
        final int POPULATIONSIZE = 100;

        //задание идеала
        Scanner scanner = new Scanner(System.in);
        ideal = scanner.next();
        int lengthOfWord = ideal.length();

        Random random = new Random();

        //генерирование первой популяции
        ArrayList<String> nextPopulation = new ArrayList<>();
        ArrayList<String> population = new ArrayList<>();

        for (int i = 0; i < POPULATIONSIZE; i++){
            String word = new String();

            for (int j = 0; j < lengthOfWord; j++)
                word = word + (char) ('A' + random.nextInt(26));

            nextPopulation.add(word);
        }
        System.out.println(nextPopulation);


        boolean flag = false;
        int numberOfPopulation = 1;

        //генерирование следующих популяций
        while (!flag) {

            //сортировка
            Collections.sort(nextPopulation, new Comparator<String>() {
                @Override
                public int compare(String s1, String s2) {
                    if (compareWithIdeal(s1) < compareWithIdeal(s2))
                        return -1;
                    else if (compareWithIdeal(s1) == compareWithIdeal(s2))
                        return 0;
                    return 1;
                }
            });

            //выбор ста лучших
            population.clear();
            population.addAll(nextPopulation.subList(0, POPULATIONSIZE));
            nextPopulation.clear();


            //проверка и вывод лучшего
            if (compareWithIdeal(population.get(0)) == 0)
                flag = true;

            System.out.println("population " + numberOfPopulation + ":\n" + "best word: " + population.get(0) +
                    "\ndistance: " + compareWithIdeal(population.get(0)));


            //скрещивание
            for (int i = 0; i < population.size(); i++) {
                for (int j = i + 1; j < population.size(); j++) {

                    String word = new String();
                    for (int i1 = 0; i1 < lengthOfWord; i1++) {
                        if (random.nextInt(100) < 3)
                            word = word + (char) ('A' + random.nextInt(26));
                        else if (random.nextBoolean())
                            word = word + population.get(i).charAt(i1);
                        else word = word + population.get(j).charAt(i1);
                    }
                    nextPopulation.add(word);
                }
            }

            numberOfPopulation++;
        }

    }
}
