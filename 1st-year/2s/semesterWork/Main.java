import java.io.FileNotFoundException;

/**
 * Created by DNS on 02.03.2016.
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Polinom3 polinom = new Polinom3("input");
        System.out.println(polinom);

        polinom.insert(-6, 1, 0, 0);
        polinom.insert(3, 2, 0, 4);
        polinom.insert(15, 2, 4, 0);
        System.out.println(polinom);

        polinom.delete(2, 0, 4);
        polinom.delete(0, 2, 0);
        polinom.delete(2, 4, 0);
        System.out.println(polinom);

        Polinom3 polinom2 = new Polinom3("input2");
        System.out.println(polinom2);
        polinom.add(polinom2);
        System.out.println(polinom);

        polinom.derivate(1);
        System.out.println(polinom);

        System.out.println(polinom.value(1, 1, 1));

        for (int i = 0; i < 3; i++)
            System.out.print(polinom.minCoef()[i] + " ");
    }
}
