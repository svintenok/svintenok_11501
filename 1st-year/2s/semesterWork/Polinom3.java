import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by DNS on 02.03.2016.
 */
public class Polinom3 {
    private Elem head = new Elem();

    //Конструктор полинома из файла с упорядоченными элементами
    public Polinom3(String filename) throws FileNotFoundException {
        Scanner sc = new Scanner(new File(filename));
        head.setCoef(sc.nextInt());
        head.setDegs(sc.nextInt(),sc.nextInt(),sc.nextInt());
        Elem p = head;
        while(sc.hasNextInt()) {
            Elem elem = new Elem(sc.nextInt(),sc.nextInt(),sc.nextInt(),sc.nextInt(), null);
            p.setNext(elem);
            p = p.getNext();
        }
    }

    public Elem getHead() {
        return head;
    }

    @Override
    //Вывод полинома строкой
    public String toString() {
        String string = new String();
        for(Elem p = head; p != null; p = p.getNext() ) {
            if(p.getCoef() > 0)
                string = string + "+";
            string = string + p.getCoef();
            if (p.getDegs()[0] != 0)
                string = string + "x^" + p.getDegs()[0];
            if (p.getDegs()[1] != 0)
                string = string + "y^" + p.getDegs()[1];
            if (p.getDegs()[2] != 0)
                string = string + "z^" + p.getDegs()[2];
        }
        return string;
    }

    //Проверка равенства мономов
    private boolean equalityOfDeg(Elem elem, int deg1, int deg2, int deg3) {
        if (elem.getDegs()[0] == deg1 && elem.getDegs()[1] == deg2 && elem.getDegs()[2] == deg3)
            return true;
        return  false;
    }

    //Вставка монома
    public  void insert(int coef, int deg1, int deg2, int deg3) {

        if (head.getDegs()[0] < deg1 ||(head.getDegs()[0] == deg1 && (head.getDegs()[1] < deg2 ||(head.getDegs()[1] == deg1 && head.getDegs()[2] < deg3))))
            head = new Elem(coef, deg1, deg2, deg3, head);
        else {
            Elem p = head;
            while (p.getDegs()[0] > deg1 && p.getNext() != null)
                p = p.getNext();
            while (p.getDegs()[1] > deg2 && p.getNext() != null && p.getNext().getDegs()[0] == p.getDegs()[0])
                p = p.getNext();
            while (p.getDegs()[2] > deg3 && p.getNext() != null && p.getNext().getDegs()[1] == p.getDegs()[1])
                p = p.getNext();

            if (equalityOfDeg(p, deg1, deg2, deg3)) {
                p.setCoef(p.getCoef() + coef);
                if (p.getCoef() == 0)
                    delete(p.getDegs()[0], p.getDegs()[1], p.getDegs()[2]);
            }
            else
                p.setNext(new Elem(coef, deg1, deg2, deg3, p.getNext()));

        }
    }

    //Удаление монома
    public void delete (int deg1, int deg2, int deg3){
        if (equalityOfDeg(head, deg1, deg2, deg3))
            head = head.getNext();
        else {
            boolean flag = false;
            Elem p;
            for(p = head; !flag && p.getNext() != null; p = p.getNext() ) {
                if (equalityOfDeg(p.getNext(), deg1, deg2, deg3)) {
                    flag = true;
                    p.setNext(p.getNext().getNext());
                }
            }
        }
    }

    //Сумма полиномов
    public void add(Polinom3 polinom3) {
        for(Elem p = polinom3.getHead(); p != null; p = p.getNext() ) {
            insert(p.getCoef(),p.getDegs()[0],p.getDegs()[1],p.getDegs()[2]);
        }
    }

    //Взятие частной производной по i-ой переменной
    public  void  derivate(int i) {
        for(Elem p = head; p != null; p = p.getNext() ) {
            if (p.getDegs()[i - 1] != 0) {
                p.setCoef(p.getCoef() * p.getDegs()[i - 1]);
                p.setDeg(p.getDegs()[i - 1] - 1, i - 1);
            }
            else delete(p.getDegs()[0], p.getDegs()[1], p.getDegs()[2]);
        }
    }

    //Вычисление значения полинома в точке
    public int value(int x, int y, int z) {
        int value = 0;
        for(Elem p = head; p != null; p = p.getNext() ) {
            value = (int) (value + p.getCoef() * Math.pow(x, p.getDegs()[0]) * Math.pow(y, p.getDegs()[1]) * Math.pow(z, p.getDegs()[2]));
        }
        return value;
    }


    //Нахождение монома с минимальным коэффициэнтом
    public int[] minCoef() {
        Elem pMin = head;
        for(Elem p = head.getNext(); p != null; p = p.getNext() ) {
            if (p.getCoef() < pMin.getCoef())
                pMin = p;
        }
        return pMin.getDegs();
    }
}
