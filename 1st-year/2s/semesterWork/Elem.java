/**
 * Created by DNS on 02.03.2016.
 */
public class Elem {
    private int coef;
    private int[] deg = new int[3];
    private Elem next;


    public Elem(int coef, int deg1, int deg2, int deg3, Elem next) {
        this.coef = coef;
        this.deg[0] = deg1;
        this.deg[1] = deg2;
        this.deg[2] = deg3;
        this.next = next;
    }

    public Elem() {}

    public int getCoef() {
        return coef;
    }
    public void setCoef(int coef) {
        this.coef = coef;
    }

    public Elem getNext() {
        return next;
    }
    public void setNext(Elem next) {
        this.next = next;
    }

    public int[] getDegs() {
        return deg;
    }
    public void setDeg(int deg, int i) {
        this.deg[i] = deg;
    }

    public void setDegs(int deg1, int deg2, int deg3) {
        this.deg[0] = deg1;
        this.deg[1] = deg2;
        this.deg[2] = deg3;
    }
}
