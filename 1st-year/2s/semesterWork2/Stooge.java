import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by DNS on 06.04.2016.
 */
public class Stooge {
    static long count = 0;

    public static void stoogeSort(ArrayList<Integer> list, int i, int j) {
        count++;
        if (list.get(j) < list.get(i)) {
            int b = list.get(i);
            list.set(i, list.get(j));
            list.set(j, b);
        }
        if (j - i > 1) {
            int t = (j - i + 1)/3;
            stoogeSort(list, i, j - t);
            stoogeSort(list, i + t, j);
            stoogeSort(list, i, j - t);
        }
    }

    public static void stoogeSort(ArrayList<Integer> list) {
        stoogeSort(list, 0, list.size() - 1);
    }

    public static void main(String[] args) throws IOException {
        File file = new File("results.txt");
        file.createNewFile();
        FileWriter writer = new FileWriter(file.getAbsoluteFile());
        ArrayList<Integer> list = new ArrayList<>();
        int n = 50;

        for(int i = 1; i <= n; i ++) {

            Scanner scanner = new Scanner(new File("file" + i + ".txt"));
            while (scanner.hasNextInt())
                list.add(scanner.nextInt());

            long timeOne = System.currentTimeMillis();
            stoogeSort(list);
            long time = System.currentTimeMillis() - timeOne;

            writer.write(i + ": " + time + ", " + count + "\n");
            count = 0;
            list.clear();
        }

        writer.flush();
    }
}
