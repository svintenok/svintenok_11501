import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * Created by DNS on 16.04.2016.
 */
public class Generation {
    public static void main(String[] args) throws IOException {
        final int n = 50;
        Random random = new Random();
        for (int i = 1; i <= n; i++) {
            File file = new File("file" + i + ".txt");
            file.createNewFile();
            FileWriter writer = new FileWriter(file.getAbsoluteFile());
            int n2 = i * 100;

            for (int j = 0; j < n2 ; j++) {
                writer.write(new String(random.nextInt(300) + " "));
            }
            writer.flush();
        }
    }
}
