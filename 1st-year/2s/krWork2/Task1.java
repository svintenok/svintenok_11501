import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by DNS on 20.04.2016.
 */
public class Task1 {
    public static void main(String[] args) {

        LinkedList<Integer> list = new LinkedList<>();
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++)
            list.add(scanner.nextInt());

        int max = list.get(0);
        int number = 1;
        int pointer = 0;

        for (int i = 1; i < n; i++) {
            if (list.get(i) == max)
                number++;
            else if (list.get(i) > max) {
                max = list.get(i);
                number = 1;
                pointer = i;
            }
        }

        if (number == 1)
            System.out.println(max + ", адрес: " + pointer);
        else
            System.out.println(max + ", количество: " + number);
    }
}
