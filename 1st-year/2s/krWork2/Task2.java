import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by DNS on 20.04.2016.
 */
public class Task2 {
    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<>();
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < 2* n; i++)
            list.add(scanner.nextInt());

        System.out.println(list);

        for (int i = 0; i < 3* n; i = i + 3) {
            if (list.get(i) > list.get(i + 1))
                list.add(i + 2, list.get(i));
            else
                list.add(i + 2,list.get(i + 1));

        }

        System.out.println(list);
    }
}
