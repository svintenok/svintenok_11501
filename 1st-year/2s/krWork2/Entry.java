/**
 * Created by DNS on 20.04.2016.
 */
public class Entry implements Comparable<Entry> {
    private String name;
    private int n;

    public Entry(String name, int n) {
        this.name = name;
        this.n = n;
    }

    public String getName() {
        return name;
    }

    public int getN() {
        return n;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setN(int n) {
        this.n = n;
    }

    @Override
    public int compareTo(Entry o) {
            return name.compareTo(o.getName());
    }

    @Override
    public String toString() {
        return  name + " " + n;
    }
}
