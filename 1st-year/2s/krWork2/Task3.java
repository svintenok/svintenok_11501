import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.concurrent.Callable;


/**
 * Created by DNS on 20.04.2016.
 */
public class Task3 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("input.txt"));
        TreeMap<String,Integer> map = new TreeMap<>();


        ArrayList<Entry> list = new ArrayList<>();

        while (scanner.hasNext())
            list.add(new Entry(scanner.next(), scanner.nextInt()));

        System.out.println(list);

        int k = 0;

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getN() % 2 == 0)
                k++;
        }

        if (k == 0)
            System.out.println("Букетов с четным количеством роз нет");
        else if (k % 2 == 0)
            System.out.println("Можно исправить ошибку, не меняя количество роз");
        else
            System.out.println("Нельзя исправить ошибку, не меняя общее количество роз");

        Collections.sort(list);
        System.out.println("Cортировка по именам:");
        System.out.println(list);

        Collections.sort(list, new Comparator<Entry>() {
            @Override
            public int compare(Entry o1, Entry o2) {
                if (o1.getN() < o2.getN())
                    return -1;
                return 1;
            }
        });

        System.out.println("Cортировка по числу роз:");
        System.out.println(list);

    }
}
