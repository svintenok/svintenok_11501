/**
 * Created by DNS on 03.03.2016.
 */
import java.util.Collection;
import java.util.Iterator;

public class LinkedCollection<T> implements Collection<T>{

    protected Elem<T> head = null;
    protected int size = 0;

    @Override
    public boolean remove(Object x) {

        if (head.getValue() == x) {
            head = head.getNext();
            size = size - 1;
            return true;
        }

        Elem p;
        for (p = head; p.getNext() != null; p = p.getNext()) {
            if (p.getNext().getValue() == x) {
                p.setNext(p.getNext().getNext());
                size = size - 1;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean contains(Object x) {

        for (Elem p = head; p != null; p = p.getNext())
            if (p.getValue() == x)
                return true;

        return false;
    }

    @Override
    public boolean add(T x) {
        if (contains(x))
            return false;
        head = new Elem<>(x, head);
        size = size + 1;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        boolean flag = true;
        Object[] a =  c.toArray();
        for (int i = 0; i < a.length; i++)
            if (!contains(a[i]))
                return false;
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        T[] a =  (T[]) c.toArray();
        for (int i = 0; i < a.length; i++)
            add(a[i]);
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Object[] a =  c.toArray();
        for (int i = 0; i < a.length; i++)
            remove(a[i]);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for (Elem<T> p = head; p != null; p = p.getNext()){
            if (!c.contains(p.getValue()))
                remove(p.getValue());
        }
        return true;
    }

    @Override
    public void clear() {
        head = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0)
            return true;
        return false;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[size];
        int i = 0;
        for (Elem p = head; p != null; p = p.getNext()) {
            array[i] = p.getValue();
            i = i + 1;
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        int i = 0;

        for (Elem p = head; p != null; p = p.getNext()) {
            a[i] =(T)p.getValue();
            i = i + 1;
        }
        return a;
    }

    public String toString(){
        String a = new String();

        for (Elem p = head; p != null; p = p.getNext()) {
            a = a + p.getValue() + " ";
        }
        return a;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }
}
