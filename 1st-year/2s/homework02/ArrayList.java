/**
 * Created by DNS on 03.03.2016.
 */
import java.util.List;
import java.util.ListIterator;

public class ArrayList<T> extends ArrayCollection<T> implements List<T> {


    @Override
    public T get(int i) {
        return (T)array[i];
    }

    @Override
    public T set(int i, T x) {
        T xOld  = (T) array[i];
        array[i] = x;
        return xOld;
    }

    @Override
    public void add(int i, T x) {
        size = size + 1;

        T x_old = (T) array[i];
        array[i] = x;

        for (int j = i + 1; j < size; j++) {
            x = (T) array[j];
            array[j] = x_old;
            x_old = x;
        }
    }

    @Override
    public boolean addAll(int i, java.util.Collection<? extends T> c) {
        T[] a =  (T[]) c.toArray();
        for (int j = 0; j < c.size(); j++)
            add(i + j, a[j]);
        return true;
    }

    @Override
    public T remove(int i) {
        T xOld = (T)array[i];
        size = size - 1;
        for (int j = i; j < size; i++) {
            array[j] = array[j + 1];
        }
        return xOld;
    }

    @Override
    public int indexOf(Object x) {
        for (int i = 0; i < size; i++) {
            if(array[i] == x)
                return i;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object x) {
        for (int i = size - 1; i >= 0; i--) {
            if(array[i] == x)
                return i;
        }
        return -1;
    }

    @Override
    public ArrayList<T> subList(int fromIndex, int toIndex) {
        ArrayList<T> list = new ArrayList<>();
        int i = 0;
        while (fromIndex + i <= toIndex) {
            list.add(i, (T)array[fromIndex + i]);
            i += 1;
        }
        return list;
    }

    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }
}
