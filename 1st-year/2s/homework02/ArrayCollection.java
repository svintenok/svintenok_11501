/**
 * Created by DNS on 03.03.2016.
 */
import java.util.Collection;
import java.util.Iterator;

public class ArrayCollection<T> implements Collection<T> {

    protected int size = 0;
    protected int CAPACITY = 10000;
    protected Object[] array = new Object[CAPACITY];


    @Override
    public boolean remove(Object x) {

        for(int i = 0; i < size; i++) {
            if (array[i] == x) {
                array[i] = array[size - 1];
                size = size - 1;
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean contains(Object x) {

        for (int i = 0; i < size; i++) {
            if (array[i] == x)
                return true;
        }
        return false;
    }

    @Override
    public boolean add(T x) {
        if (contains(x))
            return false;
        array[size] = x;
        size = size + 1;
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {

        Object[] a =  c.toArray();
        for (int i = 0; i < a.length; i++)
            if (!contains(a[i]))
                return false;
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        T[] a =  (T[]) c.toArray();
        for (int i = 0; i < c.size(); i++)
                add(a[i]);
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        Object[] a =  c.toArray();
        for (int i = 0; i < c.size(); i++)
            remove(a[i]);
        return true;
    }

    @Override
    public boolean retainAll(Collection<?> c) {

        for (int i = 0; i < size; i++)
            if (!c.contains(array[i])) {
                remove(array[i]);
                i = i - 1;
            }
        return true;
    }

    @Override
    public void clear() {
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        if (size == 0)
            return true;
        else
            return false;
    }

    @Override
    public Object[] toArray() {
        Object[] a = new Object[size];
        for(int i = 0; i < size; i++)
            a[i] = array[i];
        return a;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        for (int i = 0; i < size; i++)
            a[i] =(T)array[i];
        return a;
    }

    @Override
    public String toString() {
        String a = new String();
        for (int i = 0; i < size; i++)
            a = a + array[i] + " ";
        return a;
    }

    @Override
    public Iterator<T> iterator() {
        return null;
    }
}