/**
 * Created by DNS on 02.03.2016.
 */
public class RationalFraction implements Addable<RationalFraction>{
    private int x, y;

    public RationalFraction(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public RationalFraction() {
        this (0, 0);
    }

    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }

    public String toString() {
        return x + "/" + y;
    }

    public void reduce() {
        int i = 2;
        while(i <= this.x && i <= this.y ) {
            if ((this.x % i == 0) && (this.y % i == 0)) {
                this.x = this.x / i;
                this.y = this.y / i;
            }
            else
                i++;
        }
        if (y < 0) {
            this.x = -this.x;
            this.y = -this.y;
        }
    }

    public RationalFraction add(RationalFraction rf) {
        RationalFraction rf2 = new RationalFraction(this.x * rf.getY() + rf.getX() * this.y, this.y * rf.getY() );
        rf2.reduce();
        return rf2;
    }
}
