/**
 * Created by DNS on 02.03.2016.
 */
public class Vector2D<T extends Addable> {
    private T x, y;

    public Vector2D(T x, T y) {
        this.x = x;
        this.y = y;
    }

    public void addVector(Vector2D<T> vector2D) {
        this.x = (T) x.add(vector2D.getX());
        this.y = (T) y.add(vector2D.getY());
    }

    public T getX() {
        return x;
    }
    public T getY() {
        return y;
    }

    @Override
    public String toString() {
        return "(" + x.toString() + ", " + y.toString() + ")" ;
    }
}
