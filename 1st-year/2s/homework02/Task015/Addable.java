/**
 * Created by DNS on 02.03.2016.
 */
public interface Addable<T> {
    T add(T element);
}
