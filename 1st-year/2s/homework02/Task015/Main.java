/**
 * Created by DNS on 02.03.2016.
 */
public class Main {
    public static void main(String[] args) {
        Vector2D<RationalFraction> vector2D = new Vector2D<>(new RationalFraction(2, 3), new RationalFraction(1, 2));
        Vector2D<RationalFraction> vector2D1 = new Vector2D<>(new RationalFraction(2, 5), new RationalFraction(1, 3));
        vector2D.addVector(vector2D1);
        System.out.println(vector2D);

        Vector2D<ComplexNumber> vector2D2 = new Vector2D<>(new ComplexNumber(1, 1), new ComplexNumber(2, -1));
        Vector2D<ComplexNumber> vector2D3 = new Vector2D<>(new ComplexNumber(2, 1), new ComplexNumber(-1, 1));
        vector2D2.addVector(vector2D3);
        System.out.println(vector2D2);
    }

}
