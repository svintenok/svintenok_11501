/**
 * Created by DNS on 02.03.2016.
 */
public class ComplexNumber implements Addable<ComplexNumber> {
    private double x, y;

    public ComplexNumber(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public ComplexNumber() {
        this (0, 0);
    }

    public double getX() {
        return x;
    }
    public double getY() {
        return y;
    }

    public String toString() {
        if (y >= 0) {
            return x + "+" + y + "*i";
        }
        else {
            return x + "" + y + "*i";
        }
    }

    public ComplexNumber add(ComplexNumber cn) {
        ComplexNumber cn2 = new ComplexNumber(this.x + cn.getX(), this.y + cn.getY() );
        return cn2;
    }

}
