/**
 * Created by DNS on 03.03.2016.
 */
public class Elem <T> {
    private T value;
    private Elem next;


    public Elem(T value, Elem next) {
        this.value = value;
        this.next = next;
    }

    public Elem() {}

    public Elem(T value) {
        this.value = value;
    }

    public void setValue(T value) {
        this.value = value;
    }
    public void setNext(Elem next) {
        this.next = next;
    }

    public T getValue() {
        return value;
    }
    public Elem getNext() {
        return next;
    }
}
