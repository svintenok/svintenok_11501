/**
 * Created by DNS on 03.03.2016.
 */
import java.util.List;
import java.util.ListIterator;

public class LinkedList<T> extends LinkedCollection<T> implements List<T> {

    public Elem<T> getElem(int i) {
        Elem<T> p = head;
        for (int j = 1; j <= i; j++)
            p = p.getNext();
        return p;
    }

    @Override
    public T get(int i) {
        return getElem(i).getValue();
    }

    @Override
    public T set(int i, T x) {
        T oldX = getElem(i).getValue();
        getElem(i).setValue(x);
        return oldX;
    }

    @Override
    public void add(int i, T x) {
        if (i == 0)
            head = new Elem<>(x, head);
        else {
            Elem<T> p = getElem(i - 1);
            Elem<T> elem = new Elem<>(x, p.getNext());
            p.setNext(elem);
        }
        size += 1;
    }

    @Override
    public boolean addAll(int i, java.util.Collection<? extends T> c) {
        T[] a =  (T[]) c.toArray();
        for (int j = 0; j < c.size(); j++)
            add(i + j, a[j]);
        return true;
    }

    @Override
    public T remove(int i) {
        T x;
        if(i == 0) {
            x = head.getValue();
            head = head.getNext();
        }
        else {
            Elem<T> p = getElem(i - 1);
            x = (T) p.getNext().getValue();
            p.setNext(p.getNext().getNext());
        }
        size -= size;
        return x;
    }

    @Override
    public int indexOf(Object x) {
        int i = 0;

        for(Elem p = head; p != null; p = p.getNext()) {
            if (p.getValue() == x)
                return i;
            i = i + 1;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object x) {
        int i = 0;
        int lastIndex = -1;

        for(Elem p = head; p != null; p = p.getNext()) {
            if (p.getValue() == x)
                lastIndex = i;
            i = i + 1;
        }
        return lastIndex;
    }

    @Override
    public LinkedList<T> subList(int fromIndex, int toIndex) {
        LinkedList<T> list = new LinkedList<>();
        int i = 0;
        Elem<T> elem = getElem(fromIndex);
        while (fromIndex + i <= toIndex) {
            list.add(i, elem.getValue());
            i += 1;
            elem = elem.getNext();
        }
        return list;
    }


    @Override
    public ListIterator<T> listIterator() {
        return null;
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
    }
}
