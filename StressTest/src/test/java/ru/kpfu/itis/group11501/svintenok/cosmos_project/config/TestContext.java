package ru.kpfu.itis.group11501.svintenok.cosmos_project.config;

import org.mockito.Mockito;

import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.*;

/**
 * Author: Svintenok Kate
 * Date: 28.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Configuration
@EnableWebMvc
@EnableWebSecurity
public class TestContext {

    private static final String MESSAGE_SOURCE_BASE_NAME = "i18n/messages";

    @Bean
    public MessageSource messageSource() {
        ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();

        messageSource.setBasename(MESSAGE_SOURCE_BASE_NAME);
        messageSource.setUseCodeAsDefaultMessage(true);

        return messageSource;
    }

    @Bean
    public NewsService newsService() {
        return Mockito.mock(NewsService.class);
    }

    @Bean
    public BookingService bookingService() {
        return Mockito.mock(BookingService.class);
    }

    @Bean
    public UserService userService() {
        return Mockito.mock(UserService.class);
    }

    @Bean
    public TourService tourService() {
        return Mockito.mock(TourService.class);
    }

    @Bean
    public DepartureDateService departureDateService() {
        return Mockito.mock(DepartureDateService.class);
    }

    @Bean
    public RecallService recallService() {
        return Mockito.mock(RecallService.class);
    }

    @Bean
    public ForumService forumService() {
        return Mockito.mock(ForumService.class);
    }



}