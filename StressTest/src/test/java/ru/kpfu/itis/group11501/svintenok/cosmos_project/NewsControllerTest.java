package ru.kpfu.itis.group11501.svintenok.cosmos_project;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.config.SecurityConfig;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.config.TestContext;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.config.WebConfig;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Comment;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.News;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.NewsService;
import org.springframework.web.context.WebApplicationContext;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.UserService;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;
import static org.hamcrest.core.IsCollectionContaining.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Author: Svintenok Kate
 * Date: 28.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(classes = {TestContext.class, WebConfig.class, SecurityConfig.class})

public class NewsControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private NewsService newsServiceMock;

    @Autowired
    private UserService userServiceMock;

    @Autowired
    private WebApplicationContext webApplicationContext;


    @Before
    public void setUp(){
        Mockito.reset(newsServiceMock);
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    // =========================================== Get News List ==========================================
    @Test
    public void newsListPage_ShouldAddNewsListToModelAndRenderNewsListView() throws Exception {

        when(newsServiceMock.getNewsList()).thenReturn(Arrays.asList(this.createNews()));

        mockMvc.perform(get("/news"))
                .andExpect(status().isOk())
                .andExpect(view().name("news_list"))
                .andExpect(model().attribute("newsList", hasSize(1)))
                .andExpect(model().attribute("newsList", hasItem(
                        allOf(
                                hasProperty("id", is(1L)),
                                hasProperty("description", is("description")),
                                hasProperty("title", is("title")),
                                hasProperty("text", is("text"))
                        )
                )));
        verify(newsServiceMock, times(1)).getNewsList();
        verifyNoMoreInteractions(newsServiceMock);
    }

    // =========================================== Get One News ==========================================
    @Test
    public void newsPage_NewsNotFound_ShouldRender404View() throws Exception {

        when(newsServiceMock.getNews(1L)).thenReturn(null);

        mockMvc.perform(get("/news/{id}", 1L))
                .andExpect(view().name("handle404"));

        verify(newsServiceMock, times(1)).getNews(1L);
        verifyZeroInteractions(newsServiceMock);
    }

    @Test
    public void newsPage_NewsFound_ShouldAddNewsToModelAndRenderViewNewsView() throws Exception {

        when(newsServiceMock.getNews(1L)).thenReturn(this.createNews());

        mockMvc.perform(get("/news/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(view().name("news"))
                .andExpect(model().attribute("news", hasProperty("id", is(1L))))
                .andExpect(model().attribute("news", hasProperty("description", is("description"))))
                .andExpect(model().attribute("news", hasProperty("title", is("title"))))
                .andExpect(model().attribute("news", hasProperty("text", is("text"))));

        verify(newsServiceMock, times(1)).getNews(1L);
        verifyZeroInteractions(newsServiceMock);
    }

    @Test
    public void newsPage_NewsFound_ShouldAddCommentListToModelAndRenderViewNewsView() throws Exception {

        News news = this.createNews();
        news.setCommentList(Arrays.asList(this.createComment()));
        when(newsServiceMock.getNews(1L)).thenReturn(news);

        mockMvc.perform(get("/news/{id}", 1L))
                .andExpect(status().isOk())
                .andExpect(view().name("news"))
                .andExpect(model().attribute("news", hasProperty("commentList", hasSize(1))))
                .andExpect(model().attribute("news", hasProperty("commentList",
                        hasItem(allOf(
                                hasProperty("id", is(1L)),
                                hasProperty("text", is("text"))
                        )))));

        verify(newsServiceMock, times(1)).getNews(1L);
        verifyZeroInteractions(newsServiceMock);
    }
    // =========================================== Create News ==========================================

    @Test
    @WithMockUser(roles = {"admin"})
    public void createNews_ShouldRenderNewsCreatingView() throws Exception {
        mockMvc.perform(
                get("/news/create")
        )
                .andExpect(status().isOk())
                .andExpect(view().name("news_creating"));

    }

    // =========================================== Add Comment ==========================================

    @Test
    @WithMockUser(username = "username")
    public void addComment_EmptyText_ShouldNotAddCommentAndRenderNewsView() throws Exception {

        mockMvc.perform(post("/news/{id}/add_comment", 1L)
                .param("text", "")
        )
                .andExpect(view().name("redirect:/news/1"))
                .andExpect(redirectedUrl("/news/1"));

        verifyNoMoreInteractions(newsServiceMock);
        verifyNoMoreInteractions(userServiceMock);
    }


    @Test
    @WithMockUser(username = "username")
    public void addComment_ShouldAddCommentAndRenderNewsView() throws Exception {

        doNothing().when(newsServiceMock).addComment(isA(Comment.class));
        when(userServiceMock.getUser("username")).thenReturn(this.createUser());
        when(newsServiceMock.getNews(1L)).thenReturn(this.createNews());

        mockMvc.perform(post("/news/{id}/add_comment", 1L)
                .param("text", "text")
        )
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/news/1"))
                .andExpect(redirectedUrl("/news/1"));

        verify(newsServiceMock, times(1)).getNews(1l);
        verify(newsServiceMock, times(1)).addComment(isA(Comment.class));
        verify(userServiceMock, times(1)).getUser("username");
        verifyNoMoreInteractions(newsServiceMock);
        verifyNoMoreInteractions(userServiceMock);
    }

    // =========================================== Delete Comment =======================================


    @Test
    @WithMockUser(username = "username")
    public void removeComment_ShouldRemoveCommentAndRenderNewsView() throws Exception {

        Comment comment = this.createComment();
        doNothing().when(newsServiceMock).deleteComment(isA(Comment.class));
        when(newsServiceMock.getComment(1L)).thenReturn(comment);

        mockMvc.perform(post("/news/{id}/remove_comment", 1L)
                .param("comment_id", "1")
        )
                .andExpect(status().isFound())
                .andExpect(view().name("redirect:/news/1"))
                .andExpect(redirectedUrl("/news/1"));

        verify(newsServiceMock, times(1)).getComment(1L);
        verify(newsServiceMock, times(1)).deleteComment(comment);
        verifyNoMoreInteractions(newsServiceMock);
    }

    // ===================================================================================================

    private News createNews(){
        News news = new News();
        news.setId(1L);
        news.setTitle("title");
        news.setText("text");
        news.setDescription("description");
        news.setDate(new Date());
        news.setCommentList(new ArrayList<>());
        return news;
    }

    private Comment createComment(){
        Comment comment = new Comment();
        comment.setId(1L);
        comment.setText("text");
        comment.setUser(this.createUser());
        comment.setNews(this.createNews());
        comment.setDate(new Date());
        return comment;
    }

    private User createUser(){
        User user = new User();
        user.setLogin("username");
        user.setId(1L);
        return user;
    }
}