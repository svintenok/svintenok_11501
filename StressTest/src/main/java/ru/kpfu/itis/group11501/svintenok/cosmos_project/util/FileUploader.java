package ru.kpfu.itis.group11501.svintenok.cosmos_project.util;

import com.cloudinary.Cloudinary;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Author: Svintenok Kate
 * Date: 28.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public class FileUploader {

    public static String uploadFile(MultipartFile file, String filename) {
        System.out.println(111);
        if (!file.isEmpty()) {
            try {
                System.out.println(111);
                FileInputStream fis;
                Properties property = new Properties();
                fis = new FileInputStream("D:\\IdeaProjects\\StressTest\\src\\main\\resources\\cloudinary.properties");
                property.load(fis);
                String cloudName = property.getProperty("cloud_name");
                String apiKey = property.getProperty("api_key");
                String apiSecret = property.getProperty("api_secret");
                Map<String, String> config = new HashMap<String, String>();
                config.put("cloud_name", cloudName);
                config.put("api_key", apiKey);
                config.put("api_secret", apiSecret);
                Cloudinary cloudinary = new Cloudinary(config);
                Map options = new HashMap();
                options.put("public_id", filename);
                Map result = cloudinary.uploader().upload(file.getBytes(), options);
                System.out.println(111);
                return (String) result.get("url");
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println(44444444);
                return "";
            }
        }
        return "";
    }
}
