package ru.kpfu.itis.group11501.svintenok.cosmos_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.ForumTopic;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.TopicMessage;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.ForumService;

import javax.servlet.http.HttpServletRequest;

/**
 * Author: Svintenok Kate
 * Date: 29.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Controller
@RequestMapping("/forum")
public class ForumController {

    private final ForumService forumService;

    @Autowired
    public ForumController(ForumService forumService) {
        this.forumService = forumService;
    }


    @RequestMapping(value = "", method = RequestMethod.GET)
    public String forumTopicsPage(Model model) {
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            model.addAttribute("current_user", currentUser);
        }
        model.addAttribute("users_forum_topics", forumService.getForumTopicsList(false));
        model.addAttribute("admin_forum_topics", forumService.getForumTopicsList(true));
        return "forum_topics";
    }

    @RequestMapping(value = "/addTopic", method = RequestMethod.POST)
    public String addForumTopic(HttpServletRequest request, @RequestParam(name = "name") String name) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ForumTopic topic = new ForumTopic();
        topic.setName(name);
        topic.setUser(currentUser);
        if (request.isUserInRole("ROLE_admin")) {
            topic.setTechnical(true);
        }
        topic = forumService.addForumTopic(topic);
        return "redirect:/forum/" + topic.getId();
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String topicPage(@PathVariable(name = "id") Long id, Model model,
                            @RequestParam(name = "page", required = false) Integer page) {
        if (page == null)
            page = 1;
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            model.addAttribute("current_user", currentUser);
        }
        ForumTopic forumTopic = forumService.getForumTopic(id);
        if (forumTopic == null)
            return "handle404";
        model.addAttribute("topic", forumService.getForumTopicWithInfo(id, page));
        model.addAttribute("page", page);
        return "forum";
    }

    @RequestMapping(value = "/{id}/sendMessage", method = RequestMethod.POST)
    public String sendMessage(@PathVariable(name = "id") Long id, @RequestParam(name = "text") String text, HttpServletRequest request) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        ForumTopic topic = forumService.getForumTopic(id);

        TopicMessage message = new TopicMessage();
        message.setText(text);
        message.setUser(currentUser);
        message.setForumTopic(topic);

        forumService.addTopicMessage(message);

        return "redirect:" + request.getHeader("Referer");
    }

}