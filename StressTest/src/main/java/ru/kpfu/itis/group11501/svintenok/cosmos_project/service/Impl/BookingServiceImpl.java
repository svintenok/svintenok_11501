package ru.kpfu.itis.group11501.svintenok.cosmos_project.service.Impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Booking;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.BookingRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.RecallRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.BookingService;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 25.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Service
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final RecallRepository recallRepository;

    public BookingServiceImpl(BookingRepository bookingRepository, RecallRepository recallRepository) {
        this.bookingRepository = bookingRepository;
        this.recallRepository = recallRepository;
    }

    @Override
    public Booking getBooking(Long id) {
        return bookingRepository.findOne(id);
    }

    @Override
    public void removeBooking(Booking booking) {
        bookingRepository.delete(booking);
    }

    @Override
    public void addBooking(Booking booking) {
        bookingRepository.save(booking);
    }

    @Override
    public Booking getBooking(User user, Tour tour) {
        return bookingRepository.findOneByTourAndUser(tour, user);
    }

    @Override
    public List<Booking> getTravelsListByUser(User user) {
        return bookingRepository.findAllByUserAndIsActive(user, false);
    }

    @Override
    public int getTravelsCountByUser(User user) {
        return this.getTravelsListByUser(user).size();
    }

    @Override
    public List<Booking> getBookingListByTour(Tour tour) {
        return bookingRepository.findAllActiveByTour(tour);
    }

    @Override
    public int getBookingCountByTour(Tour tour) {
        return this.getBookingListByTour(tour).size();
    }

    @Override
    public List<Booking> getBookingListByUser(User user) {
        List<Booking> bookings = bookingRepository.findAllByUserAndIsActive(user, true);
        for (Booking booking: bookings)
            booking.setRecall(recallRepository.
                    findOneByUserIdAndDepartureDateId(booking.getUser().getId(), booking.getDepartureDate().getId()));
        return bookings;
    }
}