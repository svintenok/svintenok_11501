package ru.kpfu.itis.group11501.svintenok.cosmos_project.service.Impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Recall;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.RecallRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.RecallService;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 25.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Service
public class RecallServiceImpl implements RecallService{

    private final RecallRepository recallRepository;

    public RecallServiceImpl(RecallRepository recallRepository) {
        this.recallRepository = recallRepository;
    }

    @Override
    public Recall getRecall(Long id) {
        return recallRepository.findOne(id);
    }

    @Override
    public void addRecall(Recall recall) {
        recallRepository.save(recall);
    }

    @Override
    public void removeRecall(Recall recall) {
        recallRepository.delete(recall);
    }

    @Override
    public List<Recall> getRecallListByTour(Tour tour) {
        return recallRepository.findAllByTour(tour);
    }
}