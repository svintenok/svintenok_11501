package ru.kpfu.itis.group11501.svintenok.cosmos_project.service;

import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.DepartureDate;

/**
 * Author: Svintenok Kate
 * Date: 27.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface DepartureDateService {
    DepartureDate getDepartureDate(Long id);
}
