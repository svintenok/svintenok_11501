package ru.kpfu.itis.group11501.svintenok.cosmos_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Booking;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.BookingService;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.DepartureDateService;

/**
 * Author: Svintenok Kate
 * Date: 27.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Controller
@RequestMapping("/bookings")
public class BookingController {

    private final BookingService bookingService;
    private final DepartureDateService departureDateService;

    @Autowired
    public BookingController(BookingService bookingService, DepartureDateService departureDateService) {
        this.bookingService = bookingService;
        this.departureDateService = departureDateService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String bookingsPage(Model model) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("current_user", currentUser);
        model.addAttribute("bookings", bookingService.getBookingListByUser(currentUser));
        return "bookings";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createBooking(@RequestParam(name = "departure_date_id") Long departureDateId) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        Booking booking = new Booking();
        booking.setUser(currentUser);
        booking.setDepartureDate(departureDateService.getDepartureDate(departureDateId));
        bookingService.addBooking(booking);
        return "redirect:/bookings";
    }

    @RequestMapping(value = "/cancel", method = RequestMethod.POST)
    public String cancelBooking(@RequestParam(name = "booking_id") Long bookingId) {
        bookingService.removeBooking(bookingService.getBooking(bookingId));
        return "redirect:/bookings";
    }
}