package ru.kpfu.itis.group11501.svintenok.cosmos_project.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Booking;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Role;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.BookingRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.RecallRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.RoleRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.UserRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.UserService;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
@Service
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;
    private final BookingRepository bookingRepository;
    private final RecallRepository recallRepository;
    private final  RoleRepository roleRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BookingRepository bookingRepository,
                           RecallRepository recallRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.bookingRepository = bookingRepository;
        this.recallRepository = recallRepository;
        this.roleRepository = roleRepository;
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findOne(id);
    }

    @Override
    public User getUser(String login) {
        return userRepository.findOneByLogin(login);
    }

    @Override
    public User saveUser(User user) {
        user.setRole(roleRepository.findOneByRole("user"));
        return userRepository.save(user);
    }

    @Override
    public User getUserWithInfo(Long id) {
        User user = this.getUser(id);
        user.setTravels(bookingRepository.findAllByUserAndIsActive(user, false));
        for (Booking booking: user.getTravels())
            booking.setRecall(recallRepository.
                    findOneByUserIdAndDepartureDateId(booking.getUser().getId(), booking.getDepartureDate().getId()));
        return user;
    }

}