package ru.kpfu.itis.group11501.svintenok.cosmos_project.service;

import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;

import javax.servlet.http.Part;
import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 24.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface TourService {

    Long addTour(Tour tour, String date);
    void updateTour(Tour tour);
    Tour getTour(Long id);
    Tour getTourWithInfo(Long id, User currentUser);
    List<Tour> getToursList(String sorting, boolean reverse, String search);
}
