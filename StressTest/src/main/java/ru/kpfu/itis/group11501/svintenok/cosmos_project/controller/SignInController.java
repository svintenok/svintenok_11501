package ru.kpfu.itis.group11501.svintenok.cosmos_project.controller;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Author: Svintenok Kate
 * Date: 22.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Controller
@RequestMapping("/sign_in")
public class SignInController {

    @RequestMapping(value = "")
    public String signInPage(Model model, @RequestParam(value = "error", required = false) Boolean error) {
        if (Boolean.TRUE.equals(error)) {
            model.addAttribute("error", error);
        }
        return "login";
    }
}