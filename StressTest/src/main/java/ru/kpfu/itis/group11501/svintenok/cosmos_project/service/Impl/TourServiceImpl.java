package ru.kpfu.itis.group11501.svintenok.cosmos_project.service.Impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.BookingRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.DepartureDateRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.RecallRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.TourRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.TourService;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 24.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Service
public class TourServiceImpl implements TourService {

    private final TourRepository tourRepository;
    private final DepartureDateRepository departureDateRepository;
    private final BookingRepository bookingRepository;
    private final RecallRepository recallRepository;

    public TourServiceImpl(TourRepository tourRepository, DepartureDateRepository departureDateRepository,
                           BookingRepository bookingRepository, RecallRepository recallRepository) {
        this.tourRepository = tourRepository;
        this.departureDateRepository = departureDateRepository;
        this.bookingRepository = bookingRepository;
        this.recallRepository = recallRepository;
    }

    @Override
    public Long addTour(Tour tour, String date) {
        return null;
    }

    @Override
    public void updateTour(Tour tour) {

    }

    @Override
    public Tour getTour(Long id) {
        return tourRepository.findOne(id);
    }

    @Override
    public Tour getTourWithInfo(Long id, User currentUser) {
        Tour tour = this.getTour(id);
        tour.setUserBooking(bookingRepository.findOneByTourAndUser(tour, currentUser));
        return this.setTourInfo(tour);
    }

    @Override
    public List<Tour> getToursList(String sorting, boolean reverse, String search) {
        List<Tour> tours = tourRepository.findAll();
        if (search != null)
            tours = tourRepository.kfindAllBySearch(search);
        for (Tour tour: tours)
            setTourInfo(tour);
        if (sorting != null) {
            if (sorting.equals("rating"))
                Collections.sort(tours, new Comparator<Tour>() {
                    @Override
                    public int compare(Tour tour1, Tour tour2) {

                        double rating1 = 0;
                        double rating2 = 0;

                        if (tour1.getRating() != null)
                            rating1 = tour1.getRating();
                        if (tour2.getRating() != null)
                            rating2 = tour2.getRating();

                        if (rating1 > rating2) {
                            return -1;
                        } else if (rating1 < rating2) {
                            return 1;
                        }
                        return 0;
                    }
                });
            else if (sorting.equals("date"))
                if (sorting.equals("rating"))
                    Collections.sort(tours, new Comparator<Tour>() {
                        @Override
                        public int compare(Tour tour1, Tour tour2) {
                            Date date1 = tour1.getDepartureDate().getDate();
                            Date date2 = tour2.getDepartureDate().getDate();

                            if (date1.after(date2)) {
                                return -1;
                            } else if (date1.before(date2)) {
                                return 1;
                            }
                            return 0;
                        }
                    });
        }
        if (reverse)
            Collections.reverse(tours);
        return tours;
    }


    private Tour setTourInfo(Tour tour) {
        tour.setDepartureDate(departureDateRepository.findOneByTourIdAndIsActive(tour.getId(), true));
        tour.setBookingCount(bookingRepository.findAllActiveByTour(tour).size());
        tour.setRating(recallRepository.getTourRate(tour));
        tour.setRecallList(recallRepository.findAllByTour(tour));
        tour.setRating(recallRepository.getTourRate(tour));
        return tour;
    }
}