package ru.kpfu.itis.group11501.svintenok.cosmos_project.service;

import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Role;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
public interface UserService {
    User getUser(Long id);
    User getUser(String login);
    User saveUser(User user);
    User getUserWithInfo(Long id);
}
