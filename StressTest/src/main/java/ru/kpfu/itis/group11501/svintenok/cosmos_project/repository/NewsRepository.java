package ru.kpfu.itis.group11501.svintenok.cosmos_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.News;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
public interface NewsRepository extends JpaRepository<News, Long> {
    List<News> findAllByOrderByDateDesc();
}