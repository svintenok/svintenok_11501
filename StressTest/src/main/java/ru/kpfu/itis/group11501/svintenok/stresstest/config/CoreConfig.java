package ru.kpfu.itis.group11501.svintenok.stresstest.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.kpfu.itis.group11501.svintenok.stresstest.service")
public class CoreConfig {
}
