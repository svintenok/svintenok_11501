package ru.kpfu.itis.group11501.svintenok.cosmos_project.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.aspect.AroundLoggerAspect;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.aspect.BeforeLoggerAspect;


/**
 * Author: Svintenok Kate
 * Date: 28.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Configuration
@EnableAspectJAutoProxy
public class AOPConfig {

    @Bean
    BeforeLoggerAspect getLogger(){
        return new BeforeLoggerAspect();
    }

    @Bean
    AroundLoggerAspect getAround(){
        return new AroundLoggerAspect();
    }
}