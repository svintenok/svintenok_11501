package ru.kpfu.itis.group11501.svintenok.cosmos_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.DepartureDate;


import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 23.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface DepartureDateRepository extends JpaRepository<DepartureDate, Long> {

    DepartureDate findOneByTourIdAndIsActive(Long tourId, boolean isActive);
}
