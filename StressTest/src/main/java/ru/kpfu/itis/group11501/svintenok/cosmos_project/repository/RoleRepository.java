package ru.kpfu.itis.group11501.svintenok.cosmos_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Role;

/**
 * Author: Svintenok Kate
 * Date: 23.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findOneByRole(String role);
}
