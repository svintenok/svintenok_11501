package ru.kpfu.itis.group11501.svintenok.cosmos_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.ForumTopic;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 29.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface ForumTopicRepository extends JpaRepository<ForumTopic, Long> {
    List<ForumTopic> findAllByIsTechnicalOrderByDateAsc(boolean isTechnical);
}
