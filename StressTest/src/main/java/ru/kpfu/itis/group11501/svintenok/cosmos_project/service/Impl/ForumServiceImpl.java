package ru.kpfu.itis.group11501.svintenok.cosmos_project.service.Impl;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.ForumTopic;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.TopicMessage;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.ForumTopicRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.TopicMessageRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.ForumService;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 29.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Service
public class ForumServiceImpl implements ForumService{
    private static final int PAGE_SIZE = 10;
    private final ForumTopicRepository forumTopicRepository;
    private final TopicMessageRepository topicMessageRepository;

    public ForumServiceImpl(ForumTopicRepository forumTopicRepository, TopicMessageRepository topicMessageRepository) {
        this.forumTopicRepository = forumTopicRepository;
        this.topicMessageRepository = topicMessageRepository;
    }

    @Override
    public void addTopicMessage(TopicMessage topicMessage) {
        topicMessageRepository.save(topicMessage);
    }

    @Override
    public void removeTopicMessage(TopicMessage topicMessage) {
        topicMessageRepository.delete(topicMessage);
    }

    @Override
    public TopicMessage getTopicMessage(Long id) {
        return topicMessageRepository.findOne(id);
    }

    @Override
    public void removeForumTopic(ForumTopic forumTopic) {
        forumTopicRepository.delete(forumTopic);
    }

    @Override
    public ForumTopic addForumTopic(ForumTopic forumTopic) {
        return forumTopicRepository.save(forumTopic);
    }

    @Override
    public List<ForumTopic> getForumTopicsList(boolean isTechnical) {
        List<ForumTopic> forumTopics = forumTopicRepository.findAllByIsTechnicalOrderByDateAsc(isTechnical);
        for (ForumTopic topic: forumTopics) {
            List<TopicMessage> topicMessages = topicMessageRepository.findAllByForumTopicIdOrderByDateAsc(topic.getId());
            topic.setMessagesCount(topicMessages.size());
            if (topicMessages.size() > 0)
                topic.setLastMessage(topicMessages.get(topicMessages.size() - 1));
            topic.setPagesCount((int) Math.ceil((double) topicMessages.size()/ PAGE_SIZE));
        }
        return forumTopics;
    }

    @Override
    public ForumTopic getForumTopic(Long id) {
        return forumTopicRepository.findOne(id);
    }

    @Override
    public ForumTopic getForumTopicWithInfo(Long id, int page) {
        PageRequest pageRequest = new PageRequest(page - 1, PAGE_SIZE, Sort.Direction.ASC, "date");
        ForumTopic forumTopic = this.getForumTopic(id);
        forumTopic.setMessages(topicMessageRepository.findAllByForumTopicId(forumTopic.getId(), pageRequest));

        List<TopicMessage> topicMessages = topicMessageRepository.findAllByForumTopicIdOrderByDateAsc(forumTopic.getId());
        forumTopic.setPagesCount((int) Math.ceil((double) topicMessages.size()/ PAGE_SIZE));
        return forumTopic;
    }
}