package ru.kpfu.itis.group11501.svintenok.cosmos_project.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Comment;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.News;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.CommentRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.NewsRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.NewsService;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
@Service
public class NewsServiceImpl implements NewsService {

    private final NewsRepository newsRepository;
    private final CommentRepository commentRepository;

    @Autowired
    public NewsServiceImpl(NewsRepository newsRepository, CommentRepository commentRepository) {
        this.newsRepository = newsRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public News getNews(Long id) {
        News news = newsRepository.findOne(id);
        news.setCommentList(this.getCommentList(news));
        return news;
    }

    @Override
    public List<News> getNewsList() {
        List<News> newsList = newsRepository.findAllByOrderByDateDesc();
        return newsList;
    }

    @Override
    public News addNews(News news) {
        return newsRepository.save(news);
    }

    @Override
    public void addComment(Comment comment) {
        commentRepository.save(comment);
    }

    @Override
    public void deleteComment(Comment comment) {
        commentRepository.delete(comment);
    }

    @Override
    public Comment getComment(Long id) {
        return commentRepository.findOne(id);
    }

    private List<Comment> getCommentList(News news){
        return commentRepository.findAllByNewsId(news.getId());
    }
}