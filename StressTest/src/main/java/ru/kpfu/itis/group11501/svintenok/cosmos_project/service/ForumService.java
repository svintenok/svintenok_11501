package ru.kpfu.itis.group11501.svintenok.cosmos_project.service;

import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.ForumTopic;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.TopicMessage;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 29.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface ForumService {
    void addTopicMessage(TopicMessage topicMessage);
    void removeTopicMessage(TopicMessage topicMessage);
    TopicMessage getTopicMessage(Long id);

    void removeForumTopic(ForumTopic forumTopic);
    ForumTopic addForumTopic(ForumTopic forumTopic);
    List<ForumTopic> getForumTopicsList(boolean isTechical);
    ForumTopic getForumTopic(Long id);
    ForumTopic getForumTopicWithInfo(Long id, int page);
}
