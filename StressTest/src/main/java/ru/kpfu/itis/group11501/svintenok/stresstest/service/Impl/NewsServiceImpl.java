package ru.kpfu.itis.group11501.svintenok.stresstest.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.svintenok.stresstest.model.Comment;
import ru.kpfu.itis.group11501.svintenok.stresstest.model.News;
import ru.kpfu.itis.group11501.svintenok.stresstest.repository.CommentRepository;
import ru.kpfu.itis.group11501.svintenok.stresstest.repository.NewsRepository;
import ru.kpfu.itis.group11501.svintenok.stresstest.service.NewsService;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
@Service
public class NewsServiceImpl implements NewsService {

    private final NewsRepository newsRepository;
    private final CommentRepository commentRepository;

    @Autowired
    public NewsServiceImpl(NewsRepository newsRepository, CommentRepository commentRepository) {
        this.newsRepository = newsRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public News getNewsById(Long id) {
        News news = newsRepository.findOne(id);
        news.setCommentList(this.getCommentList(news));
        return news;
    }

    @Override
    public List<News> getNewsList() {
        List<News> newsList = newsRepository.findAll();
        return newsRepository.findAll();
    }

    private List<Comment> getCommentList(News news){
        return commentRepository.findAllByNewsId(news.getId());
    }
}