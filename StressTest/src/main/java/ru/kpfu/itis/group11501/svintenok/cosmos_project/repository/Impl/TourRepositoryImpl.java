package ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.Impl;

import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.DepartureDate;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.DepartureDate_;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour_;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.TourRepositoryCustom;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.*;
import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 29.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Transactional
public class TourRepositoryImpl implements TourRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Tour> kfindAllBySearch(String search) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

        CriteriaQuery<Tour> query = criteriaBuilder.createQuery(Tour.class);
        Root<Tour> root = query.from(Tour.class);
        query.select(root);


        Subquery<DepartureDate> subquery = query.subquery(DepartureDate.class);
        Root<DepartureDate> subRoot = subquery.from(DepartureDate.class);
        subquery.select(subRoot);
        Predicate predicate = criteriaBuilder.equal(subRoot.get(DepartureDate_.tour), root);
        Predicate predicate1 = criteriaBuilder.equal(subRoot.get(DepartureDate_.isActive), true);
        predicate = criteriaBuilder.and(predicate, predicate1);
        subquery.where(predicate);

        predicate = criteriaBuilder.exists(subquery);


        if (!search.isEmpty()) {
            Predicate p2 = criteriaBuilder.like(criteriaBuilder.lower(root.get(Tour_.title)), "%" + search.toLowerCase() + "%");
            Predicate p3 = criteriaBuilder.like(criteriaBuilder.lower(root.get(Tour_.place)), "%" + search.toLowerCase() + "%");
            p2 = criteriaBuilder.or(p2, p3);
            predicate = criteriaBuilder.and(predicate, p2);
        }

        query.where(predicate);
        return entityManager.createQuery(query).getResultList();
    }


    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }
}