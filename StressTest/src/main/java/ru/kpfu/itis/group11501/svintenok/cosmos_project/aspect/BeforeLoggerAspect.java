package ru.kpfu.itis.group11501.svintenok.cosmos_project.aspect;

import org.apache.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import java.util.Arrays;


/**
 * Author: Svintenok Kate
 * Date: 28.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Aspect
public class BeforeLoggerAspect {
    @Before("execution(* ru.kpfu.itis.group11501.svintenok.cosmos_project.controller.*.*(..)) ||" +
            "execution(* ru.kpfu.itis.group11501.svintenok.cosmos_project.service.*.*(..)) ||" +
            "execution(* ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.*.*(..))" )
    public void logBefore(JoinPoint joinPoint) {

        final Class<?> targetClass = joinPoint.getTarget().getClass();
        final Logger logger = Logger.getLogger(targetClass);

        logger.info("Entering in Method :  " + joinPoint.getSignature().getName());
        logger.info("Class Name :  " + joinPoint.getSignature().getDeclaringTypeName());
        logger.info("Arguments :  " + Arrays.toString(joinPoint.getArgs()));
        logger.info("Target class : " + joinPoint.getTarget().getClass().getName());
    }
}