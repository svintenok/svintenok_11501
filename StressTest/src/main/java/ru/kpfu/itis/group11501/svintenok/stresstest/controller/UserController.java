package ru.kpfu.itis.group11501.svintenok.stresstest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.group11501.svintenok.stresstest.service.UserService;

/**
 * Author: Svintenok Kate
 * Date: 20.04.2017
 * Group: 11-501
 * Project: StressTest
 */
@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String userPage(@PathVariable(name = "id") Long id, Model model) {
        model.addAttribute("user", userService.getUser(id));
        return "profile";
    }
}