package ru.kpfu.itis.group11501.svintenok.cosmos_project.service.Impl;

import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.DepartureDate;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.repository.DepartureDateRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.DepartureDateService;

/**
 * Author: Svintenok Kate
 * Date: 27.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Service
public class DepartureDateServiceImpl implements DepartureDateService {

    private final DepartureDateRepository departureDateRepository;

    public DepartureDateServiceImpl(DepartureDateRepository departureDateRepository) {
        this.departureDateRepository = departureDateRepository;
    }

    @Override
    public DepartureDate getDepartureDate(Long id) {
        return departureDateRepository.findOne(id);
    }
}