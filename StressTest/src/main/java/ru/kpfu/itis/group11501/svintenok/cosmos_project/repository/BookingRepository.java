package ru.kpfu.itis.group11501.svintenok.cosmos_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Booking;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 23.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface BookingRepository extends JpaRepository<Booking, Long> {

    @Query("select b from Booking b where b.user=:user and " +
            "b.departureDate in  (select db from DepartureDate db where db.tour=:tour and db.isActive=true)")
    Booking findOneByTourAndUser(@Param("tour") Tour tour, @Param("user") User user);

    @Query("select b from Booking b where b.user=:user and " +
            "b.departureDate in (select db from DepartureDate db where db.isActive=:isActive)")
    List<Booking> findAllByUserAndIsActive(@Param("user") User user, @Param("isActive") boolean isActive);

    @Query("select b from Booking b where " +
            "b.departureDate in (select db from DepartureDate db where db.tour=:tour and db.isActive=true)")
    List<Booking> findAllActiveByTour(@Param("tour") Tour tour);
}
