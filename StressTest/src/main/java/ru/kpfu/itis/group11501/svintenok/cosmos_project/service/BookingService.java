package ru.kpfu.itis.group11501.svintenok.cosmos_project.service;


import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Booking;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 08.11.2016
 * Group: 11-501
 * Task: semester project
 */
public interface BookingService {

    Booking getBooking(Long id);

    void removeBooking(Booking booking);
    void addBooking(Booking booking);
    Booking getBooking(User user, Tour tour);

    List<Booking> getTravelsListByUser(User user);
    int getTravelsCountByUser(User user);

    List<Booking> getBookingListByTour(Tour tour);
    int getBookingCountByTour(Tour tour);

    List<Booking> getBookingListByUser(User user);
}
