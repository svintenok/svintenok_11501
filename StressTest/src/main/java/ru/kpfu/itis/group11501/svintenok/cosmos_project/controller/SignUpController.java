package ru.kpfu.itis.group11501.svintenok.cosmos_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.UserService;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.util.FileUploader;

import java.util.Map;
import java.util.regex.Pattern;

/**
 * Author: Svintenok Kate
 * Date: 22.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Controller
@RequestMapping("/sign_up")
public class SignUpController {

    private final UserService userService;
    private static final Pattern passwordRegexp = Pattern.compile("[a-zA-Z0-9]{6,30}");
    private static final Pattern loginRegexp = Pattern.compile("[\\w-]{1,30}");
    private static final Pattern emailRegexp = Pattern.compile("[\\w-]+@[a-z0-9-]+\\.[a-z]{2,6}");

    private static final PasswordEncoder encoder = new BCryptPasswordEncoder();

    @Autowired
    public SignUpController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "")
    public String signUprPage() {
        return "sign_up";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String signUp(Model model, @RequestParam Map<String, String> allRequestParams,
                         @RequestParam(value = "profile_photo", required = false) MultipartFile file) {

        String login = allRequestParams.get("login");
        String password = allRequestParams.get("password");
        String passwordConf = allRequestParams.get("password_conf");
        String email = allRequestParams.get("email");
        String name = allRequestParams.get("name");
        String country = allRequestParams.get("country");

        String error = null;

        if(userService.getUser(login) != null)
            error = "existing_login";
        else if(!loginRegexp.matcher(login).matches())
            error = "wrong_login";
        else if (!emailRegexp.matcher(email).matches())
            error = "wrong_email";
        else if(!passwordRegexp.matcher(password).matches())
            error = "wrong_password";
        else if (!password.equals(passwordConf))
            error = "wrong_conf_password";

        User user = new User();
        user.setLogin(login);
        user.setPassword(encoder.encode(password));
        user.setEmail(email);
        user.setName(name);
        user.setCountry(country);

        if(error == null) {
            user = userService.saveUser(user);

            if (file != null) {
                FileUploader.uploadFile(file, "user_images/" + user.getId());
                user.setPhoto(true);
                userService.saveUser(user);
            }

            return "login";
        }

        model.addAttribute("error", error);
        model.addAttribute("login", login);
        model.addAttribute("email", email);
        model.addAttribute("name", name);
        model.addAttribute("country", country);

        return "sign_up";
    }

}