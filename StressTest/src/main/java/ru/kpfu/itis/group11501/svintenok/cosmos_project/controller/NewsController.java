package ru.kpfu.itis.group11501.svintenok.cosmos_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Comment;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.News;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.NewsService;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.UserService;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.util.FileUploader;

import javax.enterprise.inject.New;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
@Controller
@RequestMapping("/news")
public class NewsController {
    private final NewsService newsService;
    private final UserService userService;

    @Autowired
    public NewsController(NewsService newsService, UserService userService) {
        this.newsService = newsService;
        this.userService = userService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String newsListPage(Model model) {
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            model.addAttribute("current_user", currentUser);
        }
        model.addAttribute("newsList", newsService.getNewsList());
        return "news_list";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String newsPage(@PathVariable(name = "id") Long id, Model model) {
        if (SecurityContextHolder.getContext().getAuthentication() != null) {
            User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            model.addAttribute("current_user", currentUser);
        }
        News news = newsService.getNews(id);
        if (news == null)
            return "handle404";
        model.addAttribute("news", news);
        return "news";
    }

    @RequestMapping(value = "/{id}/add_comment", method = RequestMethod.POST)
    public String addComment(@PathVariable(name = "id") Long id,
                             @RequestParam(name = "text") String text,  HttpServletRequest request) {
        UserDetails currentUser = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (!text.equals("")) {
            Comment comment = new Comment();
            comment.setText(text);
            comment.setUser(userService.getUser(currentUser.getUsername()));
            comment.setDate(new Date(System.currentTimeMillis()));
            comment.setNews(newsService.getNews(id));
            newsService.addComment(comment);
        }


        return "redirect:/news/" + id;
    }


    @RequestMapping(value = "/{id}/remove_comment", method = RequestMethod.POST)
    public String removeComment(@PathVariable(name = "id") Long id,
                                @RequestParam(name = "comment_id") Long commentId, HttpServletRequest request) {
        newsService.deleteComment(newsService.getComment(commentId));
        return "redirect:/news/" + id;
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createNews(@RequestParam Map<String, String> allRequestParams, @RequestParam("file") MultipartFile file,
                             MultipartHttpServletRequest request) {
        News news = new News();
        news.setText(allRequestParams.get("text"));
        news.setDescription(allRequestParams.get("description"));
        news.setTitle(allRequestParams.get("title"));
        news = newsService.addNews(news);

        FileUploader.uploadFile(file, "news_images/" + news.getId());
        return "redirect:/news";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String newsCreatingPage() {
        return "news_creating";
    }
}