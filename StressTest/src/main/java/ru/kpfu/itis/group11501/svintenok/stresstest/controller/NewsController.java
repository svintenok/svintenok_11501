package ru.kpfu.itis.group11501.svintenok.stresstest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.group11501.svintenok.stresstest.service.NewsService;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
@Controller
@RequestMapping("/news")
public class NewsController {
    private final NewsService newsService;

    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String newsListPage(Model model) {
        model.addAttribute("newsList", newsService.getNewsList());
        return "news_list";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String newsPage(@PathVariable(name = "id") Long id, Model model) {
        model.addAttribute("news", newsService.getNewsById(id));
        return "news";
    }



}