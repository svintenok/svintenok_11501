package ru.kpfu.itis.group11501.svintenok.stresstest.service;

import ru.kpfu.itis.group11501.svintenok.stresstest.model.User;
import ru.kpfu.itis.group11501.svintenok.stresstest.repository.UserRepository;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
public interface UserService {
    User getUser(Long id);
}
