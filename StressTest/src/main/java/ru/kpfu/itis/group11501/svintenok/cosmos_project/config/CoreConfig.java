package ru.kpfu.itis.group11501.svintenok.cosmos_project.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.kpfu.itis.group11501.svintenok.cosmos_project.service")
public class CoreConfig {
}
