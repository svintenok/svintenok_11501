package ru.kpfu.itis.group11501.svintenok.cosmos_project.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.Date;

/**
 * Author: Svintenok Kate
 * Date: 29.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@StaticMetamodel(DepartureDate.class)
public class DepartureDate_ {
    public static volatile SingularAttribute<DepartureDate, Long> id;
    public static volatile SingularAttribute<DepartureDate, Date> date;
    public static volatile SingularAttribute<DepartureDate, Tour> tour;
    public static volatile SingularAttribute<DepartureDate, Boolean> isActive;
}