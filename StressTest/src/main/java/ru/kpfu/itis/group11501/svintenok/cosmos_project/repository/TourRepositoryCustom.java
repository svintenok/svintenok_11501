package ru.kpfu.itis.group11501.svintenok.cosmos_project.repository;

import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 29.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface TourRepositoryCustom {
    List<Tour> kfindAllBySearch(String search);
}
