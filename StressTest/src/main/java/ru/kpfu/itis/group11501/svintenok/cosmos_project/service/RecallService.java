package ru.kpfu.itis.group11501.svintenok.cosmos_project.service;

import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Recall;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 09.11.2016
 * Group: 11-501
 * Task: semester project
 */
public interface RecallService {
    Recall getRecall(Long id);
    void addRecall(Recall recall);
    void removeRecall(Recall recall);
    List<Recall> getRecallListByTour(Tour tour);
}
