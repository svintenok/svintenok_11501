package ru.kpfu.itis.group11501.svintenok.cosmos_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.UserService;

/**
 * Author: Svintenok Kate
 * Date: 20.04.2017
 * Group: 11-501
 * Project: StressTest
 */
@Controller
@RequestMapping("/profile")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String userPage(@PathVariable(name = "id") Long id, Model model) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("current_user", currentUser);
        model.addAttribute("user", userService.getUserWithInfo(id));
        return "profile";
    }
}