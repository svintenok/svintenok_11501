package ru.kpfu.itis.group11501.svintenok.cosmos_project.service;

import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Comment;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.News;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
public interface NewsService {
    News getNews(Long id);
    List<News> getNewsList();
    News addNews(News news);

    void addComment(Comment comment);
    void deleteComment(Comment comment);
    Comment getComment(Long id);
}
