package ru.kpfu.itis.group11501.svintenok.stresstest.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.group11501.svintenok.stresstest.model.User;
import ru.kpfu.itis.group11501.svintenok.stresstest.repository.UserRepository;
import ru.kpfu.itis.group11501.svintenok.stresstest.service.UserService;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
@Service
public class UserServiceImpl implements UserService{
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User getUser(Long id) {
        return userRepository.findOne(id);
    }
}