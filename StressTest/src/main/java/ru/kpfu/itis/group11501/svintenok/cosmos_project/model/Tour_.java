package ru.kpfu.itis.group11501.svintenok.cosmos_project.model;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

/**
 * Author: Svintenok Kate
 * Date: 29.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@StaticMetamodel(Tour.class)
public class Tour_ {
    public static volatile SingularAttribute<Tour, Long> id;
    public static volatile SingularAttribute<Tour, String> title;
    public static volatile SingularAttribute<Tour, String> place;
    public static volatile SingularAttribute<Tour, String> rocket;
    public static volatile SingularAttribute<Tour, String> description;
    public static volatile SingularAttribute<Tour, Integer> cost;
    public static volatile SingularAttribute<Tour, Integer> seatsNumber;
}
