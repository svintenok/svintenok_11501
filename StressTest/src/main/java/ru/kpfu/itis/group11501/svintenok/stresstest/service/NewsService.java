package ru.kpfu.itis.group11501.svintenok.stresstest.service;

import ru.kpfu.itis.group11501.svintenok.stresstest.model.Comment;
import ru.kpfu.itis.group11501.svintenok.stresstest.model.News;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
public interface NewsService {
    News getNewsById(Long id);
    List<News> getNewsList();
}
