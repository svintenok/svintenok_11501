package ru.kpfu.itis.group11501.svintenok.cosmos_project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Recall;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.User;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.DepartureDateService;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.RecallService;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.service.TourService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * Author: Svintenok Kate
 * Date: 24.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
@Controller
@RequestMapping("/tours")
public class TourController {

    private final TourService tourService;
    private final RecallService recallService;
    private final DepartureDateService departureDateService;

    @Autowired
    public TourController(TourService tourService, RecallService recallService, DepartureDateService departureDateService) {
        this.tourService = tourService;
        this.recallService = recallService;
        this.departureDateService = departureDateService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String tourListPage(Model model,  @RequestParam(name = "search", required = false) String search,
                               @RequestParam(name = "sorting", required = false) String sorting,
                               @RequestParam(name = "back_order", required = false) String backOrder) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("current_user", currentUser);
        boolean reverse = (backOrder != null);
        model.addAttribute("tours", tourService.getToursList(sorting, reverse, search));
        model.addAttribute("back_order", backOrder);
        model.addAttribute("search", search);
        model.addAttribute("sorting", sorting);
        return "tours";
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String tourPage(@PathVariable(name = "id") Long id, Model model) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Tour tour = tourService.getTour(id);
        if (tour == null)
            return "handle404";
        model.addAttribute("current_user", currentUser);
        model.addAttribute("tour", tourService.getTourWithInfo(id, currentUser));
        return "tour";
    }

    @RequestMapping(value = "/{id}/addRecall", method = RequestMethod.POST)
    public String addRecall(@PathVariable(name = "id") Long id,  @RequestParam(name = "departure_date_id") Long departureDateId,
                            @RequestParam(name = "text") String text, @RequestParam(name = "estimate") Integer estimate, HttpServletRequest request) {
        User currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        Recall recall = new Recall();
        recall.setUser(currentUser);
        recall.setDepartureDate(departureDateService.getDepartureDate(departureDateId));
        recall.setEstimate(estimate);
        recall.setText(text);
        recallService.addRecall(recall);
        return "redirect:" + request.getHeader("Referer");
    }

}