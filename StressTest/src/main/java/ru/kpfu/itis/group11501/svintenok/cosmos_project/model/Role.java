package ru.kpfu.itis.group11501.svintenok.cosmos_project.model;

import javax.persistence.*;

/**
 * Author: Svintenok Kate
 * Date: 31.10.2016
 * Group: 11-501
 * Task: semester project
 */
@Entity
@Table(name = "role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
