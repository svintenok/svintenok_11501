package ru.kpfu.itis.group11501.svintenok.cosmos_project.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Booking;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Recall;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.Tour;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 23.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface RecallRepository extends JpaRepository<Recall, Long> {
    @Query("select recall from Recall recall where " +
            "recall.departureDate in (select db from DepartureDate db where db.tour=:tour)")
    List<Recall> findAllByTour(@Param("tour") Tour tour);

    @Query("select avg(recall.estimate) from Recall recall where " +
            "recall.departureDate in (select db from DepartureDate db where db.tour=:tour)")
    Double getTourRate(@Param("tour") Tour tour);

    Recall findOneByUserIdAndDepartureDateId(Long userId, Long DepartureDateId);
}
