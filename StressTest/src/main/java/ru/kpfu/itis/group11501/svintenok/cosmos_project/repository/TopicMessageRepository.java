package ru.kpfu.itis.group11501.svintenok.cosmos_project.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.ForumTopic;
import ru.kpfu.itis.group11501.svintenok.cosmos_project.model.TopicMessage;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 29.05.2017
 * Group: 11-501
 * Project: CosmosProjectSpring
 */
public interface TopicMessageRepository extends JpaRepository<TopicMessage, Long> {
    List<TopicMessage> findAllByForumTopicIdOrderByDateAsc(Long id);
    List<TopicMessage> findAllByForumTopicId(Long id, Pageable pageRequest);
}
