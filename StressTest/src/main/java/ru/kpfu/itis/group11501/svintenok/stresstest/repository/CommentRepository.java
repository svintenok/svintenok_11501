package ru.kpfu.itis.group11501.svintenok.stresstest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.kpfu.itis.group11501.svintenok.stresstest.model.Comment;

import java.util.List;

/**
 * Author: Svintenok Kate
 * Date: 19.04.2017
 * Group: 11-501
 * Project: StressTest
 */
public interface CommentRepository extends JpaRepository<Comment, Long> {
    List<Comment> findAllByNewsId(Long newsId);
}
